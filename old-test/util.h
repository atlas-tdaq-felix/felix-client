#pragma once

#include "catch.hpp"

inline unsigned
port_from_sockaddr(struct sockaddr *sa)
{
  in_port_t port;
  if (sa->sa_family == AF_INET) {
    port = ((struct sockaddr_in*)sa)->sin_port;
  } else if (sa->sa_family == AF_INET6) {
    port = ((struct sockaddr_in6*)sa)->sin6_port;
  } else {
    return 0;
  }

  return ntohs(port);
}


inline unsigned
port_of_listen_socket(struct netio_listen_socket* ls)
{
  struct sockaddr_storage sa;
  netio_listen_socket_endpoint(ls, &sa);
  unsigned port = port_from_sockaddr((struct sockaddr*)&sa);
  return port;
}


inline unsigned
port_of_buffered_listen_socket(struct netio_buffered_listen_socket* ls)
{
  return port_of_listen_socket(&ls->listen_socket);
}


inline const char*
test_iface_addr()
{
    const char* addr = std::getenv("FELIX_CLIENT_TEST_IFACE");
    if(!addr)
    {
        WARN( "FELIX_CLIENT_TEST_IFACE is not set, using 127.0.0.1 as loopback address. "
                  << "If fi_endpoint fails, consider setting FELIX_CLIENT_TEST_IFACE to the RDMA device's IP address.");
        addr = "127.0.0.1";
    }
    return addr;
}
