#include "catch.hpp"

#include "felix/felix_client.hpp"

#include <cstring>

#include "util.h"

namespace {

    void on_init() {
        printf("on_init called\n");
    }

    void on_connect(netio_tag_t fid) {
        printf("on_connect called 0x%lx\n", fid);
    }

    void on_disconnect(netio_tag_t fid) {
        printf("on_disconnect called 0x%lx\n", fid);
    }

    void on_data(netio_tag_t fid, const uint8_t* data, size_t size, uint8_t status) {
        printf("on_data called for 0x%lx with size %lu and status %u\n", fid, size, status);
    };

    void on_exec() {
        printf("on_exec called\n");
    }

    TEST_CASE( "subscribe to server without thread", "[subscribe_no_thread]" ) {
        FelixClient::LogLevel log_level = FelixClient::LogLevel::info;

        FelixClient client(test_iface_addr(), "<local_interface>", log_level, true, true);

        FelixClient::OnInitCallback init_function = on_init;
        client.callback_on_init(init_function);

        FelixClient::OnDataCallback data_function = on_data;
        client.callback_on_data(data_function);

        FelixClient::OnConnectCallback connect_function = on_connect;
        client.callback_on_connect(connect_function);

        FelixClient::OnDisconnectCallback disconnect_function = on_disconnect;
        client.callback_on_disconnect(disconnect_function);

        FelixClient::UserFunction user_function = on_exec;
        client.exec(user_function);

        std::vector<std::string> fid_list({ "22", "44" });
        for(std::vector<std::string>::iterator it = fid_list.begin(); it != fid_list.end(); ++it) {
            netio_tag_t fid = std::stoul(*it, 0, 0);
            client.subscribe(fid);
        }

        client.run();
    }
}
