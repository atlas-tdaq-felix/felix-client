# felix-client

[![cistatus](https://gitlab.cern.ch/atlas-tdaq-felix/felix-client/badges/master/pipeline.svg)](https://gitlab.cern.ch/atlas-tdaq-felix/felix-client/pipelines)
[![coverage](https://gitlab.cern.ch/atlas-tdaq-felix/felix-client/badges/master/coverage.svg?job=test)](https://atlas-project-felix.web.cern.ch/atlas-project-felix/felix-client/coverage/master/src/)

![Overview on felix-client with contextHandler](doc/felix_client_general_overview_including_cH_Sept2022.png) 

To edit the diagrams use the free online editor https://www.diagrams.net/ click on "start", then on "open existing diagram" and select the ".drawio" file you find in the ""doc folder of this repo. To create a png go to "File->Export as->Advanced" and select 300dpi as resolution.
