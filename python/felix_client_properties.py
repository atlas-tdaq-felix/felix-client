from enum import Enum


class FelixClientKey(Enum):
    READ_ENV = "read_env"

    LOCAL_IP_OR_INTERFACE = "local_ip_or_interface"
    LOG_LEVEL = "log_level"
    BUS_INTERFACE = "bus_interface"
    BUS_DIR = "bus_dir"
    BUS_GROUP_NAME = "bus_group_name"
    VERBOSE_BUS = "verbose_bus"
    VERBOSE_ZYRE = "verbose_zyre"
    TIMEOUT = "timeout"
    NETIO_PAGES = "netio_pages"
    NETIO_PAGESIZE = "netio_pagesize"
    THREAD_AFFINITY = "thread_affinity"
