from enum import Enum


class FelixClientError(Enum):
    FW_TRUNC = 1
    SW_TRUNC = 2
    FW_MALF = 4
    FW_CRC = 8
    SW_MALF = 16
