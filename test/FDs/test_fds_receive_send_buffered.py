#!/usr/bin/env python3

import subprocess
import unittest
import os

from felix_test_case import FelixTestCase


class TestReceiveSendBufferedFDs(FelixTestCase):

    def setUp(self):
        self.start('receive-buffered-fds-' + FelixTestCase.netio_protocol)

    def tearDown(self):
        self.stop('receive-buffered-fds-' + FelixTestCase.netio_protocol)

    def test_thread_send_buffered_fds(self):
        try:
            timeout = 150
            iterations = "100"
            ip = FelixTestCase.ip
            fid_toflx = FelixTestCase.fid_toflx
            print(FelixTestCase.tmp_prefix)
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_CLIENT_" + FelixTestCase.uuid
            self.wait_file(os.path.join(bus_dir, group_name), timeout)
            output = subprocess.check_output(' '.join(("./felix-client-thread-send-fds-loop",
                                                       "--bus-dir", bus_dir,
                                                       "--bus-group-name", group_name,
                                                       iterations, FelixTestCase.netio_protocol + ":" + ip, fid_toflx, "HelloFelix")),
                                             timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            print(output)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)
        except subprocess.TimeoutExpired as e:
            print(e.cmd)
            print(e.output.decode())
            print("Timeout !")
            self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
