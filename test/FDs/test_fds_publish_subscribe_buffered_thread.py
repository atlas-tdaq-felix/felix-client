#!/usr/bin/env python3

import subprocess
import unittest
import os

from felix_test_case import FelixTestCase


class TestPublishSubscribeUnbufferedThreadLoopFDs(FelixTestCase):

    def setUp(self):
        self.start('fds-buffered-' + FelixTestCase.netio_protocol)

    def tearDown(self):
        self.stop('fds-buffered-' + FelixTestCase.netio_protocol)

    def test_subscribe_unbuffered_loop(self):
        try:
            timeout = 90
            ip = FelixTestCase.ip
            fid = FelixTestCase.fid
            messages = "50"
            iterations = "20"
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_CLIENT_" + FelixTestCase.uuid
            self.wait_file(os.path.join(bus_dir, group_name), timeout)
            subprocess.check_output(' '.join(("./felix-client-thread-subscribe-unsubscribe-loop",
                                              "--verbose",
                                              "--bus-dir", bus_dir,
                                              "--bus-group-name", group_name,
                                              iterations, FelixTestCase.netio_protocol + ":" + ip, fid, "--messages", messages)),
                                    timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)
        except subprocess.TimeoutExpired as e:
            print(e.cmd)
            print(e.output.decode())
            print("Timeout !")
            self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
