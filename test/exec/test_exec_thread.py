#!/usr/bin/env python3

import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestExecThread(FelixTestCase):

    def test_thread_exec(self):
        try:
            timeout = 30
            ip = FelixTestCase.ip
            group_name = "FELIX_CLIENT_" + FelixTestCase.uuid
            subprocess.check_output(' '.join(("./felix-client-thread-exec",
                                              "--verbose",
                                              "--bus-group-name", group_name, FelixTestCase.netio_protocol + ":" + ip)),
                                    timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            # print(output)
            self.assertTrue(False)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)
        except subprocess.TimeoutExpired as e:
            print(e.cmd)
            print(e.output.decode())
            print("Timeout !")
            self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
