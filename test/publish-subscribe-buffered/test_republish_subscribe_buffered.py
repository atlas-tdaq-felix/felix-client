#!/usr/bin/env python3

import concurrent.futures
import subprocess
import time
import unittest
import os

from felix_test_case import FelixTestCase


class TestRepublishSubscribeBuffered(FelixTestCase):

    def setUp(self):
        self.start('publish-buffered-' + FelixTestCase.netio_protocol)

    def tearDown(self):
        self.stop('publish-buffered-' + FelixTestCase.netio_protocol)

    def run_republish_buffered(self):
        time.sleep(5)
        self.stop('publish-buffered-' + FelixTestCase.netio_protocol)
        time.sleep(10)
        self.start('publish-buffered-' + FelixTestCase.netio_protocol)

    def run_test_subscribe_buffered(self):
        try:
            timeout = 40
            ip = FelixTestCase.ip
            fid = FelixTestCase.fid
            messages = "100"
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_CLIENT_" + FelixTestCase.uuid
            self.wait_file(os.path.join(bus_dir, group_name), timeout)
            subprocess.check_output(' '.join(("./felix-client-subscribe",
                                              "--verbose",
                                              "--bus-dir", bus_dir,
                                              "--bus-group-name", group_name,
                                              FelixTestCase.netio_protocol + ":" + ip, fid, "--messages", messages)),
                                    timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            # print(output)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)
        except subprocess.TimeoutExpired as e:
            print(e.cmd)
            print(e.output.decode())
            print("Timeout !")
            self.assertTrue(False)

    def test_subscribe_publish_buffered(self):
        task_set = {}
        executor = concurrent.futures.ThreadPoolExecutor(max_workers=2)
        a = executor.submit(self.run_test_subscribe_buffered)
        task_set[a] = "subscribe"
        b = executor.submit(self.run_republish_buffered)
        task_set[b] = "republish"

        for task in concurrent.futures.as_completed(task_set.keys()):
            print("result " + task_set[task] + " " + str(task.result()))


if __name__ == '__main__':
    unittest.main()
