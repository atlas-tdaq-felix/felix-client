#!/usr/bin/env python3

import subprocess
import unittest
import os

from felix_test_case import FelixTestCase


class TestPublishSubscribeThreadBufferedVecNoTimeout(FelixTestCase):

    def setUp(self):
        self.start('publish-buffered-' + FelixTestCase.netio_protocol)

    def tearDown(self):
        self.stop('publish-buffered-' + FelixTestCase.netio_protocol)

    def test_thread_subscribe_buffered_vec_no_timeout(self):
        try:
            timeout = 60
            ip = FelixTestCase.ip
            fids = "1152921504607371264 "
            fids += "1152921504607436800 "
            fids += "1152921504607502336 "
            fids += "1152921504607567872 "
            fids += "1152921504607633408 "
            fids += "1152921504607698944 "
            fids += "1152921504607764480 "
            fids += "1152921504607830016 "
            fids += "1152921504611565568 "
            fids += "1152921504611631104 "
            fids += "1152921504611696640 "
            fids += "1152921504611762176 "
            fids += "1152921504611827712 "
            fids += "1152921504611893248 "
            fids += "1152921504611958784 "
            fids += "1152921504612024320 "
            fids += "1152921504615759872 "
            fids += "1152921504615825408 "
            fids += "1152921504615890944 "
            fids += "1152921504615956480 "
            fids += "1152921504616022016 "
            fids += "1152921504616087552 "
            fids += "1152921504616153088 "
            fids += "1152921504616218624 "
            fids += "1152921504619954176 "
            fids += "1152921504620019712 "
            fids += "1152921504620085248 "
            fids += "1152921504620150784 "
            fids += "1152921504620216320 "
            fids += "1152921504620281856 "
            fids += "1152921504620347392 "
            fids += "1152921504620412928 "
            fids += "1152921504624148480 "
            fids += "1152921504624214016 "
            fids += "1152921504624279552 "
            fids += "1152921504624345088 "
            fids += "1152921504624410624 "
            fids += "1152921504624476160 "
            fids += "1152921504624541696 "
            fids += "1152921504624607232 "
            fids += "1152921504628342784 "
            fids += "1152921504628408320 "
            fids += "1152921504628473856 "
            fids += "1152921504628539392 "
            fids += "1152921504628604928 "
            fids += "1152921504628670464 "
            fids += "1152921504628736000 "
            fids += "1152921504628801536 "
            fids += "1152921504632537088 "
            fids += "1152921504632602624 "
            fids += "1152921504632668160 "
            fids += "1152921504632733696 "
            fids += "1152921504632799232 "
            fids += "1152921504632864768 "
            fids += "1152921504632930304 "
            fids += "1152921504632995840 "
            fids += "1152921504636731392 "
            fids += "1152921504636796928 "
            fids += "1152921504636862464 "
            fids += "1152921504636928000 "
            fids += "1152921504636993536 "
            fids += "1152921504637059072 "
            fids += "1152921504637124608 "
            fids += "1152921504637190144 "
            fids += "1152921504640925696 "
            fids += "1152921504640991232 "
            fids += "1152921504641056768 "
            fids += "1152921504641122304 "
            fids += "1152921504641187840 "
            fids += "1152921504641253376 "
            fids += "1152921504641318912 "
            fids += "1152921504641384448 "
            fids += "1152921504645120000 "
            fids += "1152921504645185536 "
            fids += "1152921504645251072 "
            fids += "1152921504645316608 "
            fids += "1152921504645382144 "
            fids += "1152921504645447680 "
            fids += "1152921504645513216 "
            fids += "1152921504645578752 "
            fids += "1152921504649314304 "
            fids += "1152921504649379840 "
            fids += "1152921504649445376 "
            fids += "1152921504649510912 "
            fids += "1152921504649576448 "
            fids += "1152921504649641984 "
            fids += "1152921504649707520 "
            fids += "1152921504649773056 "
            fids += "1152921504653508608 "
            fids += "1152921504653574144 "
            fids += "1152921504653639680 "
            fids += "1152921504653705216 "
            fids += "1152921504653770752 "
            fids += "1152921504653836288 "
            fids += "1152921504653901824 "
            fids += "1152921504653967360"

            messages = "500"
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_CLIENT_" + FelixTestCase.uuid
            self.wait_file(os.path.join(bus_dir, group_name), timeout)
            subprocess.check_output(' '.join(("./felix-client-thread-subscribe-vector",
                                              "--verbose",
                                              "--timeout=0",
                                              "--bus-dir", bus_dir,
                                              "--bus-group-name", group_name,
                                              FelixTestCase.netio_protocol + ":" + ip, fids, "--messages", messages)),
                                    timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            # print(output)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)
        except subprocess.TimeoutExpired as e:
            print(e.cmd)
            print(e.output.decode())
            print("Timeout !")
            self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
