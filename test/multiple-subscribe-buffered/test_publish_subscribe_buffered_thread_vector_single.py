#!/usr/bin/env python3

import subprocess
import unittest
import os

from felix_test_case import FelixTestCase


class TestPublishSubscribeThreadBufferedVecSingle(FelixTestCase):

    def setUp(self):
        self.start('publish-buffered-' + FelixTestCase.netio_protocol)

    def tearDown(self):
        self.stop('publish-buffered-' + FelixTestCase.netio_protocol)

    def test_thread_subscribe_buffered_vec_single(self):
        try:
            timeout = 300
            ip = FelixTestCase.ip
            fids = "1152921504607371264 "
            messages = "50"
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_CLIENT_" + FelixTestCase.uuid
            self.wait_file(os.path.join(bus_dir, group_name), timeout)
            subprocess.check_output(' '.join(("./felix-client-thread-subscribe-vector",
                                              "--verbose",
                                              "--timeout=3000",
                                              "--bus-dir", bus_dir,
                                              "--bus-group-name", group_name,
                                              FelixTestCase.netio_protocol + ":" + ip, fids, "--messages", messages)),
                                    timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            # print(output)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)
        except subprocess.TimeoutExpired as e:
            print(e.cmd)
            print(e.output.decode())
            print("Timeout !")
            self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
