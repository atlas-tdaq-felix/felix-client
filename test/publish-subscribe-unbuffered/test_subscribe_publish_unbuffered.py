#!/usr/bin/env python3

import concurrent.futures
import subprocess
import time
import unittest
import os

from felix_test_case import FelixTestCase


class TestSubscribePublishUnbuffered(FelixTestCase):

    def run_publish_unbuffered(self):
        time.sleep(5)
        self.start('publish-unbuffered-' + FelixTestCase.netio_protocol)

    def run_test_subscribe_unbuffered(self):
        try:
            timeout = 30
            ip = FelixTestCase.ip
            fid = FelixTestCase.fid
            messages = "50"
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_CLIENT_" + FelixTestCase.uuid
            self.wait_file(os.path.join(bus_dir, group_name), timeout)
            subprocess.check_output(' '.join(("./felix-client-subscribe",
                                              "--verbose",
                                              "--bus-dir", bus_dir,
                                              "--bus-group-name", group_name,
                                              FelixTestCase.netio_protocol + ":" + ip, fid, "--messages", messages)),
                                    timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            # print(output)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)
        except subprocess.TimeoutExpired as e:
            print(e.cmd)
            print(e.output.decode())
            print("Timeout !")
            self.assertTrue(False)

    def test_subscribe_publish_unbuffered(self):
        task_set = {}
        executor = concurrent.futures.ThreadPoolExecutor(max_workers=2)
        a = executor.submit(self.run_test_subscribe_unbuffered)
        task_set[a] = "subscribe"
        b = executor.submit(self.run_publish_unbuffered)
        task_set[b] = "publish"

        for task in concurrent.futures.as_completed(task_set.keys()):
            print("result " + task_set[task] + " " + str(task.result()))

        self.stop('publish-unbuffered-' + FelixTestCase.netio_protocol)


if __name__ == '__main__':
    unittest.main()
