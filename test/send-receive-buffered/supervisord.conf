[unix_http_server]
file=/tmp/felix-client_%(ENV_UUID)s.sock

[supervisord]
logfile=/tmp/%(ENV_UUID)s-supervisord.log                                     ; supervisord log file
logfile_maxbytes=50MB                                                 ; maximum size of logfile before rotation
logfile_backups=10                                                    ; number of backed up logfiles
loglevel=error                                                        ; info, debug, warn, trace
pidfile=/tmp/%(ENV_UUID)s-supervisord.pid                                      ; pidfile location
nodaemon=false                                                        ; run supervisord as a daemon
minfds=1024                                                           ; number of startup file descriptors
minprocs=200                                                          ; number of process descriptors
; user=duns                                                           ; default user
childlogdir=%(here)s                                                  ; where child log files will live

[supervisorctl]
serverurl=unix:///tmp/felix-client_%(ENV_UUID)s.sock

[rpcinterface:supervisor]
supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface

[program:receive-buffered-libfabric]
command=./netio-bus-buffered-recv
    --verbose
    --bus-dir %(ENV_TMP_PREFIX)s-bus
    --bus-group-name FELIX_CLIENT_%(ENV_UUID)s
    %(ENV_IP)s
    %(ENV_PORT)s
    %(ENV_FID_TOFLX)s
process_name=%(program_name)s
numprocs=1
stdout_logfile=%(ENV_TMP_PREFIX)s-receive-buffered-libfabric-stdout.log
stderr_logfile=%(ENV_TMP_PREFIX)s-receive-buffered-libfabric-stderr.log
autostart=false

[program:receive-buffered-tcp]
command=./netio-bus-buffered-recv
    --verbose
    --bus-dir %(ENV_TMP_PREFIX)s-bus
    --bus-group-name FELIX_CLIENT_%(ENV_UUID)s
    tcp:%(ENV_IP)s
    %(ENV_PORT)s
    %(ENV_FID_TOFLX)s
process_name=%(program_name)s
numprocs=1
stdout_logfile=%(ENV_TMP_PREFIX)s-receive-buffered-tcp-stdout.log
stderr_logfile=%(ENV_TMP_PREFIX)s-receive-buffered-tcp-stderr.log
autostart=false

[program:receive-multi-buffered-libfabric]
command=./netio-bus-buffered-recv --verbose --bus-group-name FELIX_CLIENT_%(ENV_UUID)s %(ENV_IP)s %(ENV_PORT)s %(ENV_FID_TOFLX)s %(ENV_ALT_FID_TOFLX)s
process_name=%(program_name)s
numprocs=1
stdout_logfile=%(ENV_TMP_PREFIX)s-receive-multi-buffered-libfabric-stdout.log
stderr_logfile=%(ENV_TMP_PREFIX)s-receive-multi-buffered-libfabric-stderr.log
autostart=false

[program:receive-multi-buffered-tcp]
command=./netio-bus-buffered-recv --verbose --bus-group-name FELIX_CLIENT_%(ENV_UUID)s tcp:%(ENV_IP)s %(ENV_PORT)s %(ENV_FID_TOFLX)s %(ENV_ALT_FID_TOFLX)s
process_name=%(program_name)s
numprocs=1
stdout_logfile=%(ENV_TMP_PREFIX)s-receive-multi-buffered-tcp-stdout.log
stderr_logfile=%(ENV_TMP_PREFIX)s-receive-multi-buffered-tcp-stderr.log
autostart=false
