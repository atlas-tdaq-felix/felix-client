#!/usr/bin/env python3
import concurrent.futures
import subprocess
import unittest
import os
from time import sleep
from felix_test_case import FelixTestCase


class TestContinuousLoopbackUnbuffered(FelixTestCase):

    def setUp(self):
        self.start('loopback-unbuffered-' + FelixTestCase.netio_protocol)

    def tearDown(self):
        self.stop('loopback-unbuffered-' + FelixTestCase.netio_protocol)

    def run_resend_continuous_loopback_unbuffered(self):
        sleep(5)
        self.stop('loopback-unbuffered-' + FelixTestCase.netio_protocol)
        sleep(10)
        self.start('loopback-unbuffered-' + FelixTestCase.netio_protocol)

    def run_subscribe_continuous_loopback_unbuffered(self):
        try:
            print("starting\n")
            timeout = 160
            ip = FelixTestCase.ip
            fid = FelixTestCase.fid
            fid_toflx = FelixTestCase.fid_toflx
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_CLIENT_" + FelixTestCase.uuid
            self.wait_file(os.path.join(bus_dir, group_name), timeout)
            subprocess.check_output(' '.join(("./felix-client-thread-continuous-loopback",
                                              "--verbose",
                                              "--bus-dir", bus_dir,
                                              "--bus-group-name", group_name, FelixTestCase.netio_protocol + ":" + ip, fid, fid_toflx,
                                              "\"Hello FELIX\"")),
                                    timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="utf-8")
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)
        except subprocess.TimeoutExpired as e:
            print(e.cmd)
            print(e.output.decode())
            print("Timeout !")
            self.assertTrue(False)

    def test_send_continuous_loopback_unbuffered(self):
        task_set = {}
        executor = concurrent.futures.ThreadPoolExecutor(max_workers=2)
        a = executor.submit(self.run_subscribe_continuous_loopback_unbuffered)
        task_set[a] = "subscribe"
        b = executor.submit(self.run_resend_continuous_loopback_unbuffered)
        task_set[b] = "resend"

        for task in concurrent.futures.as_completed(task_set.keys()):
            print("result " + task_set[task] + " " + str(task.result()))


if __name__ == '__main__':
    unittest.main()
