#!/usr/bin/env python

from __future__ import unicode_literals

import subprocess32 as subprocess
import unittest

from felix_test_case import FelixTestCase


class TestSubscribeTimeoutThread(FelixTestCase):

    def test_thread_subscribe_timeout(self):
        try:
            timeout = 10
            subscribe_timeout = "2000"
            ip = FelixTestCase.ip
            fid = FelixTestCase.fid
            messages = "0"
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_CLIENT_" + FelixTestCase.uuid
            subprocess.check_output(' '.join(("./felix-client-thread-subscribe",
                                              "--verbose",
                                              "--bus-dir", bus_dir,
                                              "--bus-group-name", group_name,
                                              FelixTestCase.netio_protocol + ":" + ip, fid, "--messages", messages, "--timeout", subscribe_timeout)),
                                    timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            # print(output)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 1)
        except subprocess.TimeoutExpired as e:
            print(e.cmd)
            print(e.output.decode())
            print("Timeout !")
            self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
