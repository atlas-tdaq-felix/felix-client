#!/usr/bin/env python3

import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestMultiReceiveSendUnbuffered(FelixTestCase):

    def setUp(self):
        self.start('receive-multi-unbuffered-' + FelixTestCase.netio_protocol)

    def tearDown(self):
        self.stop('receive-multi-unbuffered-' + FelixTestCase.netio_protocol)

    def test_thread_send_unbuffered(self):
        try:
            timeout = 30
            ip = FelixTestCase.ip
            group_name = "FELIX_CLIENT_" + FelixTestCase.uuid
            output = subprocess.check_output(' '.join(("./felix-client-thread-send-nb-mt",
                                                       "--verbose",
                                                       "--bus-group-name", group_name,
                                                       FelixTestCase.netio_protocol + ":" + ip, "HelloFelix", FelixTestCase.fid_toflx, FelixTestCase.alt_fid_toflx)),
                                             timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            print(output)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)
        except subprocess.TimeoutExpired as e:
            print(e.cmd)
            print(e.output.decode())
            print("Timeout !")
            self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
