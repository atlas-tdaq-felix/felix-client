#!/usr/bin/env python3

import subprocess
import unittest
import os

from felix_test_case import FelixTestCase


class TestLoopbackUnbufferedMulti(FelixTestCase):

    def setUp(self):
        self.start('loopback-unbuffered-multi-' + FelixTestCase.netio_protocol)

    def tearDown(self):
        self.stop('loopback-unbuffered-multi-' + FelixTestCase.netio_protocol)

    def test_send_loopback_unbuffered_multi(self):
        try:
            print("starting\n")
            timeout = 30
            ip = FelixTestCase.ip
            did = FelixTestCase.did
            cid = FelixTestCase.cid
            elink = FelixTestCase.elink
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_CLIENT_" + FelixTestCase.uuid
            self.wait_file(os.path.join(bus_dir, group_name), timeout)
            subprocess.check_output(' '.join(("./felix-client-thread-loopback-multi-nb",
                                              "--bus-dir", bus_dir,
                                              "--log-level debug",
                                              "--bus-group-name", group_name, FelixTestCase.netio_protocol + ":" + ip, cid, did, elink, "3",
                                              "\"Hello Multi FELIX\"")),
                                    timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="utf-8")
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)
        except subprocess.TimeoutExpired as e:
            print(e.cmd)
            print(e.output.decode())
            print("Timeout !")
            self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
