#!/usr/bin/env python3

import time
import timeout_decorator
import unittest

from felix_test_case import FelixTestCase

from libfelix_client_thread_py import FelixClientConfig, FelixClientThread, FelixClientCmd, FelixClientStatus  # noqa: E402
from felix_client_properties import FelixClientKey  # noqa: E402


class TestSendCmd(FelixTestCase):

    def setUp(self):
        self.start('send-cmd-loopback-timeout')

    def tearDown(self):
        self.stop('send-cmd-loopback-timeout')

    def on_init(self):
        print("on_init called")

    @timeout_decorator.timeout(60)
    def test_send_cmd_timeout(self):

        config = FelixClientConfig()
        config.on_init_callback(self.on_init)

        config.property[FelixClientKey.LOCAL_IP_OR_INTERFACE.value] = FelixTestCase.ip
        config.property[FelixClientKey.BUS_DIR.value] = FelixTestCase.tmp_prefix + '-bus'
        config.property[FelixClientKey.BUS_GROUP_NAME.value] = "FELIX_CLIENT_" + FelixTestCase.uuid
        config.property[FelixClientKey.LOG_LEVEL.value] = 'trace'
        config.property[FelixClientKey.VERBOSE_BUS.value] = 'True'
        config.property[FelixClientKey.TIMEOUT.value] = '5000'
        config.property[FelixClientKey.NETIO_PAGES.value] = '256'
        config.property[FelixClientKey.NETIO_PAGESIZE.value] = '65536'

        fct = FelixClientThread(config)

        fids = [0x1000000000080000, 0x1000000000090000, 0x10000000000a0000]
        cmd = FelixClientCmd.GET
        cmd_args = ["REG_MAP_VERSION"]

        for i in range(4):
            print("Calling send_cmd", i)
            replies = None
            status = None
            (status, replies) = fct.send_cmd(fids, cmd, cmd_args)
            print("Done send_cmd", i)

            print("Status", i, status)

            if i < 3:
                self.assertEqual(status, FelixClientStatus.OK)

                self.assertEqual(len(replies), 1)
                reply = replies[0]

                self.assertEqual(reply.status, FelixClientStatus.OK)
                self.assertEqual(reply.ctrl_fid, 0x1000000812008000)
                self.assertEqual(reply.message, 'Register Map')
                self.assertEqual(reply.value, 0x0400)
            else:
                print("Timeout Check")
                self.assertEqual(status, FelixClientStatus.ERROR_NO_REPLY)

                self.assertEqual(len(replies), 1)
                reply = replies[0]
                self.assertEqual(reply.status, FelixClientStatus.ERROR_NO_REPLY)
                self.assertEqual(reply.ctrl_fid, 0x1000000812008000)
                self.assertEqual(reply.message, 'Did not receive a reply')
                self.assertEqual(reply.value, 0)

                print("Waiting for answer to be ignored")
                time.sleep(10)

            print("Done check", i)

        print("Cleanup")
        config.on_init_callback(None)


if __name__ == '__main__':
    unittest.main()
