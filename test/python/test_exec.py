#!/usr/bin/env python3

import time
import timeout_decorator
import unittest

from felix_test_case import FelixTestCase

from libfelix_client_thread_py import FelixClientConfig, FelixClientThread  # noqa: E402
from felix_client_properties import FelixClientKey  # noqa: E402


class TestExec(FelixTestCase):

    exec_called = False

    def on_init(self):
        print("on_init called")
        self.exec_called = False

    def on_exec(self):
        print("on_exec called")
        self.exec_called = True

    @timeout_decorator.timeout(10)
    def test_exec(self):

        config = FelixClientConfig()
        config.on_init_callback(self.on_init)

        config.property[FelixClientKey.LOCAL_IP_OR_INTERFACE.value] = FelixTestCase.ip
        config.property[FelixClientKey.BUS_GROUP_NAME.value] = "FELIX_CLIENT_" + FelixTestCase.uuid

        print(config.property)

        fct = FelixClientThread(config)

        fct.exec(self.on_exec)

        while not self.exec_called:
            time.sleep(1)

        config.on_init_callback(None)

        self.assertTrue(self.exec_called)


if __name__ == '__main__':
    unittest.main()
