#!/usr/bin/env python3

import subprocess
import unittest
import os

from felix_test_case import FelixTestCase


class TestLoopbackGroupingBuffered(FelixTestCase):

    def setUp(self):
        self.start('loopback-grouping-buffered-' + FelixTestCase.netio_protocol)

    def tearDown(self):
        self.stop('loopback-grouping-buffered-' + FelixTestCase.netio_protocol)

    def test_send_loopback_grouping_buffered(self):
        try:
            timeout = 30
            ip = FelixTestCase.ip
            fid = FelixTestCase.fid
            fid_toflx = FelixTestCase.fid_toflx
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_CLIENT_" + FelixTestCase.uuid
            self.wait_file(os.path.join(bus_dir, group_name), timeout)
            subprocess.check_output(' '.join(("./felix-client-thread-vector-loopback-nb",
                                              "--verbose",
                                              "--bus-dir", bus_dir,
                                              "--bus-group-name", group_name, FelixTestCase.netio_protocol + ":" + ip, fid, fid_toflx,
                                              "\"Goodbye FELIX\"")),
                                    timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="utf-8")
            # print(output)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)
        except subprocess.TimeoutExpired as e:
            print(e.cmd)
            print(e.output.decode())
            print("Timeout !")
            self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
