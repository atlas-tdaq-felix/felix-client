#!/usr/bin/env python3

import concurrent.futures
import subprocess
import time
import unittest
import os

from felix_test_case import FelixTestCase


class TestSendReceiveUnbuffered(FelixTestCase):

    def run_receive_unbuffered(self):
        time.sleep(5)
        self.start('receive-unbuffered-' + FelixTestCase.netio_protocol)

    def run_test_thread_send_unbuffered(self):
        try:
            timeout = 30
            ip = FelixTestCase.ip
            fid_toflx = FelixTestCase.fid_toflx
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_CLIENT_" + FelixTestCase.uuid
            self.wait_file(os.path.join(bus_dir, group_name), timeout)
            subprocess.check_output(' '.join(("./felix-client-thread-send",
                                              "--verbose",
                                              "--bus-dir", bus_dir,
                                              "--bus-group-name", group_name,
                                              FelixTestCase.netio_protocol + ":" + ip, fid_toflx, "HelloFelix")),
                                    timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            # print(output)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 1)
        except subprocess.TimeoutExpired as e:
            print(e.cmd)
            print(e.output.decode())
            print("Timeout !")
            self.assertTrue(False)

    def test_thread_send_unbuffered(self):
        task_set = {}
        executor = concurrent.futures.ThreadPoolExecutor(max_workers=2)
        a = executor.submit(self.run_test_thread_send_unbuffered)
        task_set[a] = "send"
        b = executor.submit(self.run_receive_unbuffered)
        task_set[b] = "receive"

        for task in concurrent.futures.as_completed(task_set.keys()):
            print("result " + task_set[task] + " " + str(task.result()))

        self.stop('receive-unbuffered-' + FelixTestCase.netio_protocol)


if __name__ == '__main__':
    unittest.main()
