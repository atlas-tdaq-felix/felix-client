#!/usr/bin/env python3
# #!/bin/env PYTHONUNBUFFERED="yes" python
"""
Loopback for send-cmd request

Usage:
    netio-bus-send-cmd-loopback [options] <hostname_or_ip_or_iface> <recv-port> <recv-fid> <publish-port> <publish-fid>

Options:
    -b, --netio-pagesize=SIZE           NetIO page size in kilobytes [default: 64]
    -B, --netio-pages=N                 Number of NetIO pages [default: 256]
    -g, --bus-group-name GROUP_NAME     Group name to use for the bus [default: FELIX]
    -b, --bus-dir DIRECTORY             Directory to use for the bus [default: ./bus]
    -v, --verbose                       Verbose output
    -w, --netio-watermark=SIZE          NetIO watermark in kilobytes [default: 56]
    -i, --increase-time=S               Increase the time for the time it takes to reply by S seconds [default: 0]

Arguments:
    <hostname_or_ip_or_iface>   Hostname or IP or Interface to publish from
    <recv-port>                 Receive Port number to use
    <recv-fid>                  FID to receive commands
    <publish-port>              Publish Port number to use
    <publish-fid>               FID to publish replies
"""
import functools
import json
import netifaces
import os
import socket
import sys
import time

print = functools.partial(print, flush=True)  # noqa: A001

# add the ../python path to find felix python libs
sys.path.append(os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'python'))

from docopt import docopt  # noqa: E402

from netio_py import ffi, lib  # noqa: E402

from libfelix_bus_py import FelixBus, FelixBusInfo  # noqa: E402

from felixtag import FELIX_TAG  # noqa: E402


@ffi.def_extern()
def on_init(data):
    """On Init."""
    if verbose:
        print("on_init")


@ffi.def_extern()
def on_publish_subscribe(netio_socket, fid, addr, addrlen):
    """On Subscribe."""
    if verbose:
        print("on_subscribe")


@ffi.def_extern()
def on_publish_connection_established(socket):
    """On Connection Established."""
    if verbose:
        print("connection to subscriber established")


@ffi.def_extern()
def on_publish_connection_closed(socket):
    """On Connection Closed."""
    if verbose:
        print("connection to subscriber closed")


@ffi.def_extern()
def on_publish_buffer_available(netio_socket):
    """On Buffer Available."""
    if verbose:
        print("on_buffer_available")


@ffi.def_extern()
def on_recv_connection_established(socket):
    """On Connection Established."""
    if verbose:
        print("connection from sender established")


@ffi.def_extern()
def on_recv_connection_closed(socket):
    """On Connection Closed."""
    if verbose:
        print("connection from sender closed")


@ffi.def_extern()
def on_recv_msg_received(socket, data, size):
    """On Message Received."""
    global delay_time

    if verbose:
        print("message from sender received with size", size)

    # convert data and remove 16 byte prefix (what is that for?)
    buffer = ffi.buffer(data, size)
    b = bytes(buffer[16:])

    # decode and use uuid of first entry
    json_data = json.loads(b)
    uuid = json_data[0]['uuid']
    print(uuid)

    # HACK, we just assume they ask for REGMAP and we answer 0x400
    # FIXME not sure why the first byte is ignored
    ndjson_bytes = bytearray()
    ndjson_bytes.extend(b'\0[{"uuid":"')
    ndjson_bytes.extend(uuid.encode('utf-8'))
    ndjson_bytes.extend(b'","status":0,"message":"Register Map","value":1024}]')
    print(ndjson_bytes)

    # delay reply if needed
    if delay_time > 0:
        print("Delaying answer by", delay_time, "seconds")
        time.sleep(delay_time)
    delay_time += increase_time

    print("Sending answer")
    lib.netio_buffered_publish(netio_socket, publish_fid, ffi.from_buffer(ndjson_bytes), len(ndjson_bytes), 0, ffi.NULL)
    lib.netio_buffered_publish_flush(netio_socket, publish_fid, ffi.NULL)
    print("Sent answer")


def get_first_ip(interface):
    """TBD"""
    address = None
    try:
        address = netifaces.ifaddresses(interface)
    except ValueError:
        pass
    if address and address[netifaces.AF_INET] and address[netifaces.AF_INET][0] and address[netifaces.AF_INET][0]['addr']:
        return address[netifaces.AF_INET][0]['addr']
    return None


if __name__ == "__main__":
    """fifo2elink."""
    global verbose
    global delay_time

    args = docopt(__doc__, version=sys.argv[0] + " " + FELIX_TAG)

    bus_dir = args["--bus-dir"]
    bus_group_name = args["--bus-group-name"]
    verbose = args['--verbose']

    pagesize = int(args['--netio-pagesize'], 0)
    num_pages = int(args['--netio-pages'], 0)
    watermark = int(args['--netio-watermark'], 0)

    delay_time = 0
    increase_time = int(args['--increase-time'], 0)

    hostname = args['<hostname_or_ip_or_iface>'].encode('utf-8')
    tcp = lib.netio_tcp_mode(hostname)
    host = ffi.string(lib.netio_hostname(hostname)).decode('utf-8')
    if verbose:
        print("using:", "tcp" if tcp else "libfabric", host)

    ip = get_first_ip(host)
    if not ip:
        ip = socket.gethostbyname(host)
    recv_port = int(args['<recv-port>'], 0)
    recv_fid = int(args['<recv-fid>'], 0)
    publish_port = int(args['<publish-port>'], 0)
    publish_fid = int(args['<publish-fid>'], 0)

    if verbose:
        print("netio_context")
    context = ffi.new("struct netio_context *")

    if verbose:
        print("netio_init")
    lib.netio_init(context)

    if verbose:
        print("netio_eventloop and cb")
    evloop = ffi.addressof(context.evloop)
    evloop.cb_init = lib.on_init

    if verbose:
        print("felix-bus")
    bus = FelixBus()
    bus.set_path(bus_dir)
    bus.set_groupname(bus_group_name)

    if verbose:
        print("PubSub")
    publish_info = FelixBusInfo()
    publish_info.ip = ip
    publish_info.raw_tcp = tcp
    publish_info.port = publish_port
    publish_info.unbuffered = False
    publish_info.pubsub = True
    publish_info.netio_pages = num_pages
    publish_info.netio_pagesize = pagesize*1024

    if verbose:
        print("Publishing on bus fid", hex(publish_fid))
    bus.publish(publish_fid, "publish-register", publish_info)

    if verbose:
        print("netio_socket publish setup")
    publish_attr = ffi.new("struct netio_buffered_socket_attr *")
    publish_attr.num_pages = num_pages
    publish_attr.pagesize = pagesize * 1024
    publish_attr.watermark = watermark * 1024

    netio_socket = ffi.new("struct netio_publish_socket *")
    lib.netio_publish_socket_init(netio_socket, context, hostname, publish_port, publish_attr)
    netio_socket.cb_subscribe = lib.on_publish_subscribe
    netio_socket.cb_connection_established = lib.on_publish_connection_established
    netio_socket.cb_connection_closed = lib.on_publish_connection_closed
    netio_socket.cb_buffer_available = lib.on_publish_buffer_available

    if verbose:
        print("Send")
    recv_info = FelixBusInfo()
    recv_info.ip = ip
    recv_info.raw_tcp = tcp
    recv_info.port = recv_port
    recv_info.unbuffered = False
    recv_info.pubsub = False
    recv_info.netio_pages = num_pages
    recv_info.netio_pagesize = pagesize*1024

    if verbose:
        print("Publishing on bus fid", hex(recv_fid))
    bus.publish(recv_fid, "recv-register", recv_info)

    if verbose:
        print("netio_socket send setup")
    recv_attr = ffi.new("struct netio_buffered_socket_attr *")
    recv_attr.num_pages = num_pages
    recv_attr.pagesize = pagesize * 1024
    recv_attr.watermark = watermark * 1024

    netio_recv_socket = ffi.new("struct netio_buffered_listen_socket *")
    if tcp:
        lib.netio_buffered_listen_tcp_socket_init(netio_recv_socket, context, recv_attr)
        lib.netio_buffered_listen_tcp(netio_recv_socket, ip.encode('utf-8'), recv_port)
    else:
        lib.netio_buffered_listen_socket_init(netio_recv_socket, context, recv_attr)
        lib.netio_buffered_listen(netio_recv_socket, ip.encode('utf-8'), recv_port)
    netio_recv_socket.cb_connection_established = lib.on_recv_connection_established
    netio_recv_socket.cb_connection_closed = lib.on_recv_connection_closed
    netio_recv_socket.cb_msg_received = lib.on_recv_msg_received

    if verbose:
        print("EventLoop")
    lib.netio_run(evloop)

    if verbose:
        print("Closing")

    print("Never Ends")
