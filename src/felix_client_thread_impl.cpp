#include <iostream>
#include <unistd.h>
#include <cstring>
#include <list>

#include "felix/felix_client_util.hpp"

#include "felix/felix_client_properties.h"
#include "felix/felix_client_exception.hpp"
#include "felix/felix_client_thread_impl.hpp"


FelixClientThreadImpl::FelixClientThreadImpl(Config& config) {
    if (config.property[FELIX_CLIENT_READ_ENV] == "True") {
        std::list<std::string> propertiesList = {
            FELIX_CLIENT_LOCAL_IP_OR_INTERFACE,
            FELIX_CLIENT_LOG_LEVEL,
            FELIX_CLIENT_BUS_DIR,
            FELIX_CLIENT_BUS_GROUP_NAME,
            FELIX_CLIENT_VERBOSE_BUS,
            FELIX_CLIENT_TIMEOUT,
            FELIX_CLIENT_NETIO_PAGES,
            FELIX_CLIENT_NETIO_PAGESIZE,
            FELIX_CLIENT_THREAD_AFFINITY,
        };

        std::string prefix = "FELIX_CLIENT_";

        for (const auto & property : propertiesList) {
            char* value = std::getenv((prefix + property).c_str());
            if (value != NULL) {
                config.property[property] = value;
            }
        }
    }

    std::string local_hostname = config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE];
    std::string log_lvl = config.property[FELIX_CLIENT_LOG_LEVEL];
    unsigned level = get_log_level(log_lvl);
    std::string timeout = config.property[FELIX_CLIENT_TIMEOUT];
    if (timeout == "") {
        timeout = "0";
    }
    try {
        timeoutms = std::stoul(timeout, 0, 0);
    } catch (std::invalid_argument const& error) {
        std::cout << "WARNING Timeout ignored: <" << timeout << ">\n";
        timeoutms = 0;
    }

    std::string felix_netio_pages = config.property[FELIX_CLIENT_NETIO_PAGES];
    unsigned netio_pages = felix_netio_pages == "" ? 0 : std::stoul(felix_netio_pages, 0, 0);

    std::string felix_netio_pagesize = config.property[FELIX_CLIENT_NETIO_PAGESIZE];
    unsigned netio_pagesize = felix_netio_pagesize == "" ? 0 : std::stoul(felix_netio_pagesize, 0, 0);

    std::string bus_dir = config.property[FELIX_CLIENT_BUS_DIR];
    bus_dir = bus_dir == "" ? "./bus" : bus_dir;

    std::string bus_group_name = config.property[FELIX_CLIENT_BUS_GROUP_NAME];
    bus_group_name = bus_group_name == "" ? "FELIX" : bus_group_name;

    client = new FelixClient(local_hostname,
                             bus_dir,
                             bus_group_name,
                             level,
                             config.property[FELIX_CLIENT_VERBOSE_BUS] == "True",
                             netio_pages,
                             netio_pagesize);

    if (config.on_init_callback) {
        client->callback_on_init(config.on_init_callback);
    }

    if (config.on_data_callback) {
        client->callback_on_data(config.on_data_callback);
    }

    if (config.on_connect_callback) {
        client->callback_on_connect(config.on_connect_callback);
    }

    if (config.on_disconnect_callback) {
        client->callback_on_disconnect(config.on_disconnect_callback);
    }

    if (config.property[FELIX_CLIENT_THREAD_AFFINITY] != "") {
        client->set_thread_affinity(config.property[FELIX_CLIENT_THREAD_AFFINITY]);
    }

    thread = std::thread(&FelixClient::run, client);

    while(!client->is_ready()) {
        usleep(100000);
    }
}

FelixClientThreadImpl::~FelixClientThreadImpl() {
    client->stop();
    if (thread.joinable()) {
        thread.join();
    }
    delete client;
}


void FelixClientThreadImpl::send_data(uint64_t fid, const uint8_t* data, size_t size, bool flush) {
    client->send_data(fid, data,  size, flush);
}


void FelixClientThreadImpl::send_data(netio_tag_t fid, const std::vector<const uint8_t*>& msgs, const std::vector<size_t>& sizes) {
    client->send_data(fid, msgs, sizes);
}


void FelixClientThreadImpl::send_data_nb(uint64_t fid, const uint8_t* data, size_t size, bool flush) {
    client->send_data_nb(fid, data,  size, flush);
}


void FelixClientThreadImpl::send_data_nb(netio_tag_t fid, const std::vector<const uint8_t*>& msgs, const std::vector<size_t>& sizes) {
    client->send_data_nb(fid, msgs, sizes);
}

void FelixClientThreadImpl::subscribe(uint64_t fid) {
    int status = client->subscribe(fid, timeoutms);
    if (status == FELIX_CLIENT_STATUS_TIMEOUT) {
        throw FelixClientResourceNotAvailableException();
    }
}

void FelixClientThreadImpl::subscribe(const std::vector<uint64_t>& fids) {
    int status = client->subscribe(fids, timeoutms);
    if (status == FELIX_CLIENT_STATUS_TIMEOUT) {
        throw FelixClientResourceNotAvailableException();
    }
}


void FelixClientThreadImpl::unsubscribe(uint64_t fid) {
    while(client->unsubscribe(fid) == FELIX_CLIENT_STATUS_CONNECTION_BUSY)
    {
        usleep(100);
    }
}

void FelixClientThreadImpl::exec(const UserFunction &user_function) {
    client->exec(user_function);
}

void FelixClientThreadImpl::init_send_data(uint64_t fid) {
    client->init_send_data(fid);
}

void FelixClientThreadImpl::init_subscribe(uint64_t fid) {
    client->init_subscribe(fid);
}

FelixClientThreadImpl::Status FelixClientThreadImpl::send_cmd(const std::vector<uint64_t>& fids, Cmd cmd, const std::vector<std::string>& cmd_args, std::vector<Reply>& replies) {
    return client->send_cmd(fids, cmd, cmd_args, replies);
}

void FelixClientThreadImpl::user_timer_start(unsigned long interval) {
    client->user_timer_start(interval);
}

void FelixClientThreadImpl::user_timer_stop() {
    client->user_timer_stop();
}

void FelixClientThreadImpl::callback_on_user_timer(OnUserTimerCallback on_user_timer_cb) {
    client->callback_on_user_timer(on_user_timer_cb);
}

extern "C" {
    FelixClientThreadInterface * create_felix_client(FelixClientThreadInterface::Config& config) {
        return new FelixClientThreadImpl(config);
    }
}
