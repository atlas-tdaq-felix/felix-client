/**
 * Copyright (c) 2020 rxi
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the MIT license. See `log.c` for details.
 */

#ifndef CLOG_H
#define CLOG_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

#define CLOG_VERSION "0.1.0"

typedef struct {
  va_list ap;
  const char *fmt;
  const char *file;
  struct tm *time;
  void *udata;
  int line;
  int level;
} clog_Event;

typedef void (*clog_LogFn)(clog_Event *ev);
typedef void (*clog_LockFn)(bool lock, void *udata);

//enum { LOG_TRACE, LOG_DEBUG, LOG_INFO, LOG_WARN, LOG_ERROR, LOG_FATAL };
enum { CLOG_FATAL, CLOG_ERROR, CLOG_WARN, CLOG_INFO, CLOG_DEBUG, CLOG_TRACE };

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define clog_trace(...) clog_log(CLOG_TRACE, __FILENAME__, __LINE__, __VA_ARGS__)
#define clog_debug(...) clog_log(CLOG_DEBUG, __FILENAME__, __LINE__, __VA_ARGS__)
#define clog_info(...)  clog_log(CLOG_INFO, __FILENAME__, __LINE__, __VA_ARGS__)
#define clog_warn(...)  clog_log(CLOG_WARN, __FILENAME__, __LINE__, __VA_ARGS__)
#define clog_error(...) clog_log(CLOG_ERROR, __FILENAME__, __LINE__, __VA_ARGS__)
#define clog_fatal(...) clog_log(CLOG_FATAL, __FILENAME__, __LINE__, __VA_ARGS__)

const char* clog_level_string(int level);
void clog_set_lock(clog_LockFn fn, void *udata);
void clog_set_level(int level);
int clog_level();
void clog_set_quiet(bool enable);
int clog_add_callback(clog_LogFn fn, void *udata, int level);
int clog_add_fp(FILE *fp, int level);

void clog_log(int level, const char *file, int line, const char *fmt, ...);

#ifdef __cplusplus
}
#endif

#endif
