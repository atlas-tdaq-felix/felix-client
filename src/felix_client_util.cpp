#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <cstring>
#include <list>

#include "netio/netio.h"

#include "felix/felix_client_properties.h"

#include "felix/felix_client_exception.hpp"
#include "felix/felix_client_util.hpp"

#include "clog.h"

// Returns "" in case neither the IP nor the Interface was found.
std::string get_value_from_getifaddrs(std::string key, bool return_ip) {
    struct ifaddrs *ifaddr, *ifa;
    char ip[NI_MAXHOST];

    if (getifaddrs(&ifaddr) == -1) {
        clog_fatal("getifaddrs problem");
        throw FelixClientException("felix-client-util:get_value_from_getifaddrs getifaddrs problem");
    }

    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
        if (ifa->ifa_addr == NULL)
            continue;

        int status = getnameinfo(ifa->ifa_addr, sizeof(struct sockaddr_in), ip, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
        if (status != 0) {
            continue;
        }

        const char* host = netio_hostname(key.c_str());
        std::string protocol = netio_protocol(key.c_str());
        if (((strcmp(ip, host) == 0) || (strcmp(ifa->ifa_name, host) == 0)) && (ifa->ifa_addr->sa_family==AF_INET)) {
            clog_trace("Interface : <%s>", ifa->ifa_name);
            clog_trace("Address : <%s>", ip);
            std::string value = protocol + ":" + (return_ip ? ip : ifa->ifa_name);
            freeifaddrs(ifaddr);
            clog_trace("InterfaceOrAddress : <%s>", value.c_str());
            return value;
        }
    }

    // not found
    freeifaddrs(ifaddr);
    return "";
}

std::string get_ip_from_interface(const std::string& interface) {
    return get_value_from_getifaddrs(interface, true);
}

std::string get_interface_from_ip(const std::string& ip) {
    return get_value_from_getifaddrs(ip, false);
}

unsigned get_log_level(const std::string& log_level) {
    if (log_level == "trace") return CLOG_TRACE;
    if (log_level == "debug") return CLOG_DEBUG;
    if (log_level == "info") return CLOG_INFO;
    if (log_level == "notice") return CLOG_INFO; // for backwards compatibility
    if (log_level == "warning") return CLOG_WARN;
    if (log_level == "error") return CLOG_ERROR;
    if (log_level == "fatal") return CLOG_FATAL;
    return CLOG_INFO;
}

std::string netio_socket_state_name(enum netio_socket_state e)
{
   switch (e)
   {
      case DISCONNECTED: return "DISCONNECTED";
      case CONNECTING: return "CONNECTING";
      case CONNECTED: return "CONNECTED";
      default: return "Unknown netio_socket_state_name for: " + std::to_string(e);
   }
}
