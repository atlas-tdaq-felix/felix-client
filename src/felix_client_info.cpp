#include "felix/felix_client_info.hpp"
#include "felix/felix_client_exception.hpp"

#include "clog.h"


netio_socket_type FelixClientContext::getType(){
    return mType;
}

netio_socket_connection FelixClientContext::getConnectionType(){
    return mConnection;
}

subscribe_state FelixClientContext::getSubscriptionState(){
    return mSubscribed.load();
}

std::unordered_map<std::string, timespec> FelixClientContext::getSubscriptionTimes(){
    return mSubscriptionTimes;
}

SocketWrapper* FelixClientContext::getSocket(){
    return mSocket;
}

bool FelixClientContext::getForRegister(){
    return mForRegister;
}

uint FelixClientContext::getWatermark(){
    return mWatermark;
}

uint FelixClientContext::getPagesize(){
    return mPagesize;
}

uint FelixClientContext::getPages(){
    return mPages;
}

uint FelixClientContext::getPort(){
    return mPort;
}

std::string FelixClientContext::getIp(){
    return mIp;
}

bool FelixClientContext::isTcp() {
    return mTcp;
}

void FelixClientContext::setSubscriptionTimes(std::string name, timespec ts){
    if(name == "subscribe" || name == "subscribe_evloop"){
        mSubscriptionTimes[name] = ts;
    }else{
       clog_error("Invalid entry for SubscriptionTime");
   }
}

void FelixClientContext::setSocket(SocketWrapper* s){
    mSocket = s;
}

void FelixClientContext::setInfos(std::string& ip, uint port, uint pages, uint pagesize, uint watermark, bool unbuffered, bool pubsub, bool tcp){
    mIp = ip;
    mPort = port;
    mPages = pages;
    mPagesize = pagesize;
    mWatermark = watermark;
    mType = unbuffered ? UNBUFFERED : BUFFERED;
    mConnection = pubsub ? PUBSUB : SENDRECV;
    mTcp = tcp;
}

void FelixClientContext::setSubscriptionState(subscribe_state s){
    if (s == SUB){
        if (mMultiWaitCtx){
            mMultiWaitCtx->count++;
            if(mMultiWaitCtx->count == mMultiWaitCtx->expectedSize){
                mMultiWaitCtx->allSubscribed.notify_all();
            }
        }
        mSubscribeWait.notify_all();        //Socket changes to subscribe state
    }
    mSubscribed = s;
}

void FelixClientContext::setForRegister(bool r){
    mForRegister = r;
}

void FelixClientContext::setWaitContext(MultiFidsWaitContext* ctx){
    mMultiWaitCtx = ctx;
}

void FelixClientContext::clearWaitContext(){
    mMultiWaitCtx = NULL;
}

void FelixClientContext::removeSocket(){
    mSocket = mDummyWrapper.get();
}
