#include <pybind11/pybind11.h>
#include <pybind11/functional.h>
#include <pybind11/chrono.h>
#include <pybind11/stl.h>
#include <pybind11/stl_bind.h>

#include <array>
#include <iostream>
#include <memory>
#include <map>
#include <vector>

#include "felix/felix_client_properties.h"
#include "felix/felix_client_thread_impl.hpp"

namespace py = pybind11;

std::function<void (uint64_t fid, py::bytes data, uint8_t status)> python_on_data_callback;

void cxx_on_data_callback(uint64_t fid, const uint8_t* data, size_t size, uint8_t status) {
    // printf("cxx_on_data_callback\n");
    py::bytes bytes((const char*)data, size);
    if (python_on_data_callback) {
        python_on_data_callback(fid, bytes, status);
    }
}

PYBIND11_MAKE_OPAQUE(std::map<std::string, std::string>);
PYBIND11_MAKE_OPAQUE(std::vector<FelixClientThreadExtension::Reply>);

PYBIND11_MODULE(libfelix_client_thread_py, m) {
    m.doc() = "Python bindings for FelixClientThread.";

    // Properties
    py::bind_map<std::map<std::string, std::string>>(m, "FelixClientProperties");

    //  enum Cmd
    py::enum_<FelixClientThreadExtension::Cmd>(m, "FelixClientCmd", py::arithmetic())
        .value("UNKNOWN", FelixClientThreadExtension::Cmd::UNKNOWN)
        .value("NOOP", FelixClientThreadExtension::Cmd::NOOP)
        .value("GET", FelixClientThreadExtension::Cmd::GET)
        .value("SET", FelixClientThreadExtension::Cmd::SET)
        .value("TRIGGER", FelixClientThreadExtension::Cmd::TRIGGER)
        .value("ECR_RESET", FelixClientThreadExtension::Cmd::ECR_RESET)
        .export_values();

    // enum Status
    py::enum_<FelixClientThreadExtension::Status>(m, "FelixClientStatus", py::arithmetic())
        .value("OK", FelixClientThreadExtension::Status::OK)
        .value("ERROR", FelixClientThreadExtension::Status::ERROR)
        .value("ERROR_NO_SUBSCRIPTION", FelixClientThreadExtension::Status::ERROR_NO_SUBSCRIPTION)
        .value("ERROR_NO_CONNECTION", FelixClientThreadExtension::Status::ERROR_NO_CONNECTION)
        .value("ERROR_NO_REPLY", FelixClientThreadExtension::Status::ERROR_NO_REPLY)
        .value("ERROR_INVALID_CMD", FelixClientThreadExtension::Status::ERROR_INVALID_CMD)
        .value("ERROR_INVALID_ARGS", FelixClientThreadExtension::Status::ERROR_INVALID_ARGS)
        .value("ERROR_INVALID_REGISTER", FelixClientThreadExtension::Status::ERROR_INVALID_REGISTER)
        .value("ERROR_NOT_AUTHORIZED", FelixClientThreadExtension::Status::ERROR_NOT_AUTHORIZED)
        .export_values();

    py::bind_vector<std::vector<FelixClientThreadExtension::Reply>>(m, "FelixClientReplyVector");

    // struct Reply
    py::class_<FelixClientThreadExtension::Reply, std::shared_ptr<FelixClientThreadExtension::Reply>> reply(m, "FelixClientReply");
    reply.def(py::init<>())
        .def_readwrite("ctrl_fid", &FelixClientThreadExtension::Reply::ctrl_fid)
        .def_readwrite("status", &FelixClientThreadExtension::Reply::status)
        .def_readwrite("value", &FelixClientThreadExtension::Reply::value)
        .def_readwrite("message", &FelixClientThreadExtension::Reply::message)
    ;

    // struct Config
    py::class_<FelixClientThreadInterface::Config, std::shared_ptr<FelixClientThreadInterface::Config>> config(m, "FelixClientConfig");
    // Config()
    config.def(py::init<>())
        .def("on_data_callback",
            [](FelixClientThreadInterface::Config &self,
            std::function<void (uint64_t fid, py::bytes data, uint8_t status)> on_data_callback) {
                python_on_data_callback = on_data_callback;
                self.on_data_callback = cxx_on_data_callback;
            },
            py::arg("on_data_callback"))
        .def("on_init_callback",
            [](FelixClientThreadInterface::Config &self,
            std::function<void (void)> on_init_callback) {
                self.on_init_callback = on_init_callback;
            },
            py::arg("on_init_callback"))
        .def("on_connect_callback",
            [](FelixClientThreadInterface::Config &self,
            std::function<void (uint64_t fid)> on_connect_callback) {
                self.on_connect_callback = on_connect_callback;
            },
            py::arg("on_connect_callback"))
        .def("on_disconnect_callback",
            [](FelixClientThreadInterface::Config &self,
            std::function<void (uint64_t fid)> on_disconnect_callback) {
                self.on_disconnect_callback = on_disconnect_callback;
            },
            py::arg("on_disconnect_callback"))
        .def_readwrite("property", &FelixClientThreadInterface::Config::property)
    ;

    // class FelixClient()
    py::class_<FelixClientThreadImpl, std::shared_ptr<FelixClientThreadImpl>> fct(m, "FelixClientThread");
    fct.def(py::init<FelixClientThreadInterface::Config&>()) // , py::call_guard<py::gil_scoped_release>())
        // void send_data(uint64_t fid, const uint8_t* data, size_t size, bool flush) override;
        .def("send_data",
            [](FelixClientThreadImpl &self, uint64_t fid, py::bytes &bytes, bool flush) {
                py::buffer_info info(py::buffer(bytes).request());
                const uint8_t* data = reinterpret_cast<const uint8_t*>(info.ptr);
                size_t size = static_cast<size_t>(info.size);
                self.send_data(fid, data, size, flush);
            },
            py::call_guard<py::gil_scoped_release>(),
            py::arg("fid"), py::arg("bytes"), py::arg("flush"))

        // void send_data(netio_tag_t fid, const std::vector<const uint8_t*>& msgs, const std::vector<size_t>& sizes) override;
        .def("send_data",
            [](FelixClientThreadImpl &self, netio_tag_t fid, const std::vector<py::bytes> &list_of_bytes) {
                std::vector<const uint8_t*> msgs;
                std::vector<size_t> sizes;

                for(auto bytes : list_of_bytes) {
                    py::buffer_info info(py::buffer(bytes).request());
                    const uint8_t* data = reinterpret_cast<const uint8_t*>(info.ptr);
                    size_t size = static_cast<size_t>(info.size);

                    msgs.push_back(data);
                    sizes.push_back(size);
                }
                self.send_data(fid, msgs, sizes);
            },
            py::call_guard<py::gil_scoped_release>(),
            py::arg("fid"), py::arg("lis_of_bytes"))

        // void send_data_nb(uint64_t fid, const uint8_t* data, size_t size, bool flush) override;
        .def("send_data_nb",
            [](FelixClientThreadImpl &self, uint64_t fid, py::bytes &bytes, bool flush) {
                py::buffer_info info(py::buffer(bytes).request());
                const uint8_t* data = reinterpret_cast<const uint8_t*>(info.ptr);
                size_t size = static_cast<size_t>(info.size);
                self.send_data_nb(fid, data, size, flush);
            },
            py::call_guard<py::gil_scoped_release>(),
            py::arg("fid"), py::arg("bytes"), py::arg("flush"))

        // void send_data(netio_tag_t fid, const std::vector<const uint8_t*>& msgs, const std::vector<size_t>& sizes) override;
        .def("send_data_nb",
            [](FelixClientThreadImpl &self, netio_tag_t fid, const std::vector<py::bytes> &list_of_bytes) {
                std::vector<const uint8_t*> msgs;
                std::vector<size_t> sizes;

                for(auto bytes : list_of_bytes) {
                    py::buffer_info info(py::buffer(bytes).request());
                    const uint8_t* data = reinterpret_cast<const uint8_t*>(info.ptr);
                    size_t size = static_cast<size_t>(info.size);

                    msgs.push_back(data);
                    sizes.push_back(size);
                }
                self.send_data_nb(fid, msgs, sizes);
            },
            py::call_guard<py::gil_scoped_release>(),
            py::arg("fid"), py::arg("lis_of_bytes"))

        // void subscribe(uint64_t fid) override;
        .def("subscribe", py::overload_cast<uint64_t>(&FelixClientThreadImpl::subscribe),
            py::call_guard<py::gil_scoped_release>(),
            py::arg("fid"))

        // void subscribe(const std::vector<uint64_t>& fids) override;
        .def("subscribe", py::overload_cast<const std::vector<uint64_t>&>(&FelixClientThreadImpl::subscribe),
            py::call_guard<py::gil_scoped_release>(),
            py::arg("fids"))

        // void unsubscribe(uint64_t fid) override;
        .def("unsubscribe", py::overload_cast<uint64_t>(&FelixClientThreadImpl::unsubscribe),
            py::call_guard<py::gil_scoped_release>(),
            py::arg("fid"))

	    // void exec(const UserFunction &user_function) override;
        .def("exec", &FelixClientThreadImpl::exec,
            py::call_guard<py::gil_scoped_release>(),
            py::arg("function"))

        // void init_send_data(uint64_t fid) override;
        .def("init_send_data", &FelixClientThreadImpl::init_send_data,
            py::call_guard<py::gil_scoped_release>(),
            py::arg("fid"))

        // void init_subscribe(uint64_t fid) override;
        .def("init_subscribe", &FelixClientThreadImpl::init_subscribe,
            py::call_guard<py::gil_scoped_release>(),
            py::arg("fid"))

        // Status send_cmd(const std::vector<uint64_t>& fids, Cmd cmd, const std::vector<std::string>& cmd_args, std::vector<Reply>& replies) override;
        .def("send_cmd",
            [](FelixClientThreadImpl &self, const std::vector<uint64_t>& fids, FelixClientThreadExtension::Cmd cmd, const std::vector<std::string>& cmd_args) {
                std::vector<FelixClientThreadExtension::Reply> replies;
                FelixClientThreadExtension::Status status = self.send_cmd(fids, cmd, cmd_args, replies);

                // for (auto reply : replies) {
                //     std::cout << std::hex << reply.ctrl_fid << std::endl;
                //     std::cout << reply.status << std::endl;
                //     std::cout << reply.message << std::endl;
                //     std::cout << reply.value << std::endl;
                // }
                return std::make_tuple(status, replies);
            },
            py::return_value_policy::reference,
            py::call_guard<py::gil_scoped_release>(),
            py::arg("fids"), py::arg("cmd"), py::arg("cmd_args"))

        // void user_timer_start(unsigned long interval) override;
        .def("user_timer_start", &FelixClientThreadImpl::user_timer_start,
            py::call_guard<py::gil_scoped_release>(),
            py::arg("interval"))

        // void user_timer_stop() override;
        .def("user_timer_stop", &FelixClientThreadImpl::user_timer_stop,
            py::call_guard<py::gil_scoped_release>())

        // void callback_on_user_timer(OnUserTimerCallback on_user_timer_cb) override;
        .def("callback_on_user_timer", &FelixClientThreadImpl::callback_on_user_timer,
            py::call_guard<py::gil_scoped_release>(),
            py::arg("on_user_timer_cb"))

    ;
}
