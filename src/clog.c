/*
 * Copyright (c) 2020 rxi
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "clog.h"
#include <math.h>
#include <sys/time.h>

#define MAX_CALLBACKS 32

typedef struct {
  clog_LogFn fn;
  void *udata;
  int level;
} CCallback;

static struct {
  void *udata;
  clog_LockFn lock;
  int level;
  bool quiet;
  CCallback callbacks[MAX_CALLBACKS];
} CL;


static const char *clevel_strings[] = {
  "FATAL", "ERROR", "WARN", "INFO", "DEBUG", "TRACE"
};


#ifdef CLOG_USE_COLOR
static const char *clevel_colors[] = {
  "\x1b[35m", "\x1b[31m", "\x1b[33m", "\x1b[32m", "\x1b[36m", "\x1b[94m"
};
#endif


static void cstdout_callback(clog_Event *ev) {
  char buf[20];
  struct timeval t;
  int msec;
  buf[strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S", ev->time)] = '\0';
  gettimeofday(&t, NULL);
  msec = lrint(t.tv_usec/1000.0);
  char buffer[24];
  snprintf(buffer, sizeof(buffer), "%s.%03d", buf, msec);
#ifdef CLOG_USE_COLOR
  fprintf(
    ev->udata, "%s %s%-5s\x1b[0m \x1b[90m%s %s:%d:\x1b[0m ",
    buffer, clevel_colors[ev->level], clevel_strings[ev->level], "client",
    ev->file, ev->line);
#else
  fprintf(
    ev->udata, "%s %-5s %s %s:%d: ",
    buffer, clevel_strings[ev->level], "client", ev->file, ev->line);
#endif
  vfprintf(ev->udata, ev->fmt, ev->ap);
  fprintf(ev->udata, "\n");
  fflush(ev->udata);
}


static void cfile_callback(clog_Event *ev) {
  char buf[64];
  buf[strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S", ev->time)] = '\0';
  fprintf(
    ev->udata, "%s %-5s %s:%d: ",
    buf, clevel_strings[ev->level], ev->file, ev->line);
  vfprintf(ev->udata, ev->fmt, ev->ap);
  fprintf(ev->udata, "\n");
  fflush(ev->udata);
}


static void clog_lock(void)   {
  if (CL.lock) { CL.lock(true, CL.udata); }
}


static void clog_unlock(void) {
  if (CL.lock) { CL.lock(false, CL.udata); }
}


const char* clog_level_string(int level) {
  return clevel_strings[level];
}


void clog_set_lock(clog_LockFn fn, void *udata) {
  CL.lock = fn;
  CL.udata = udata;
}


void clog_set_level(int level) {
  CL.level = CLOG_INFO;
  clog_info("Setting log_level to %s", clog_level_string(level));
  CL.level = level;
}

int clog_level() {
  return CL.level;
}

void clog_set_quiet(bool enable) {
  CL.quiet = enable;
}


int clog_add_callback(clog_LogFn fn, void *udata, int level) {
  for (int i = 0; i < MAX_CALLBACKS; i++) {
    if (!CL.callbacks[i].fn) {
      CL.callbacks[i] = (CCallback) { fn, udata, level };
      return 0;
    }
  }
  return -1;
}


int clog_add_fp(FILE *fp, int level) {
  return clog_add_callback(cfile_callback, fp, level);
}


static void cinit_event(clog_Event *ev, void *udata) {
  if (!ev->time) {
    time_t t = time(NULL);
    ev->time = localtime(&t);
  }
  ev->udata = udata;
}


void clog_log(int level, const char *file, int line, const char *fmt, ...) {
  clog_Event ev = {
    .fmt   = fmt,
    .file  = file,
    .line  = line,
    .level = level,
  };

  clog_lock();

  if (!CL.quiet && level <= CL.level) {
    if ( level <= CLOG_WARN){
      cinit_event(&ev, stderr);
    } else {
      cinit_event(&ev, stdout);
    }
    va_start(ev.ap, fmt);
    cstdout_callback(&ev);
    va_end(ev.ap);
  }

  for (int i = 0; i < MAX_CALLBACKS && CL.callbacks[i].fn; i++) {
    CCallback *cb = &CL.callbacks[i];
    if (level <= cb->level) {
      cinit_event(&ev, cb->udata);
      va_start(ev.ap, fmt);
      cb->fn(&ev);
      va_end(ev.ap);
    }
  }

  clog_unlock();
}
