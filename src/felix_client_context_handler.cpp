#include "felix/felix_client_context_handler.hpp"
#include "felix/felix_client_exception.hpp"

#include "clog.h"

/**
 * @brief Get netio_socket_type by FID
 *
 * The information is stored in the info object or this FID and is read from the bus when the info object is created.
 * @param fid: FID
 */
netio_socket_type FelixClientContextHandler::getType(uint64_t fid){
    return infoByFid[fid]->getType();
}

/**
 * @brief Get netio_socket_type from socket
 *
 * This function returns the netio_socket_type of a void pointer to a socket.
 * If the socket does not exists, it returns BUFFERED since it has to have a valid return value.
 * @param socket: void pointer to a netio_socket
 */
netio_socket_type FelixClientContextHandler::getType(void* socket){
    return std::visit(
    overloaded{[](netio_subscribe_socket& s) {
            return BUFFERED;
        },
        [](netio_unbuffered_subscribe_socket& s) {
            return UNBUFFERED;
        },
        [](netio_send_socket& s) {
            return UNBUFFERED;
        },
        [](netio_buffered_send_socket& s) {
            return BUFFERED;
        },
        [](std::monostate& s) {
            return BUFFERED;
        },
    }, *socketContextMap[socket]->socket);
}

/**
 * @brief Get netio_socket_connection by FID
 *
 * The information is stored in the info object or this FID and is read from the bus when the info object is created.
 * A connection type can either be of type PUBSUB or SENDRECV.
 * @param fid: FID
 */
netio_socket_connection FelixClientContextHandler::getConnectionType(uint64_t fid){
    return infoByFid[fid]->getConnectionType();
}

/**
 * @brief Get netio_socket_connection from socket
 *
 * This function returns the netio_socket_connection of a void pointer to a socket.
 * A connection type can either be of type PUBSUB or SENDRECV.
 * If the socket does not exists, it returns SENDRECV since it has to have a valid return value.
 * @param socket: void pointer to a netio_socket
 */
netio_socket_connection FelixClientContextHandler::getConnectionType(void* socket){
    return std::visit(
    overloaded{[](netio_subscribe_socket& s) {
            return PUBSUB;
        },
        [](netio_unbuffered_subscribe_socket& s) {
            return PUBSUB;
        },
        [](netio_send_socket& s) {
            return SENDRECV;
        },
        [](netio_buffered_send_socket& s) {
            return SENDRECV;
        },
        [](std::monostate& s) {
            return SENDRECV;
        },
    }, *socketContextMap[socket]->socket);
}

/**
 * @brief Get netio_socket_state by FID
 *
 * Returns the state of a socket that is associated to this FID. If the FID has no associated socket the function returns DISCONNECTED.
 * We must portect this call with a mutex since send_sockets are removed fromt he socketContextMap when a connection is closed.
 * @param fid: FID
 */
netio_socket_state FelixClientContextHandler::getSocketState(uint64_t fid){
    std::unique_lock<std::mutex> lck(send_mtx);
    SocketWrapper* sw = infoByFid[fid]->getSocket();
    return std::visit(
        overloaded{[](std::monostate& s) {
                return DISCONNECTED;
            },[this](auto& s){
               return socketContextMap[static_cast<void*>(&s)]->state.load();
            }
        }, *sw);
}

/**
 * @brief Get subscribe_state of a FID
 *
 * Returns the ssubscribe_state of a FID.
 * @param fid: FID
 */
subscribe_state FelixClientContextHandler::getSubscriptionState(uint64_t fid){
    return infoByFid[fid]->getSubscriptionState();
}

/**
 * @brief Returns a map with timestamps to measure subscription times
 *
 * The first timestamp is taken in the scubribe function, called by the user, the second one when the subscribe is processed by the eventloop.
 * @param fid: FID
 */
std::unordered_map<std::string, timespec> FelixClientContextHandler::getSubscriptionTimes(uint64_t fid){
    return infoByFid[fid]->getSubscriptionTimes();
}

uint FelixClientContextHandler::getWatermark(uint64_t fid){
    return infoByFid[fid]->getWatermark();
}

uint FelixClientContextHandler::getPagesize(uint64_t fid){
    return infoByFid[fid]->getPagesize();
}

uint FelixClientContextHandler::getPages(uint64_t fid){
    return infoByFid[fid]->getPages();
}

uint FelixClientContextHandler::getPort(uint64_t fid){
    return infoByFid[fid]->getPort();
}

std::string FelixClientContextHandler::getIp(uint64_t fid){
    return infoByFid[fid]->getIp();
}

std::string FelixClientContextHandler::getAddress(uint64_t fid){
    return infoByFid[fid]->getIp() + ":" + std::to_string(infoByFid[fid]->getPort());
}

bool FelixClientContextHandler::isTcp(uint64_t fid) {
    return infoByFid[fid]->isTcp();
}

std::vector<uint64_t> FelixClientContextHandler::getFidsToResubscribe(){
    return mResubFids;
}

std::vector<uint64_t> FelixClientContextHandler::getFidsBySocket(void* socket){
    return socketContextMap[socket]->subFids;
}


/**
 * @brief Get vector of all subscribed FIDs
 *
 * This function returns a vector of all FIDs that are subscribed via a subscribe socket right now.
 * This list is used in the destructor to unsubscribe and close a connection properly before we delete the associated subscribe socket.
 */
std::vector<uint64_t> FelixClientContextHandler::getFidsToUnsubscribe(){
    std::vector<uint64_t> allFids;
    std::for_each(socketContextMap.begin(), socketContextMap.end(), [&allFids](auto& ctx_it){std::copy(ctx_it.second->subFids.begin(), ctx_it.second->subFids.end(), std::back_inserter(allFids));});
    std::vector<uint64_t> fidsToUnsub(allFids.size());
    auto end_it = std::copy_if(allFids.begin(), allFids.end(), fidsToUnsub.begin(), [this](auto& fid){return (this->infoByFid[fid]->getSubscriptionState() == SUB || this->infoByFid[fid]->getSubscriptionState() == SUBING)&&( this->infoByFid[fid]->getConnectionType() == PUBSUB);});
    fidsToUnsub.resize(std::distance(fidsToUnsub.begin(), end_it));
    return fidsToUnsub;
}


/**
 * @brief Get vector of all send sockets
 *
 * This function returns a vector of pointers to all send sockets that are connected right now.
 * The function iterates over all sockets in the socketContextMap and adds the pointer of netio_send_socket (also in buffered case) to the vector.
 * This list is used in the destructor to properly shutdown the connection to the server side via netio_disconnect() before deleting the socket object.
 */
std::vector<netio_send_socket*> FelixClientContextHandler::getSendSockets(){
    std::vector<netio_send_socket*> sendSockets;
    for (auto& ctx_it : socketContextMap){
        netio_send_socket* socket = std::visit(
        overloaded{[](netio_subscribe_socket& s) {
                return static_cast<netio_send_socket*>(NULL);
            },
            [](netio_unbuffered_subscribe_socket& s) {
                return static_cast<netio_send_socket*>(NULL);
            },
            [](netio_send_socket& s) {
                return &s;
            },
            [](netio_buffered_send_socket& s) {
                return &(s.send_socket);
            },
            [](std::monostate& s) {
                return static_cast<netio_send_socket*>(NULL);
            },
        }, *ctx_it.second->socket);
        if(socket){
            sendSockets.push_back(socket);
        }
    }
    return sendSockets;
}

/**
 * @brief Returns a map of void pointers to subscribe socket and the associated netio_socket_type
 *
 * Since we need to distinguish between buffered and unbuffered subscribe sockets when we want to clean them up, this function returns void pointer to
 * all subscribe sockets and their netio_socket_type stored in map.
 * This map/list is used in the destructor to properly shutdown the subscribe socket after all FIDs are unsubscribed.
 */
std::unordered_map<void*, netio_socket_type> FelixClientContextHandler::getSubSocketsToDelete(){
    std::unordered_map<void*, netio_socket_type> voidSubSocketsToDelete;
    for (auto& it :mSubSocketsToDelete){
        netio_socket_type type = std::visit(
            overloaded{[](netio_subscribe_socket& s) {
                return BUFFERED;
            },
            [](netio_unbuffered_subscribe_socket& s) {
                return UNBUFFERED;
            },
            [](netio_send_socket& s) {
                return UNBUFFERED;
            },
            [](netio_buffered_send_socket& s) {
                return BUFFERED;
            },
            [](std::monostate& s) {
                return BUFFERED;
            },
        }, *it);
        voidSubSocketsToDelete[mGetVoidSocketPointer(*it)] = type;
    }
    return voidSubSocketsToDelete;
}


bool FelixClientContextHandler::isForRegister(uint64_t fid){
    return infoByFid[fid]->getForRegister();
}

/**
 * @brief Returns true is FID is subscribed
 *
 * @param fid: FID
 */
bool FelixClientContextHandler::isSubscribed(uint64_t fid){
    if (infoByFid.count(fid) > 0){
        return (infoByFid[fid]->getSubscriptionState() == SUB);
    } else {
        return false;
    }
}

/**
 * @brief Function blocks until all FIDs are subscribed or the timeout expired.
 *
 * The function can be used to wait for the subscription of one or more FIDs was successful or the timeot expired.
 * To achieve this, the function adds a multiWaitCtx object to all FID info objects, which has in internal counter that is increased for every
 * subscription that was succesful. The conditional variale "multiWaitCtx->allSubscribed" uses the std function wait_for to determine if the timeout expires
 * or the condition is met.
 * Note: The wait_for() function requires a unique_lock on a mutex. Since we do not realy need to synchronise something, it is sufficient to use a local mutex/lock
 * @param fids: vector fo FIDs tha are checked for their subscription state
 * @param timeoutMs:timeout in ms after which the funciton returns even when not all FIDs are subscribed.
 */
bool FelixClientContextHandler::isSubscribed(std::vector<uint64_t> fids, uint64_t timeoutMs){
    auto multiWaitCtx = std::make_unique<MultiFidsWaitContext>();
    multiWaitCtx->expectedSize = fids.size();
    for(auto fid : fids){
        if (infoByFid.count(fid) > 0){
            if(infoByFid[fid]->getSubscriptionState() != SUB){
                infoByFid[fid]->setWaitContext(multiWaitCtx.get());
            }
            else{multiWaitCtx->count++;}
        }
    }
    clog_debug("Waiting for %d fids to subscribe. Count: %d", multiWaitCtx->expectedSize, multiWaitCtx->count);
    bool status = true;
    if(multiWaitCtx->count < multiWaitCtx->expectedSize ){
        auto timeExpiredMs = std::chrono::milliseconds(0);
        std::mutex m;
        std::unique_lock<std::mutex> lck(m);
        auto t0 = std::chrono::steady_clock::now();
        while (multiWaitCtx->count != multiWaitCtx->expectedSize && timeExpiredMs < std::chrono::milliseconds(timeoutMs)){
            status = (multiWaitCtx->allSubscribed.wait_for(lck, std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::milliseconds(timeoutMs) - timeExpiredMs)) == std::cv_status::no_timeout);
            timeExpiredMs = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - t0);
        }
    }
    for (auto fid : fids){
        infoByFid[fid]->clearWaitContext();
    }
    return status;
}

/**
 * @brief Checks if all FIDs are unsubscribed
 *
 * Since this function iterates over all sockets, we need to make sure that no socket is removed from the socketContextMap while we are are iterting.
 * Because the socketContextMap contains sockets of all types, a scoped_lock is used to lock the send_mtx and sub_mtx.
 */
bool FelixClientContextHandler::areAllFidsUnsubscribed(){
    std::scoped_lock lock(send_mtx, sub_mtx);
    for (auto& ctx_it : socketContextMap){
        clog_debug("socket 0x%p has state 0x%p", ctx_it.first, netio_socket_state_name(ctx_it.second->state.load()));
        if(ctx_it.second->state != DISCONNECTED && getConnectionType(ctx_it.first) == PUBSUB){
            return false;
        }
    }
    return true;
}


/**
 * @brief Checks if all FIDs associated with a particular socket are unsubscribed
 *
 * This function checks if there are FIDs associated to the socket, that are not ualready nscubscribed or currently unsubscribing.
 * @param socket: void pointer to a netio_socket. (should be a subscribe_socket but will also work with send_socket)
 */
bool FelixClientContextHandler::areAllFidsUnsubscribed(void* socket){
    auto fids = getFidsBySocket(socket);
    int i = std::count_if(fids.begin(), fids.end(), [this](auto& fid){return (this->infoByFid[fid]->getSubscriptionState() != UNSUB && this->infoByFid[fid]->getSubscriptionState() != UNSUBING);});
    clog_debug("Remaining subscritions: %d", i);
    return i;
}


/**
 * @brief Check if felix-client can subscribe to this FID
 *
 * FIDs are not eligible to subscribe if we don't have a felix_client_info object for this FID or if the FID was unsubscribed before.
 *
 * TODO:Maybe we also need to check what we want to do here if the FID is in the process of unsubscribing but not done yet
 *
 * @param fid: FID
 */
bool FelixClientContextHandler::canSubscribe(uint64_t fid){
    if (infoByFid.count(fid) == 0 || (infoByFid[fid]->getSubscriptionState() == UNSUB)) {
        return true;
    } else {
        return false;
    }
}


/**
 * @brief Checks if all felix_client_info object for this FID already exists.
 *
 * @param fid: FID
 */
bool FelixClientContextHandler::exists(uint64_t fid){
    return infoByFid.count(fid);
}

/**
 * @brief checks if all socket for this void pointer still exists.
 *
 * @param socket: void pointer to socket of any type.
 */
bool FelixClientContextHandler::exists(void* socket){
    return socketContextMap.count(socket);
}


/**
 * @brief Function that waits for a socket associated to this FID to be connected or timeout
 *
 * This function retrives the socket associated to this FID and checks its connection state.
 * If it is currently connecting, it will wait on a condition variable that is notified in the on_connection_establiched() callback.
 * If hte connection is not established within the spcified timeout, the function returns fals for not connected.
 *
 * @param fid: FID
 * @param timeoutMs: timeout [ms]
 *
 */
bool FelixClientContextHandler::waitConnected(uint64_t fid, uint64_t timeoutMs){
    if (infoByFid.count(fid) > 0){
        SocketWrapper* sw = infoByFid[fid]->getSocket();
        return std::visit(
        overloaded{[](std::monostate& s) {
                return false;
            },[this, timeoutMs, fid](auto& s){
                if(socketContextMap[static_cast<void*>(&s)].get()->state.load()==CONNECTED){
                    return true;
                } else if(socketContextMap[static_cast<void*>(&s)].get()->state.load()==DISCONNECTED){
                    return false;
                }else{
                    auto timeExpiredMs = std::chrono::milliseconds(0);
                    std::unique_lock<std::mutex> lck(socketContextMap[static_cast<void*>(&s)].get()->mMux);
                    clog_debug("Start waiting, fid: 0x%x with socket 0x%p", fid, socketContextMap[static_cast<void*>(&s)].get());
                    auto t0 = std::chrono::steady_clock::now();
                    while(socketContextMap[static_cast<void*>(&s)].get()->state.load()==CONNECTING && timeExpiredMs < std::chrono::milliseconds(timeoutMs)){
                        socketContextMap[static_cast<void*>(&s)].get()->mConnectionWait.wait_for(lck, std::chrono::milliseconds(timeoutMs));
                        timeExpiredMs = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - t0);
                    }
                    return (socketContextMap[static_cast<void*>(&s)].get()->state.load() == CONNECTED);
                }
            }
        }, *sw);
    } else {
        return false;
    }
}

void FelixClientContextHandler::setSubscriptionTimes(uint64_t fid, std::string name, timespec ts){
    return infoByFid[fid]->setSubscriptionTimes(name, ts);
}

/**
 * @brief Setting socket state
 *
 * If the new state is CONNECTED or DISCONNECTED, the a condition variable is notified to inform the subscribing thread if the operation was successful or not
 *
 * @param fid: FID
 * @param s: New socket state
 *
 */
void FelixClientContextHandler::setSocketState(uint64_t fid, netio_socket_state s){
    if (infoByFid.count(fid) > 0){
        SocketWrapper* sw = infoByFid[fid]->getSocket();
        clog_debug("For socket: 0x%p new state %d", mGetVoidSocketPointer(*sw), s);
        socketContextMap[mGetVoidSocketPointer(*sw)]->state = s;
        if (s == CONNECTED || s == DISCONNECTED){socketContextMap[mGetVoidSocketPointer(*sw)]->mConnectionWait.notify_all();}
    }
}

/**
 * @brief Setting socket state
 *
 * If the new state is CONNECTED or DISCONNECTED, the a condition variable is notified to inform the subscribing thread if the operation was successful or not
 *
 * @param socket: void pointer to a netio_socket
 * @param s: New socket state
 *
 */
void FelixClientContextHandler::setSocketState(void* socket, netio_socket_state s){
    socketContextMap[socket]->state = s;
    if (s == CONNECTED || s == DISCONNECTED){socketContextMap[socket]->mConnectionWait.notify_all();}
}

void FelixClientContextHandler::setSubscriptionState(uint64_t fid, subscribe_state s){
    infoByFid[fid]->setSubscriptionState(s);
}


/**
 * @brief Adds FID to a set of FIDs that need to subscribe
 *
 * FIDs get either added to the list during the first subscription, if they are not subcribed yet or if the server side shut down the connection.
 * Felix-client will try to subscribe all FIDs in this list periodically until the subscription is succesful.
 *
 * @param fid: FID
 */
void FelixClientContextHandler::addToFidsToResubscribe(uint64_t fid){
    auto it = std::find(mResubFids.begin(), mResubFids.end(), fid);
    if (it == mResubFids.end()){
        mResubFids.push_back(fid);
    }
}

void FelixClientContextHandler::removeFromFidsToResubscribe(uint64_t fid){
    auto it = std::find(mResubFids.begin(), mResubFids.end(), fid);
    if (it != mResubFids.end()){
        mResubFids.erase(it);
    }else{
        clog_debug("Fid not foud in mReSubFids");
    }
}

/**
 * @brief Remove socket object
 *
 * This function removes sockets form the socketContextMaps and thereby destroying it.
 * Different cases need to be considered to take care of all references to this socket and to update the corresponding information.
 *
 * SubSocket objects must not be destroyed here, since they contain a send and listen socket that are not cleaned up yet.
 * Instead all references to the socket are removed and they object is put into a different list to destroy it later.
 *
 * @param socket: void pointer to a netio_socket
 * @param connection_refused: Status variable if the socket is removed because the connection was refused
 */
void FelixClientContextHandler::removeSocket(void* socket, bool connection_refused){
    auto socketFids = getFidsBySocket(socket);
    if (getConnectionType(socket) == PUBSUB ){
        for (auto& fid : socketFids)
        {
            if(!terminatingFelixClient.load()){ //necessary?
                auto it = std::find(mResubFids.begin(), mResubFids.end(), fid);
                if (it == mResubFids.end()){
                    mResubFids.push_back(fid);
                }
            }else{
                clog_debug("Already terminating felix-client not adding fids to mResubFids");
            }
            infoByFid.at(fid)->removeSocket();
            infoByFid.at(fid)->setSubscriptionState(UNSUB);
        }

        if(!connection_refused){
            mSubSocketsToDelete.push_back(std::move(socketContextMap[socket]->socket));
        }
        std::unique_lock<std::mutex> lock(sub_mtx);
        socketContextMap.erase(socket);
    } else {
        {
            std::unique_lock<std::mutex> lck(socketContextMap[socket].get()->mMux);
            for (auto& fid : socketFids)
            {
                if(!terminatingFelixClient.load()){
                    auto it = std::find(mResubFids.begin(), mResubFids.end(), fid);
                    if (it == mResubFids.end()){
                        mResubFids.push_back(fid);
                    }
                }else{
                    clog_debug("Already terminating felix-client not adding fids to mResubFids");
                }
                infoByFid.at(fid)->removeSocket();
            }
        }
        std::unique_lock<std::mutex> lock(send_mtx);
        socketContextMap.erase(socket);
    }
}


/**
 * @brief Update all information related to a subscription when unsubscribing
 *
 * This function erases the FID from the corresponding socket and the socket is removed from the fid_info.
 *
 * @param fid: FID
 */
void FelixClientContextHandler::updateFidsWhenUnsub(uint64_t fid){
    auto fids = &socketContextMap[mGetVoidSocketPointer(*infoByFid[fid]->getSocket())].get()->subFids;
    auto it = std::find(fids->begin(), fids->end(), fid);
    if (it != fids->end()){
        fids->erase(it);
        infoByFid[fid]->removeSocket();
    }else{
        clog_debug("Fid not foud in SubFids");
    }
}


/**
 * @brief Updates the info from bus (usually before subscribing) or creates new felix_client_info object if it does not exist yet
 *
 * @param fid: FID
 * @param bus: pointer to FelixBus object
 * @param forRegister: determines if FID is used to get info from register (which assigns a different callback)
 */
void FelixClientContextHandler::createOrUpdateInfo(uint64_t fid, felixbus::FelixBus* bus, bool forRegister){
    if (infoByFid.count(fid)) {
        if((fid & 0x8000) && getSocketState(fid) != DISCONNECTED){
            return;
        }
        try {
            mUpdateInfo(fid, bus);
        } catch(std::exception& e){
            clog_info("Error while updating info from bus: %s. FID: 0x%x", e.what(), fid);
            throw;
        }

    } else {
        try {
            mCreateInfo(fid, bus);
        } catch(std::exception& e){
            clog_info("Error while creating info from bus: %s. FID: 0x%x", e.what(), fid);
            throw FelixClientResourceNotAvailableException();
        }
    }
    if(forRegister){
        infoByFid[fid]->setForRegister(forRegister);
    }
}

/**
 * @brief Creates new felix_client_info object
 *
 * The information is retrieved from the bus file. A mutex prevents concurrent access to the bus object.
 * Additionally time for the bus read is meassured and a warning is printed is the access took more than 1s.
 *
 * @param fid: FID
 * @param bus: pointer to FelixBus object
 */
void FelixClientContextHandler::mCreateInfo(uint64_t fid, felixbus::FelixBus* bus){
    std::error_code ec;
    felixbus::FelixBusInfo bus_info;
    {
        struct timespec t0, t1;
        clock_gettime(CLOCK_MONOTONIC_RAW, &t0);
        std::lock_guard<std::mutex> lock(info_mtx);
        bus_info = bus->get_info(fid, ec);
        clock_gettime(CLOCK_MONOTONIC_RAW, &t1);
        double bus_useconds = (t1.tv_sec - t0.tv_sec)*1e6 + (t1.tv_nsec - t0.tv_nsec)*1e-3;
        if(bus_useconds > 1000000){
            clog_warn("Bus access took more than one second: %dus.", bus_useconds);
        }
    }
    if (!ec) {
        infoByFid[fid] = std::make_unique<FelixClientContext>();
        if(bus_info.ip == ""){throw std::invalid_argument("Elink not in bus");}
        infoByFid[fid]->setInfos(bus_info.ip, bus_info.port, bus_info.netio_pages, bus_info.netio_pagesize, 0.9 * bus_info.netio_pagesize, bus_info.unbuffered, bus_info.pubsub, bus_info.raw_tcp);
    } else {
        throw FelixClientResourceNotAvailableException();//d::exception(ec.message());
    }
    clog_debug("Connection: %d type: %d for FID: 0x%x", bus_info.pubsub, bus_info.unbuffered, fid);
    clog_debug("IP: %s Port: %d", bus_info.ip.c_str(), bus_info.port);
}

/**
 * @brief Updates felix_client_info object
 *
 * The information is retrieved from the bus file. A mutex prevents concurrent access to the bus object.
 * Additionally time for the bus read is meassured and a warning is printed is the access took more than 1s.
 *
 * @param fid: FID
 * @param bus: pointer to FelixBus object
 */
void FelixClientContextHandler::mUpdateInfo(uint64_t fid, felixbus::FelixBus* bus){
    std::error_code ec;
    felixbus::FelixBusInfo bus_info;
    {
        struct timespec t0, t1;
        clock_gettime(CLOCK_MONOTONIC_RAW, &t0);
        std::lock_guard<std::mutex> lock(info_mtx);
        bus_info = bus->get_info(fid, ec);
        clock_gettime(CLOCK_MONOTONIC_RAW, &t1);
        double bus_useconds = (t1.tv_sec - t0.tv_sec)*1e6 + (t1.tv_nsec - t0.tv_nsec)*1e-3;
        if(bus_useconds > 1000000){
            clog_warn("Bus access took more than one second: %dus.", bus_useconds);
        }
    }
    if (!ec) {
        auto info = std::move(infoByFid[fid]);
        if (info.get() == NULL){
            mCreateInfo(fid, bus);
            return;
        }
        if(bus_info.ip == ""){throw std::invalid_argument("Elink not in bus");}
        info->setInfos(bus_info.ip, bus_info.port, bus_info.netio_pages, bus_info.netio_pagesize,  0.9 * bus_info.netio_pagesize, bus_info.unbuffered, bus_info.pubsub, bus_info.raw_tcp);
        infoByFid[fid] = std::move(info);
    } else {
        clog_error("Error while reading bus: %s", ec.message().c_str());
        throw FelixClientResourceNotAvailableException();
    }
}

void FelixClientContextHandler::setSocketToAllUnsubscribed(void* socket){
    socketContextMap[socket]->closingConnection = true;
}

bool FelixClientContextHandler::addOrCreateSocket(uint64_t fid){ //returns true if a new socket was created and needs to be initialized
    if(mAddSocket(fid)){return false;}
    else {mCreateSocket(fid);}
    return true;
}

/**
 * @brief Adds socket to felix_client_info
 *
 * Sockets are identfied by the endpoint they connect to. If a socket to the requested endpoint already exists, this socket is
 * added to the felix_client_info object of the particular FID. The stats is set to SUB automatically, since the subscription should
 * be fast and there is no exact feedback from the derver (felix-tohost) side when the subscription is actually processed.
 *
 * @param fid: FID
 */
bool FelixClientContextHandler::mAddSocket(uint64_t fid){
    std::string fid_address = getAddress(fid);
    //Socket already exists
    for(auto&& ctx : socketContextMap){
        if(ctx.second->address == fid_address && !ctx.second->closingConnection){
            infoByFid[fid]->setSocket(ctx.second->socket.get());
            infoByFid[fid]->setSubscriptionState(SUB);
            ctx.second->subFids.push_back(fid);
            return true;
        }
    }
    return false;
}

/**
 * @brief Create new socket
 *
 * The type of socket is determined by the felix-bus and the information provided for a particular FID.
 * Afterwars the socket is added to the socketContextMap that holds all socket objects.
 *
 * @param fid: FID
 */
void FelixClientContextHandler::mCreateSocket(uint64_t fid){
    std::unique_ptr<SocketWrapper> sw_ptr;
    if(infoByFid[fid]->getType() == BUFFERED && infoByFid[fid]->getConnectionType() == PUBSUB){
        sw_ptr = std::make_unique<SocketWrapper>(SocketWrapper{netio_subscribe_socket{}});
    } else if (infoByFid[fid]->getType() == UNBUFFERED && infoByFid[fid]->getConnectionType() == PUBSUB){
        sw_ptr = std::make_unique<SocketWrapper>(SocketWrapper{netio_unbuffered_subscribe_socket{}});
    }else if(infoByFid[fid]->getType() == BUFFERED && infoByFid[fid]->getConnectionType() == SENDRECV){
        sw_ptr = std::make_unique<SocketWrapper>(SocketWrapper{netio_buffered_send_socket{}});
    } else if(infoByFid[fid]->getType() == UNBUFFERED && infoByFid[fid]->getConnectionType() == SENDRECV){
        sw_ptr = std::make_unique<SocketWrapper>(SocketWrapper{netio_send_socket{}});
    }else{
        return; //should not enter here
    }
    socketContextMap[static_cast<void*>(sw_ptr.get())] = std::make_unique<SocketContext>();
    auto* sctx = socketContextMap[static_cast<void*>(sw_ptr.get())].get();
    sctx->state = CONNECTING;
    sctx->closingConnection = false;
    sctx->subFids = std::vector<uint64_t>{fid};
    sctx->socket = std::move(sw_ptr);
    sctx->address = getAddress(fid);
    infoByFid[fid]->setSocket(sctx->socket.get());
    clog_debug("Create Socket. FID: 0x%x. Socket 0x%p should be 0x%p", fid, sctx->socket.get(), infoByFid[fid]->getSocket());
    return;
}

/**
 * @brief Returns a void pointer to all kinds of sockets
 *
 * @param wrapper: Pointer to SocketWrapper objects that contains requested socket
 */
void* FelixClientContextHandler::mGetVoidSocketPointer(SocketWrapper& wrapper) {
    return std::visit([](auto& containedValueRef) {
        return static_cast<void*>(&containedValueRef);
    }, wrapper);
}

/**
 * @brief deletes subscribe sockets
 *
 * Subsocket objects must not be destroyed right away, when they are erased.
 * This function takes care about destroying the objects later, when it is save to do so.
 *
 * @param socket: Pointer to subscribe socket
 */
void FelixClientContextHandler::deleteSubSocket(void* socket){
    for(  std::vector<std::unique_ptr<SocketWrapper>> ::iterator iter = mSubSocketsToDelete.begin(); iter != mSubSocketsToDelete.end(); ++iter )
    {
        if( mGetVoidSocketPointer(*iter->get()) == socket )
        {
            mSubSocketsToDelete.erase(iter);
            break;
        }
    }
}

void FelixClientContextHandler::pullSendTask(send_var_t& task){
    mSendQueue->popTask(task);
}

void FelixClientContextHandler::pullSubTask(sub_var_t& task){
    mSubQueue->popTask(task);
}
