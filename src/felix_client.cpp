#include "felix/felix_client.hpp"
#include "felix/felix_client_exception.hpp"
#include "felix/felix_client_util.hpp"
#include "felix/felix_client_thread.hpp"

#include "felix/felix_fid.h"

#include "clog.h"

#include "netio/netio.h"
#include "netio/netio_tcp.h"

extern "C" {
    #include "jWrite.h"
}

#include <algorithm>
#include <uuid/uuid.h>
#include <unordered_set>
#include <numeric>
#include <regex>
#include <unistd.h>
#include <set>

FelixClient::FelixClient(const std::string& local_ip_or_interface,
                         std::string bus_dir,
                         std::string bus_group_name,
                         unsigned log_level,
                         bool verbose_bus,
                         unsigned netio_pages,
                         unsigned netio_pagesize) :
            ready(false),
            timer_delay_millis(0),
            ctx({0}),
            user_timer({0}),
            unbuffered_netio_pages(netio_pages),
            unbuffered_netio_pagesize(netio_pagesize),
            local_ip_address(get_ip_from_interface(local_ip_or_interface)) {

    // // handle here to make sure trace logging looks ok
    // local_ip_address = get_ip_from_interface(local_ip_or_interface);

    if (local_ip_address == "") {
        clog_fatal("Could not deduce the local ip address from %s", local_ip_or_interface.c_str());
        throw FelixClientException("felix-client: Could not deduce the local ip address from local_ip_or_interface");
    }

    clog_info("Local IP: %s", local_ip_address.c_str());

    clog_info("Initializing netio-next");
    netio_init(&ctx);

    clog_set_level(log_level);
    netio_set_debug_level(log_level);

    // init callback
    ctx.evloop.cb_init = on_init_eventloop;
    ctx.evloop.data = this;

    contextHandler = std::make_unique<FelixClientContextHandler>();


    send_signal = netio_signal();//(struct netio_signal*)malloc(sizeof(struct netio_signal)); // c style

    netio_signal_init(&ctx.evloop, &send_signal);
    send_signal.data = this;
    send_signal.cb = on_send_signal;


    sub_signal = netio_signal();
    netio_signal_init(&ctx.evloop, &sub_signal);
    sub_signal.data = this;
    sub_signal.cb = on_sub_signal;

    // setup timer for reconnection/resubscription
    netio_timer_init(&ctx.evloop, &subscribe_timer);
    subscribe_timer.cb = on_timer;
    subscribe_timer.data = this;
    terminating_felix_client = false;
    contextHandler->terminatingFelixClient = false;

    user_timer_init();

    clog_info("Initializing felix-bus");
    bus.set_path(bus_dir);

    bus.set_groupname(bus_group_name);
    bus.set_verbose(verbose_bus);
}

std::vector<int> FelixClient::parseCpuRange(const std::string &range) {
    static const std::regex re(R"(((\d+)-(\d+))|(\d+))");
    std::smatch sm;
    std::vector<int> data;
    for (std::string s(range); std::regex_search(s, sm, re); s = sm.suffix()) {
        if (sm[1].str().empty()) {
            data.push_back(std::stoi(sm[0]));
        } else {
            for (int i = std::stoi(sm[2]); i <= std::stoi(sm[3]); ++i) {
                data.push_back(i);
            }
        }
    }
    return data;
}

void FelixClient::set_thread_affinity(const std::string& affinity) {
    this->affinity = affinity;
}

FelixClient::~FelixClient(){
    clog_info("Cleaning up FelixClient.");
    for(auto& it : unbuf_mem_regions){
        clog_debug("Removing mem reg: 0x%x. There are %d mem regions remaining.", it.second.mr, unbuf_mem_regions.size());
        it.second.size = 0;
        free(it.second.data);
    }
}

// event loop control
void FelixClient::run() {
    if (affinity != "") {
        std::vector<int> cpus = parseCpuRange(affinity);
        if (!cpus.empty()) {
            cpu_set_t pmask;
            CPU_ZERO(&pmask);
            for (int cpu : cpus) {
                CPU_SET(cpu, &pmask);
            }
            int s = sched_setaffinity(0, sizeof(pmask), &pmask);
            if (s != 0) {
                clog_error("Setting affinity to CPU rage [%s] failed with error = %d", affinity.c_str(), s);
            }
        }
    }
    pthread_setname_np(pthread_self(), "felix-client-thread");

    evloop_thread_id = std::this_thread::get_id();
    clog_info("In run: 0x%x", evloop_thread_id);
    clog_info("Running netio_run");
    netio_run(&ctx.evloop);
    clog_info("Finished netio_run");
}

void FelixClient::stop() {
    clog_info("Stopping netio");
    terminating_felix_client = true;
    contextHandler->terminatingFelixClient = true;
    netio_timer_close(&ctx.evloop, &subscribe_timer);
    netio_timer_close(&ctx.evloop, &user_timer);

    //unsubscribing all elinks
    std::vector<uint64_t> fids_to_unsubscribe = contextHandler->getFidsToUnsubscribe();
    clog_debug("Closing %d subscriptions", fids_to_unsubscribe.size());
    for(auto fid : fids_to_unsubscribe){
        if(evloop_thread_id == std::this_thread::get_id()){
            send_unsubscribe_msg(fid);
        } else {
            unsubscribe(fid);
        }
    }

    uint timeout = 1000;
    uint t_expired = 0;
    uint span = 25U;


    while (!contextHandler->areAllFidsUnsubscribed() && (t_expired <= timeout)){
        std::this_thread::sleep_for(std::chrono::milliseconds(span));
        t_expired += span;

    }
    if (timeout <= t_expired){
        clog_error("Timeout during stopping netio, not all subscriptions have been closed. %d subscriptions remain open.", contextHandler->getFidsToUnsubscribe().size());
    }


    std::vector<netio_send_socket*> send_sockets = contextHandler->getSendSockets();
    total_number_send_sockets.store(send_sockets.size());
    clog_debug("Closing netio %d send sockets.", total_number_send_sockets.load());
    for(auto& socket : send_sockets){
        netio_disconnect(socket);
    }

    t_expired = 0;
    while ((total_number_send_sockets.load(std::memory_order_relaxed) != 0) && (t_expired <= timeout)){
        std::this_thread::sleep_for(std::chrono::milliseconds(span));
        t_expired += span;
    }
    if (timeout <= t_expired){
        clog_error("Timeout during stopping netio, not all send sockets have been disconnected, %d remain open.", total_number_send_sockets.load());
    }
    netio_signal_close(&ctx.evloop, &send_signal);
    netio_signal_close(&ctx.evloop, &sub_signal);
    netio_terminate_signal(&ctx.evloop);
}

int FelixClient::send_evloop(netio_tag_t fid, ToFlxHeader header, const uint8_t* data, size_t size, bool flush, std::promise<uint8_t> prom_send_done){
    if(contextHandler->getSocketState(fid) == DISCONNECTED){
        clog_error("Unbuffered socket is disconnected. Thread: 0x%x FID: 0x%x", std::this_thread::get_id(), fid);
        std::exception_ptr e = std::make_exception_ptr(FelixClientSendConnectionException());
        prom_send_done.set_exception(e);
        return 1;
    }

    int status = NETIO_STATUS_OK;

    if (contextHandler->getType(fid) == BUFFERED) {
        netio_buffered_send_socket* buffered_socket = get_buffered_send_socket(fid);
        if(!contextHandler->exists(static_cast<void*>(buffered_socket))){return NETIO_STATUS_OK;}
        size_t len = sizeof(header) + size;
        uint8_t* buf = (uint8_t*)malloc(len);
        memcpy(buf, &header, sizeof(header));
        memcpy(buf + sizeof(header), data, size);
        clog_debug("Sending (buffered) for 0x%x, i.e. tag %d, socket 0x%p", fid, header.elink, buffered_socket);
        status = netio_buffered_send(buffered_socket, (void*)buf, len);

        clog_debug("Status %d", status);
        free(buf);

        if (flush) {
            netio_buffered_flush(buffered_socket);
        }

    } else {

        netio_send_socket* unbuffered_socket = get_unbuffered_send_socket(fid);
        std::string addr = contextHandler->getAddress(fid);
        clog_debug("Begin sending process. Buffer ptr: 0x%p", unbuf_mem_regions[addr].data);
        memcpy(unbuf_mem_regions[addr].data, &header, sizeof(header));
        memcpy((uint8_t*)unbuf_mem_regions[addr].data + sizeof(header), data, size);
        clog_debug("Memcopy done. Total size: %d (header: %d + size: %d)", sizeof(header) + size, sizeof(header), size);
        if(!contextHandler->exists(static_cast<void*>(unbuffered_socket))){return NETIO_STATUS_OK;}
        status = netio_send(unbuffered_socket, &unbuf_mem_regions[addr], unbuf_mem_regions[addr].data, sizeof(header) + size, (uint64_t)&unbuf_mem_regions[addr]);
    }
    prom_send_done.set_value(status);
    return status;
}







int FelixClient::send_evloop(netio_tag_t fid, const std::vector<const uint8_t*>& msgs, const std::vector<size_t>& sizes, std::promise<uint8_t> prom_send_done){

    if(contextHandler->getSocketState(fid) == DISCONNECTED){
        clog_error("Unbuffered socket is disconnected. Thread: 0x%x FID: 0x%x", std::this_thread::get_id(), fid);
        std::exception_ptr e = std::make_exception_ptr(FelixClientSendConnectionException());
        prom_send_done.set_exception(e);
        return 1;
    }

    int status = NETIO_STATUS_OK;

    if (contextHandler->getType(fid) == BUFFERED) {
        netio_buffered_send_socket* buffered_socket = get_buffered_send_socket(fid);
        clog_debug("Socket = 0x%x with state: %d", buffered_socket, contextHandler->getSocketState(fid));
        if(!contextHandler->exists(static_cast<void*>(buffered_socket))){return NETIO_STATUS_OK;}
        //Adding up messages
        ToFlxHeader header;
        size_t totalSize = accumulate(sizes.begin(), sizes.end(), 0) + sizeof(header) * msgs.size();
        uint8_t* buf = (uint8_t*)malloc(totalSize);
        size_t currentSize = 0;

        for(unsigned i = 0; i < msgs.size(); i++){

            header.length = sizes[i];
            header.reserved = 0;
            header.elink = get_elink(fid);

            memcpy(buf + currentSize, &header, sizeof(header));
            memcpy(buf + currentSize + sizeof(header), msgs[i], sizes[i]);

            currentSize += sizeof(header) + sizes[i];
        }

        clog_debug("Sending (buffered) for 0x%x", fid);
        clog_debug("Sending %d messages with the total size of %d", msgs.size(), totalSize);
        {
            status = netio_buffered_send(buffered_socket, (void*)buf, totalSize);
            clog_debug("Status %d", status);
            free(buf);
            netio_buffered_flush(buffered_socket);
        }
    } else {
        // Unbuffered
        std::string addr = contextHandler->getAddress(fid);
        netio_send_socket* unbuffered_socket = get_unbuffered_send_socket(fid);
        if(!contextHandler->exists(static_cast<void*>(unbuffered_socket))){return NETIO_STATUS_OK;}
        ToFlxHeader header;
        size_t totalSize = accumulate(sizes.begin(), sizes.end(), 0) + sizeof(header) * msgs.size();
        size_t currentSize = 0;

        for(unsigned i = 0; i < msgs.size(); i++){

            header.length = sizes[i];
            header.reserved = 0;
            header.elink = get_elink(fid);

            memcpy((uint8_t*)unbuf_mem_regions[addr].data + currentSize, &header, sizeof(header));
            memcpy((uint8_t*)unbuf_mem_regions[addr].data + currentSize + sizeof(header), msgs[i], sizes[i]);

            currentSize += sizeof(header) + sizes[i];
        }

        clog_debug("Sending (unbuffered) for 0x%x", fid);
        clog_debug("Sending %d messages with the total size of %d", msgs.size(), totalSize);

        {
            status = netio_send(unbuffered_socket, &unbuf_mem_regions[addr], unbuf_mem_regions[addr].data, totalSize, (uint64_t)&unbuf_mem_regions[addr]);
        }

    }
    prom_send_done.set_value(status);
    return status;

}







void FelixClient::establish_send_connection(uint64_t fid, std::promise<uint8_t> prom_sub_done){
    if (contextHandler->getType(fid) == BUFFERED) {
        netio_buffered_send_socket* buffered_socket = get_buffered_send_socket(fid);
        clog_debug("Buffered socket = 0x%p with state: %d", buffered_socket, contextHandler->getSocketState(fid));
        if(contextHandler->getSocketState(fid) == CONNECTING || contextHandler->getSocketState(fid) == CONNECTED){
            prom_sub_done.set_value(0);
            return;
        } else {
            clog_error("Buffered socket is disconnected. Evloop thread: 0x%x FID: 0x%x state is %d", std::this_thread::get_id(), fid, contextHandler->getSocketState(fid));
            std::exception_ptr e = std::make_exception_ptr(FelixClientSendConnectionException());
            prom_sub_done.set_exception(e);
            return;
        }
    } else {
        netio_send_socket* unbuffered_socket = get_unbuffered_send_socket(fid);
        clog_debug("Unbuffered socket = 0x%x with state: %d", unbuffered_socket, contextHandler->getSocketState(fid));
        if(contextHandler->getSocketState(fid) == CONNECTING || contextHandler->getSocketState(fid) == CONNECTED){
            prom_sub_done.set_value(0);
            return;
        } else {
            clog_error("Unbuffered socket is disconnected. Evloop thread: 0x%x FID: 0x%x", std::this_thread::get_id(), fid);
            std::exception_ptr e = std::make_exception_ptr(FelixClientSendConnectionException());
            prom_sub_done.set_exception(e);
            return;
        }
    }

}



void FelixClient::send_data(netio_tag_t fid, const uint8_t* data, size_t size, bool flush) {
    if(terminating_felix_client.load()){
        clog_debug("Already terminating, should not send data");
        return;
    }

    try{
        contextHandler->createOrUpdateInfo(fid, &bus, false);
    } catch (std::exception& e) {
        clog_error("Problem while reading bus: %s", e.what());
        throw FelixClientSendConnectionException();
    }

    clog_debug("Address for FID 0x%x; %s:%d", fid, contextHandler->getIp(fid).c_str(), contextHandler->getPort(fid));

    if (!contextHandler->exists(fid)) {
        clog_error("Invalid Bus entry");
        throw FelixClientSendConnectionException();
    }

    if(evloop_thread_id == std::this_thread::get_id()){
        send_data_intern(fid, data, size, flush);
        return;
    }

    //If there is no connection, we have to establish a new one through the evloop
    if (contextHandler->getSocketState(fid) == DISCONNECTED || contextHandler->getSubscriptionState(fid) == TIMEOUT){ //What about CONNECTING?
        std::promise<uint8_t> prom_sub_done;
        std::future<uint8_t> future_sub_done = prom_sub_done.get_future();
        contextHandler->pushSendTask(SendConnectEvent{fid, std::move(prom_sub_done)});
        netio_signal_fire(&send_signal);
        try{
            std::chrono::milliseconds timeout (5000);
            if (future_sub_done.wait_for(timeout)==std::future_status::timeout){
                clog_error("Timeout to establish send connection expired, stop blocking to prevent deadlock.");
                throw FelixClientResourceNotAvailableException();
            }
            future_sub_done.get();
        } catch(std::exception& e){
            clog_info("Exception in evloop thread: %s thread ID: 0x%x FID: 0x%x", e.what(), std::this_thread::get_id(), fid);
            throw;
        }
    }

    if(contextHandler->getSocketState(fid) == CONNECTING){
        if(!contextHandler->waitConnected(fid, 2000)){
            clog_error("Socket is not connected. Instead: %d on thread: 0x%x FID: 0x%x", contextHandler->getSocketState(fid), std::this_thread::get_id(), fid);
            throw FelixClientSendConnectionException();
        }
    }

    ToFlxHeader header;
    header.length = size;
    header.reserved = 0;
    header.elink = get_elink(fid);
    int status = NETIO_STATUS_OK;

    //send
    std::promise<uint8_t> prom_send_done;
    std::future<uint8_t> future_send_done = prom_send_done.get_future();

    contextHandler->pushSendTask(SendDataEvent{fid, header, data, size, flush, std::move(prom_send_done)});
    netio_signal_fire(&send_signal);

    // wait for promise

    try{
        std::chrono::milliseconds timeout (5000);
        if (future_send_done.wait_for(timeout)==std::future_status::timeout){
            clog_error("Send timeout expired, stop blocking to prevent deadlock."); //No information if send we executed later. Potentially data is corrupted from now on since function will return.
            throw FelixClientException("Send timeout expired");
        }
        status = future_send_done.get();
    } catch(std::exception& e){
        clog_info("Exception in evloop thread: %s thread ID: 0x%x FID: 0x%x", e.what(), std::this_thread::get_id(), fid);
        throw;
    }

    switch(status) {
        case NETIO_STATUS_OK: return;
        case NETIO_STATUS_TOO_BIG: throw FelixClientMessageTooBigException();
        case NETIO_STATUS_AGAIN: throw FelixClientResourceNotAvailableException();
        case NETIO_STATUS_PARTIAL: throw FelixClientPartiallyCompletedException();
        default: throw FelixClientException("Unknown status code: " + status);
    }
}

void FelixClient::send_data(netio_tag_t fid, const std::vector<const uint8_t*>& msgs, const std::vector<size_t>& sizes){
    if(terminating_felix_client.load()){
        clog_debug("Already terminating, should not send data");
        return;
    }

    try{
        contextHandler->createOrUpdateInfo(fid, &bus, false);
    } catch (std::exception& e) {
        clog_error("Problem while reading bus: %s", e.what());
        throw FelixClientSendConnectionException();
    }

    if (!contextHandler->exists(fid)) {
        clog_error("Invalid Bus entry ");
        throw FelixClientSendConnectionException();
    }

    clog_info("Address %s", contextHandler->getAddress(fid).c_str());
    if (msgs.size() != sizes.size()){
        FelixClientException("Error! Number of messages differs from number of sizes");
    }

    if(evloop_thread_id == std::this_thread::get_id()){
        send_data_intern(fid, msgs, sizes);
        return;
    }

    if (contextHandler->getSocketState(fid) == DISCONNECTED || contextHandler->getSubscriptionState(fid) == TIMEOUT){ //What about CONNECTIONG?
        std::promise<uint8_t> prom_sub_done;
        std::future<uint8_t> future_sub_done = prom_sub_done.get_future();
        contextHandler->pushSendTask(SendConnectEvent{fid, std::move(prom_sub_done)});
        netio_signal_fire(&send_signal);
        try{
            std::chrono::milliseconds timeout (5000);
            if (future_sub_done.wait_for(timeout)==std::future_status::timeout){
                clog_error("Timeout to establish send connection expired, stop blocking to prevent deadlock.");
                throw FelixClientResourceNotAvailableException();
            }
            future_sub_done.get();
        } catch(std::exception& e){
            clog_info("Exception in evloop thread: %s thread ID: 0x%x FID: 0x%x", e.what(), std::this_thread::get_id(), fid);
            throw;
        }
    }

    if(contextHandler->getSocketState(fid) == CONNECTING){
        if(!contextHandler->waitConnected(fid, 2000)){
            clog_error("Socket is not connected. Instead: %d on thread: 0x%x FID: 0x%x", contextHandler->getSocketState(fid), std::this_thread::get_id(), fid);
            throw FelixClientSendConnectionException();
        }
    }

    std::promise<uint8_t> prom_send_done;
    std::future<uint8_t> future_send_done = prom_send_done.get_future();
    contextHandler->pushSendTask(SendDataVectorEvent{fid, msgs, sizes, std::move(prom_send_done)});
    netio_signal_fire(&send_signal);

    int status = NETIO_STATUS_OK;
    // wait for promise

    try{
        std::chrono::milliseconds timeout (5000);
        if (future_send_done.wait_for(timeout)==std::future_status::timeout){
            clog_error("Send timeout expired, stop blocking to prevent deadlock."); //No information if send we executed later. Potentially data is corrupted from now on since function will return.
            throw FelixClientException("Send timeout expired");
        }
        status = future_send_done.get();
    } catch(std::exception& e){
        clog_info("Exception in evloop thread: %s thread ID: 0x%x FID: 0x%x", e.what(), std::this_thread::get_id(), fid);
        throw;
    }

    switch(status) {
    case NETIO_STATUS_OK: return;
    case NETIO_STATUS_TOO_BIG: throw FelixClientMessageTooBigException();
    case NETIO_STATUS_AGAIN: throw FelixClientResourceNotAvailableException();
    case NETIO_STATUS_PARTIAL: throw FelixClientPartiallyCompletedException();
    default: throw FelixClientException("Unknown status code: " + status);
    }
}


void FelixClient::send_data_nb(netio_tag_t fid, const uint8_t* data, size_t size, bool flush) {
    if(terminating_felix_client.load()){
        clog_debug("Already terminating, should not send data");
        return;
    }

    if (!contextHandler->exists(fid) || contextHandler->getSocketState(fid) != CONNECTED) {
        clog_error("No corresponding send connection for this FID. Have you tried init_send_data?");
        throw FelixClientSendConnectionException();
    }

    std::vector<uint8_t> data_copy(data, data+size);

    ToFlxHeader header;
    header.length = size;
    header.reserved = 0;
    header.elink = get_elink(fid);

    //send
    std::promise<uint8_t> prom_send_done;

    contextHandler->pushSendTask(SendDataNbEvent{fid, header, std::move(data_copy), size, flush, std::move(prom_send_done)});
    netio_signal_fire(&send_signal);
}

void FelixClient::send_data_nb(netio_tag_t fid, const std::vector<const uint8_t*>& msgs, const std::vector<size_t>& sizes){
    if(terminating_felix_client.load()){
        clog_debug("Already terminating, should not send data");
        return;
    }

    if (!contextHandler->exists(fid) || contextHandler->getSocketState(fid) != CONNECTED) {
        clog_error("No corresponding send connection for this FID. Have you tried init_send_data?");
        throw FelixClientSendConnectionException();
    }

    if (msgs.size() != sizes.size()){
        FelixClientException("Error! Number of messages differs from number of sizes");
    }

    std::vector<std::vector<uint8_t>> data;
    std::vector<const uint8_t*> msgs_copy;
    for (size_t i = 0; i < msgs.size(); i++){
        data.emplace_back(msgs[i], msgs[i]+sizes[i]);
        msgs_copy.push_back(&data[i][0]);
    }

    std::vector<uint64_t> size_copy(sizes);

    std::promise<uint8_t> prom_send_done;
    contextHandler->pushSendTask(SendDataVectorNbEvent{fid, std::move(data), std::move(msgs_copy), std::move(size_copy), std::move(prom_send_done)});
    netio_signal_fire(&send_signal);
}


//We need non-blocking version of send data for tests that only run FelixClient instead of FelixClientThread
void FelixClient::send_data_intern(netio_tag_t fid, const uint8_t* data, size_t size, bool flush) {

    ToFlxHeader header;
    header.length = size;
    header.reserved = 0;
    header.elink = get_elink(fid);
    int status = NETIO_STATUS_OK;
    std::promise<uint8_t> dummy_prom_sub;
    std::promise<uint8_t> dummy_prom_send;

    if((contextHandler->getSocketState(fid) != CONNECTING) || (contextHandler->getSocketState(fid) != CONNECTED)){
        try{
            establish_send_connection(fid, std::move(dummy_prom_sub));
        } catch(std::exception& e){
            clog_info("Exception while establishing connection: %s thread ID: 0x%x FID: 0x%x", e.what(), std::this_thread::get_id(), fid);
            throw;
        }
    }

    if(contextHandler->getSocketState(fid) == CONNECTING){
        contextHandler->pushSendTask(SendDataInternEvent{fid, header, data, size, flush, std::move(dummy_prom_send),1});
        netio_signal_fire(&send_signal);
        return;
    }

    try{
        status = send_evloop(fid, header, data, size, flush, std::move(dummy_prom_send));
    } catch(std::exception& e){
        clog_info("Exception while sending: %s thread ID: 0x%x FID: 0x%x", e.what(), std::this_thread::get_id(), fid);
        throw;
    }

    switch(status) {
        case NETIO_STATUS_OK: return;
        case NETIO_STATUS_TOO_BIG: throw FelixClientMessageTooBigException();
        case NETIO_STATUS_AGAIN: throw FelixClientResourceNotAvailableException();
        case NETIO_STATUS_PARTIAL: throw FelixClientPartiallyCompletedException();
        default: throw FelixClientException("Unknown status code: " + status);
    }
}


//We need non-blocking version of send data for tests that only run FelixClient instead of FelixClientThread
void FelixClient::send_data_intern(netio_tag_t fid, const std::vector<const uint8_t*>& msgs, const std::vector<size_t>& sizes) {

    int status = NETIO_STATUS_OK;
    std::promise<uint8_t> dummy_prom_sub;
    std::promise<uint8_t> dummy_prom_send;

    if((contextHandler->getSocketState(fid) != CONNECTING) || (contextHandler->getSocketState(fid) != CONNECTED)){
        try{
            establish_send_connection(fid, std::move(dummy_prom_sub));
        } catch(std::exception& e){
            clog_info("Exception while establishing connection: %s thread ID: 0x%x FID: 0x%x", e.what(), std::this_thread::get_id(), fid);
            throw;
        }
    }

    if(contextHandler->getSocketState(fid) == CONNECTING){
        contextHandler->pushSendTask(SendDataInternVectorEvent{fid, msgs, sizes, std::move(dummy_prom_send), 1});
        netio_signal_fire(&send_signal);
        return;
    }

    try{
        status = send_evloop(fid, msgs, sizes, std::move(dummy_prom_send));
    } catch(std::exception& e){
        clog_info("Exception while sending: %s thread ID: 0x%x FID: 0x%x", e.what(), std::this_thread::get_id(), fid);
        throw;
    }

    switch(status) {
        case NETIO_STATUS_OK: return;
        case NETIO_STATUS_TOO_BIG: throw FelixClientMessageTooBigException();
        case NETIO_STATUS_AGAIN: throw FelixClientResourceNotAvailableException();
        case NETIO_STATUS_PARTIAL: throw FelixClientPartiallyCompletedException();
        default: throw FelixClientException("Unknown status code: " + status);
    }
}


void FelixClient::exec_send(){
    send_var_t task;
    contextHandler->pullSendTask(task);

    std::visit(overloaded {
        [](std::monostate const&) {
            clog_info("Nothing to send. Send task queue is empty.");
        },
        [this](SendDataEvent& sendData) {
            this->send_evloop(sendData.fid, sendData.header, sendData.data, sendData.size, sendData.flush, std::move(sendData.prom_send_done));
        },
        [this](SendDataNbEvent& sendNbData) {
            this->send_evloop(sendNbData.fid, sendNbData.header, &sendNbData.data[0], sendNbData.size, sendNbData.flush, std::move(sendNbData.prom_send_done));
        },
        [this](SendDataVectorEvent& sendDataVector) {
            this->send_evloop(sendDataVector.fid, sendDataVector.msgs, sendDataVector.sizes, std::move(sendDataVector.prom_send_done));
        },
        [this](SendDataVectorNbEvent& sendDataNbVector) {
            this->send_evloop(sendDataNbVector.fid, sendDataNbVector.msgs, sendDataNbVector.sizes, std::move(sendDataNbVector.prom_send_done));
        },
        [this](SendConnectEvent& sendConnect) {
            this->establish_send_connection(sendConnect.fid, std::move(sendConnect.prom_sub_done));
        },
        [this](SendDataInternEvent& sendData) {
            if(contextHandler->getSocketState(sendData.fid) == CONNECTED){
                this->send_evloop(sendData.fid, sendData.header, sendData.data, sendData.size, sendData.flush, std::move(sendData.prom_send_done));
            }else{
                if(sendData.count< maxSendAttempts){
                    contextHandler->pushSendTask(SendDataInternEvent{sendData.fid, sendData.header, sendData.data, sendData.size, sendData.flush, std::move(sendData.prom_send_done), ++sendData.count});
                }
            }
        },
        [this](SendDataInternVectorEvent& sendDataVector) {
            if(contextHandler->getSocketState(sendDataVector.fid) == CONNECTED){
                this->send_evloop(sendDataVector.fid, sendDataVector.msgs, sendDataVector.sizes, std::move(sendDataVector.prom_send_done));
            }else{
                if(sendDataVector.count < maxSendAttempts){
                    contextHandler->pushSendTask(SendDataInternVectorEvent{sendDataVector.fid, sendDataVector.msgs, sendDataVector.sizes, std::move(sendDataVector.prom_send_done), ++sendDataVector.count});
                }
            }
        },
    }, task);
}


void FelixClient::exec_sub(){
    sub_var_t task;
    contextHandler->pullSubTask(task);

    std::visit(overloaded {
        [](std::monostate const&) {
            clog_info("Nothing to send. Send task queue is empty.");
        },
        [this](UnsubEvent& unsub) {
            this->send_unsubscribe_msg(unsub.fid, std::move(unsub.prom_unsub_done));
        },
        [this](SubscribeEvent const& sub) {
            this->subscribe_evloop(sub.fid);
        },
    }, task);
}


void FelixClient::init_send_data(netio_tag_t fid) {
    clog_info("init_send_data for FID 0x%x", fid);
    if(terminating_felix_client.load()){
        clog_debug("Already terminating, should not send data");
        return;
    }

    try{
        contextHandler->createOrUpdateInfo(fid, &bus, false);
    } catch (std::exception& e) {
        clog_error("Problem while reading bus: %s", e.what());
        throw FelixClientSendConnectionException();
    }

    clog_debug("Address for FID 0x%x; %s:%d", fid, contextHandler->getIp(fid).c_str(), contextHandler->getPort(fid));

    if (!contextHandler->exists(fid)) {
        clog_error("Invalid Bus entry");
        throw FelixClientSendConnectionException();
    }

    //If there is no connection, we have to establish a new one through the evloop
    if (contextHandler->getSocketState(fid) == DISCONNECTED || contextHandler->getSubscriptionState(fid) == TIMEOUT){ //What about CONNECTING?
        std::promise<uint8_t> prom_sub_done;
        contextHandler->pushSendTask(SendConnectEvent{fid, std::move(prom_sub_done)});
        netio_signal_fire(&send_signal);
    }

    return;
}

void FelixClient::init_subscribe(netio_tag_t fid) {
    clog_info("init_subscribe not implemented, subscribe will do the job");
    return;
}


void FelixClient::on_send_signal(void* ptr){
    clog_trace("On send signal");
    static_cast<FelixClient*>(ptr)->exec_send();
}

void FelixClient::on_sub_signal(void* ptr){
    clog_debug("On sub signal");
    static_cast<FelixClient*>(ptr)->exec_sub();
}


void FelixClient::send_data(netio_tag_t fid, struct iovec* iov, unsigned n, bool flush) {
    clog_info("send_msg(netio_tag_t fid, struct iovec* iov, unsigned n): not implemented");
    throw FelixClientSendConnectionException();
}

int FelixClient::subscribe(const std::vector<netio_tag_t>& fids, uint timeoutms, bool for_register) {

    std::vector<netio_tag_t> all_fids;
    clog_debug("Subscribing to %d FIDs", fids.size());
    uint8_t alreadySubscribedCounter = 0;
    for(auto& fid : fids){
        // Already subscribed
        if (!contextHandler->canSubscribe(fid)) {
            clog_info("Already subscribed to 0x%x", fid);
            alreadySubscribedCounter++;
            continue;
        }
        if (fid & 0x8000){
            clog_info("Can't subscribe to toflx fid: 0x%x", fid);
            continue;
        }

        try{
            contextHandler->createOrUpdateInfo(fid, &bus, for_register);
            struct timespec t0;
            clock_gettime(CLOCK_MONOTONIC_RAW, &t0);
            contextHandler->setSubscriptionTimes(fid, "subscribe", t0);
            contextHandler->setSubscriptionState(fid, SUBING);
        } catch(...){
            continue;
        }
        all_fids.push_back(fid);
        contextHandler->addToFidsToResubscribe(fid);
    }
    if (fids.size() == alreadySubscribedCounter){
        return FELIX_CLIENT_STATUS_ALREADY_DONE;
    }

    uint64_t actual_delay = 1000;
    netio_timer_start_ms(&subscribe_timer, actual_delay);
    if(evloop_thread_id == std::this_thread::get_id()){
        for (const auto& fid : all_fids){
            if (!subscribe_evloop(fid)){
                if (timeoutms == 0) {
                    return FELIX_CLIENT_STATUS_OK;
                }
                return FELIX_CLIENT_STATUS_TIMEOUT;
            }
        }
    } else {
        for (auto& fid : all_fids){
            contextHandler->pushSubTask(SubscribeEvent{fid});
            netio_signal_fire(&sub_signal);
        }
    }

    if (timeoutms == 0) {
       return FELIX_CLIENT_STATUS_OK;
    }
    std::vector<netio_tag_t> not_subscribed_fids;

    if(!contextHandler->isSubscribed(all_fids, timeoutms)){
        for (auto& fid : all_fids){
            if(!contextHandler->isSubscribed(fid)){
                not_subscribed_fids.push_back(fid);
                contextHandler->setSubscriptionState(fid, TIMEOUT);
            }
        }
    }

    if (not_subscribed_fids.empty()) {
        if(fids.size() > all_fids.size() + alreadySubscribedCounter){
            return FELIX_CLIENT_STATUS_TIMEOUT;
        }
        return FELIX_CLIENT_STATUS_OK;
    }
    return FELIX_CLIENT_STATUS_TIMEOUT;

}

int FelixClient::subscribe(netio_tag_t fid, uint timeoutms, bool for_register) {
    if(terminating_felix_client.load()){
        clog_debug("Already terminating, should not subscribe anymore");
        return FELIX_CLIENT_STATUS_OK;
    }
    std::vector<uint64_t> fids{fid};
    return subscribe(fids, timeoutms, for_register);
}

bool FelixClient::subscribe_evloop(netio_tag_t fid) {
    try{
        contextHandler->createOrUpdateInfo(fid, &bus);
        struct timespec t1;
        clock_gettime(CLOCK_MONOTONIC_RAW, &t1);
        contextHandler->setSubscriptionTimes(fid, "subscribe_evloop", t1);
    } catch(const std::exception& e){
        return false;
    }
    int ret;
    clog_trace("type (0=buffered, 1=unbuffered) %d", contextHandler->getType(fid));

    if (contextHandler->getType(fid) == BUFFERED) {
        netio_subscribe_socket* buffered_socket = get_buffered_subscribe_socket(fid);
        clog_debug("(Re-)subscribing (buffered) for fid 0x%x and socket 0x%p", fid, buffered_socket);
        ret = netio_subscribe(buffered_socket, fid);
        clog_debug("(Re-)subscribing (buffered) for 0x%x with send socket has been done", fid);

    } else {
        netio_unbuffered_subscribe_socket* unbuffered_socket = get_unbuffered_subscribe_socket(fid);
        clog_debug("(Re-)subscribing (unbuffered) for 0x%x and socket 0x%p", fid, unbuffered_socket);
        clog_debug("Subscribed to unbuffered subscribe socket. Netio_page_size: %d netio pages: %d", unbuffered_socket->recv_socket.attr.buffer_size, unbuffered_socket->recv_socket.attr.num_buffers);
        ret = netio_unbuffered_subscribe(unbuffered_socket, fid);
    }
    if(ret == NETIO_STATUS_AGAIN){
        contextHandler->updateFidsWhenUnsub(fid);
        contextHandler->pushSubTask(SubscribeEvent{fid});
        netio_signal_fire(&sub_signal);
        return false;
    } else if(ret == NETIO_STATUS_ERROR){
        return false;
    }
    if(contextHandler->getSocketState(fid) != CONNECTED && contextHandler->getSocketState(fid) != CONNECTING){
        contextHandler->setSocketState(fid, CONNECTING);
    }
    if(contextHandler->getSocketState(fid) == CONNECTED){
        contextHandler->setSubscriptionState(fid, SUB);
        contextHandler->removeFromFidsToResubscribe(fid);
        struct timespec t2;
        clock_gettime(CLOCK_MONOTONIC_RAW, &t2);
        std::unordered_map<std::string, timespec> subscriptionTimes = contextHandler->getSubscriptionTimes(fid);

        double subscribe_useconds = (t2.tv_sec - subscriptionTimes["subscribe"].tv_sec)*1e6
                                    + (t2.tv_nsec - subscriptionTimes["subscribe"].tv_nsec)*1e-3;
        clog_debug("Beginning of subscribe() until on_connect for fid 0x%x took %fus", fid, subscribe_useconds);

        double subscribe_evloop_useconds = (t2.tv_sec - subscriptionTimes["subscribe_evloop"].tv_sec)*1e6
                                            + (t2.tv_nsec - subscriptionTimes["subscribe_evloop"].tv_nsec)*1e-3;
        clog_debug("Beginning of subscribe_evloop() until on_connect for fid 0x%x took %fus", fid, subscribe_evloop_useconds);
        if (cb_on_connect) {
            (cb_on_connect)(fid);
        }
    }
    return true;
}

void FelixClient::send_unsubscribe_msg(uint64_t fid, std::promise<uint8_t> prom_unsub_done){
    void* socket;
    int ret = 0;
    if (contextHandler->getType(fid) == BUFFERED) {
        struct netio_subscribe_socket* buffered_socket;
        buffered_socket = contextHandler->getSocket<netio_subscribe_socket*>(fid);
        socket = static_cast<void*>(buffered_socket);
        if(!contextHandler->exists(socket) || contextHandler->getSubscriptionState(fid) == UNSUB){prom_unsub_done.set_value(1);return;}
        clog_info("Unsubscribing (buffered) for 0x%x from %s and socket 0x%p", fid, get_address(buffered_socket).c_str(), buffered_socket);
        ret = netio_unsubscribe(buffered_socket, fid);
    } else {
        struct netio_unbuffered_subscribe_socket* unbuffered_socket;
        unbuffered_socket = contextHandler->getSocket<netio_unbuffered_subscribe_socket*>(fid);
        socket = static_cast<void*>(unbuffered_socket);
        if(!contextHandler->exists(socket) || contextHandler->getSubscriptionState(fid) == UNSUB){prom_unsub_done.set_value(1);return;}
        clog_info("Unsubscribing (unbuffered) for 0x%x from %s", fid, get_address(unbuffered_socket).c_str());
        ret = netio_unbuffered_unsubscribe(unbuffered_socket, fid);
    }
    if(ret == NETIO_STATUS_AGAIN){
        contextHandler->pushSubTask(UnsubEvent{fid, std::move(prom_unsub_done)});
        netio_signal_fire(&sub_signal);
        return;
    } else if(ret == NETIO_STATUS_ERROR){
        prom_unsub_done.set_value(2);
    }
    contextHandler->updateFidsWhenUnsub(fid);
    contextHandler->setSubscriptionState(fid, UNSUB);
    if(contextHandler->areAllFidsUnsubscribed(socket)){
        contextHandler->setSocketToAllUnsubscribed(socket);
    }
    prom_unsub_done.set_value(0);
}


int FelixClient::unsubscribe(netio_tag_t fid) {
    clog_info("Unsubscribing from 0x%x", fid);
    if (fid & 0x8000){
        return FELIX_CLIENT_STATUS_OK;
    }

    if(evloop_thread_id == std::this_thread::get_id()){
        send_unsubscribe_msg(fid);
        return FELIX_CLIENT_STATUS_OK;
    }
    if(!contextHandler->exists(fid)){
        return FELIX_CLIENT_STATUS_OK;
    }

    std::promise<uint8_t> prom_unsub_done;
    std::future<uint8_t> future_unsub_done = prom_unsub_done.get_future();
    uint8_t status = FELIX_CLIENT_STATUS_OK;
    if (contextHandler->getSubscriptionState(fid) == SUB || contextHandler->getSubscriptionState(fid) == SUBING){
        contextHandler->setSubscriptionState(fid, UNSUBING);
        contextHandler->pushSubTask(UnsubEvent{fid, std::move(prom_unsub_done)});
        netio_signal_fire(&sub_signal);

        try{
            std::chrono::milliseconds timeout (5000);
            if (future_unsub_done.wait_for(timeout)==std::future_status::timeout){
                clog_error("Unsubscribe timeout expired, stop blocking to prevent deadlock.");
                return FELIX_CLIENT_STATUS_TIMEOUT;
            }
            status = future_unsub_done.get();
        } catch(std::exception& e){
            clog_info("Exception while unsubscribing: %s FID: 0x%x", e.what(), fid);
            throw;
        }
    }
    return status; //0 = OK, 1 = Already done
}


void FelixClient::exec(const UserFunction &user_function ) {
    struct netio_signal* signal = (struct netio_signal*)malloc(sizeof(struct netio_signal)); // c style
    struct SignalData* data = new SignalData(); // C++ style (struct SignalData*)malloc(sizeof(struct SignalData)); Does this need to be cleaned up?
    data->signal = signal;
    data->user_function = user_function;
    data->evloop = &ctx.evloop;

    netio_signal_init(&ctx.evloop, signal);
    signal->data = data;
    signal->cb = on_signal;

    netio_signal_fire(signal);
}


netio_tag_t FelixClient::get_ctrl_fid(netio_tag_t fid) {
    uint8_t sid = 0;
    uint8_t to_flx = 1;
    uint8_t virt = 1;
    netio_tag_t ctrl_fid = get_fid_from_ids(get_did(fid),
                                            get_cid(fid),
                                            COMMAND_REPLY_LINK,
                                            sid,
                                            get_vid(fid),
                                            to_flx,
                                            virt);
    return ctrl_fid;
}

netio_tag_t FelixClient::get_subscribe_fid(netio_tag_t fid) {
    uint8_t sid = 0;
    uint8_t to_flx = 0;
    uint8_t virt = 1;
    netio_tag_t subscribe_fid = get_fid_from_ids(get_did(fid),
                                                 get_cid(fid),
                                                 COMMAND_REPLY_LINK,
                                                 sid,
                                                 get_vid(fid),
                                                 to_flx,
                                                 virt);
    return subscribe_fid;
}


FelixClientThread::Status FelixClient::send_cmd(const std::vector<uint64_t>& fids,
                                                         FelixClientThread::Cmd cmd,
                                                         const std::vector<std::string>& cmd_args,
                                                         std::vector<FelixClientThread::Reply>& replies) {
    clog_debug("send_cmd");
    if(terminating_felix_client.load()){
        clog_debug("Already terminating not sending command");
        return FelixClientThread::Status::OK;
    }

    // No commands to be send, all OK
    if(fids.size() == 0) {
        return FelixClientThread::Status::OK;
    }

    std::string message;
    FelixClientThread::Status status = FelixClientThread::Status::OK;

    // check the command and command arguments
    switch(cmd) {
        case FelixClientThread::NOOP:
            if (cmd_args.size() != 0) {
                message = std::string("Too many arguments for NOOP, needs 0 while %d given.", cmd_args.size());
                status = FelixClientThread::Status::ERROR_INVALID_ARGS;
                break;
            }
            break;
        case FelixClientThread::GET:
            if (cmd_args.size() != 1) {
                message = std::string("Not enough arguments for GET, needs 1 while %d given.", cmd_args.size());
                status = FelixClientThread::Status::ERROR_INVALID_ARGS;
                break;
            }
            break;
        case FelixClientThread::SET:
            if (cmd_args.size() != 2) {
                message = std::string("Not enough arguments for SET, needs 2 while %d given.", cmd_args.size());
                status = FelixClientThread::Status::ERROR_INVALID_ARGS;
                break;
            }
            break;
        case FelixClientThread::ECR_RESET:
            if (cmd_args.size() != 1) {
                message = std::string("Not enough arguments for ECR_RESET, needs 1 while", cmd_args.size());
                status = FelixClientThread::Status::ERROR_INVALID_ARGS;
                break;
            }
            break;
        default:
            message = "Cmd not available: " + FelixClientThread::to_string(cmd);
            status = FelixClientThread::Status::ERROR_INVALID_CMD;
            break;
    }


    // deduce set of ctrl_fids from list of normal fids
    std::unordered_map<netio_tag_t, netio_tag_t> ctrl_by_subscribe_fid;
    std::set<uint64_t> sub_fids_set;
    clog_info("Looking up %d subscribe FIDs", fids.size());
    for(auto const& fid : fids) {
        netio_tag_t ctrl_fid = get_ctrl_fid(fid);
        netio_tag_t sub_fid = get_subscribe_fid(ctrl_fid);
        sub_fids_set.insert(sub_fid);
        std::error_code ec;
        felixbus::FelixBusInfo bus_info = bus.get_info(sub_fid, ec);

        if( bus_info.ip != "" ){
            ctrl_by_subscribe_fid[sub_fid] = ctrl_fid;
        }
        else{
            clog_error("Discarding ctrl fid 0x%x because reply fid 0x%x does not appear in the bus", ctrl_fid, sub_fid);
            FelixClientThread::Reply reply;
            reply.ctrl_fid = ctrl_fid;
            reply.status = FelixClientThread::Status::ERROR_NO_SUBSCRIPTION;
            reply.message = "Invalid fid, not found in bus";
            reply.value = 0;
            replies.push_back(reply);
        }
    }

    std::vector<uint64_t> sub_fids;
    sub_fids.assign(sub_fids_set.begin(), sub_fids_set.end());

    clog_info("Subscribing to %d FIDs", ctrl_by_subscribe_fid.size());
    uint subscribe_timeout = 5000;
    bool for_register = true;
    int ret = subscribe(sub_fids, subscribe_timeout, for_register);
    if(ret == FELIX_CLIENT_STATUS_TIMEOUT){
        auto it = ctrl_by_subscribe_fid.begin();
        while (it != ctrl_by_subscribe_fid.end()){
            if(!contextHandler->isSubscribed(it->first)){
                FelixClientThread::Reply reply;
                reply.ctrl_fid = it->second;
                reply.status = FelixClientThread::Status::ERROR_NO_SUBSCRIPTION;
                reply.message = "Could not subscribe: timeout";
                reply.value = 0;
                replies.push_back(reply);
                it = ctrl_by_subscribe_fid.erase(it);
            } else {
                it++;
            }
        }
    }

    clog_info("Sending command");

    // continue with the connected ctrl_fids and send them the command
    std::unordered_map<std::string, netio_tag_t> ctrl_fid_by_uuid;
    for(auto const& sub_ctrl_fids : ctrl_by_subscribe_fid) {
        uuid_t uuid_dec;
        char uuid[36 + 1];
        uuid_generate(uuid_dec);
        uuid_unparse_upper(uuid_dec, uuid);

        clog_info("Preparing cmd...");
        char json[256];
        struct jWriteControl jwc;
        jwOpen(&jwc, json, 256 - 1, JW_ARRAY, JW_COMPACT);
        jwArr_object(&jwc);
        jwObj_string(&jwc, FelixClientThread::to_string(FelixClientThread::UUID).c_str(), uuid);
        jwObj_string(&jwc, FelixClientThread::to_string(FelixClientThread::CMD).c_str(), FelixClientThread::to_string(cmd).c_str());
        jwObj_array(&jwc, FelixClientThread::to_string(FelixClientThread::CMD_ARGS).c_str());
        for(auto const& cmd_arg : cmd_args) {
            jwArr_string(&jwc, cmd_arg.c_str());
        }
        jwEnd(&jwc);
        jwEnd(&jwc);
        jwClose(&jwc);
        int json_length = strlen(json);
        clog_debug("len=%d; json=%d", json_length, json);

        try {
            clog_info("Sending cmd to fid 0x%x", sub_ctrl_fids.second);
            send_data(sub_ctrl_fids.second, (const uint8_t*)json, json_length, true);
            uuids.emplace(uuid);
            ctrl_fid_by_uuid.emplace(uuid, sub_ctrl_fids.second);
        } catch (FelixClientException& error) {
            clog_info("Could not send cmd for FID 0x%x because: %s", sub_ctrl_fids.second, error.what());
            // sending timed out or other problem
            FelixClientThread::Reply reply;
            reply.ctrl_fid = sub_ctrl_fids.second;
            reply.status = FelixClientThread::Status::ERROR_NO_CONNECTION;
            reply.message = "Could not send cmd";
            reply.value = 0;
            replies.push_back(reply);
        }

    }

    // wait for replies (or timeout)
    clog_info("Waiting for %d replies...", ctrl_fid_by_uuid.size());
    for(auto const &it : ctrl_fid_by_uuid) {
        clog_debug("%s 0x%x", it.first.c_str(), it.second);
    }

    uint timeoutms = 5000;
    uint timeout = 0;
    uint span = std::min(timeoutms, 250U);
    uint recv_replies = 0;
    while ((ctrl_fid_by_uuid.size() != recv_replies) && (timeout <= timeoutms)) {
        recv_replies = 0;
        for (auto& uuid_it : ctrl_fid_by_uuid){
            if (reply_by_uuid.find(uuid_it.first) != reply_by_uuid.end()) {
                recv_replies++;
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(span));
        timeout += span;
    }
    clog_info("Finished waiting");

    for (auto& ctrl_fid_uuid : ctrl_fid_by_uuid){
        // take it out of the list of uuids
        std::string uuid = ctrl_fid_uuid.first;
        uuids.erase(uuid);

        auto reply_it = reply_by_uuid.find(uuid);
        if(reply_it != reply_by_uuid.end()){
            clog_info("uuid: %s", ctrl_fid_uuid.first.c_str());
            FelixClientThread::Reply reply = (*reply_it).second;
            replies.push_back(reply);

            clog_info("Found reply for %s", ctrl_fid_uuid.first.c_str());
            clog_info(" %s", FelixClientThread::to_string(reply).c_str());

            reply_by_uuid.erase(reply_it);
        } else {
            netio_tag_t ctrl_fid = ctrl_fid_uuid.second;
            clog_debug("UUID %s timed out for ctrl_fid 0x%x", uuid.c_str(), ctrl_fid);

            FelixClientThread::Reply reply;
            reply.ctrl_fid = ctrl_fid;
            reply.status = FelixClientThread::Status::ERROR_NO_REPLY;
            reply.message = "Did not receive a reply";
            reply.value = 0;
            replies.push_back(reply);
        } //TODO: What happens to replies that arrive aftert the timeout and staty in the map
        clog_debug("send_cmd done");
    }

    // calculate summary_status, no replies is an ERROR
    FelixClientThread::Status summary_status = replies.size() > 0 ? FelixClientThread::Status::OK : FelixClientThread::Status::ERROR;
    summary_status = std::max(summary_status, status);
    for(auto const& reply : replies) {
        summary_status = std::max(summary_status, reply.status);
    }
    clog_info("Calculated summary status %d:%s from %d replies", summary_status, FelixClientThread::to_string(summary_status).c_str(), replies.size());

    return summary_status;
}

void FelixClient::callback_on_init( OnInitCallback on_init ) {
    clog_info("Registering on_init");
    cb_on_init = on_init;
}

void FelixClient::callback_on_data( OnDataCallback on_data ) {
    clog_info("Registering on_data");
    cb_on_data = on_data;
}

void FelixClient::callback_on_buffer( OnBufferCallback on_buffer ) {
    clog_info("Registering on_buffer");
    cb_on_buffer = on_buffer;
}

void FelixClient::callback_on_connect( OnConnectCallback on_connect ) {
    clog_info("Registering on_connect");
    cb_on_connect = on_connect;
}

void FelixClient::callback_on_disconnect( OnDisconnectCallback on_disconnect ) {
    clog_info("Registering on_disconnect");
    cb_on_disconnect = on_disconnect;
}

void FelixClient::callback_on_user_timer(  OnUserTimerCallback on_user_timer_cb ) {
    clog_info("Registering on_user_timer");
    cb_on_user_timer = on_user_timer_cb;
}

void FelixClient::on_init_eventloop(void* ptr) {
    clog_info("On init");
    FelixClient* client = static_cast<FelixClient*>(ptr);
    client->ready = true;
    std::function<void()> callback = client->cb_on_init;
    if (callback) {
        (callback)();
    }
}

void FelixClient::on_connection_established(void* socket) {
    clog_debug("on_connection_established");
    if (contextHandler->exists(socket)){
        contextHandler->setSocketState(socket, CONNECTED);
        for(auto fid : contextHandler->getFidsBySocket(socket)){
            if (contextHandler->exists(fid)){
                if(contextHandler->getConnectionType(fid) == PUBSUB){
                    if(contextHandler->getSubscriptionState(fid) == TIMEOUT){ //FID has timed out before and we do not want to notify the user nor need this subscription
                        contextHandler->setSubscriptionState(fid, UNSUBING);
                        send_unsubscribe_msg(fid);
                        //contextHandler->removeFromFidsToResubscribe(fid);
                        return;
                    }
                }
                contextHandler->removeFromFidsToResubscribe(fid);
                contextHandler->setSubscriptionState(fid, SUB);
                struct timespec t2;
                clock_gettime(CLOCK_MONOTONIC_RAW, &t2);
                std::unordered_map<std::string, timespec> subscriptionTimes = contextHandler->getSubscriptionTimes(fid);
                double subscribe_useconds = (t2.tv_sec - subscriptionTimes["subscribe"].tv_sec)*1e6
                                        + (t2.tv_nsec - subscriptionTimes["subscribe"].tv_nsec)*1e-3;
                clog_debug("Beginning of subscribe() until on_connect for fid 0x%x took %fus", fid, subscribe_useconds);
                double subscribe_evloop_useconds = (t2.tv_sec - subscriptionTimes["subscribe_evloop"].tv_sec)*1e6
                                                + (t2.tv_nsec - subscriptionTimes["subscribe_evloop"].tv_nsec)*1e-3;
                clog_debug("Beginning of subscribe_evloop() until on_connect for fid 0x%x took %fus", fid, subscribe_evloop_useconds);
                if (cb_on_connect) {
                    (cb_on_connect)(fid);
                }
                clog_debug("Tag: 0x%x out of %d FIDs has status: CONNECTED", fid, contextHandler->getFidsBySocket(socket).size());
            }
        }
    }
}

void FelixClient::on_connection_established(struct netio_send_socket* socket) {
    clog_info("Unbuffered send connection established");
    static_cast<FelixClient*>(socket->usr)->on_connection_established((void*)socket);
}

void FelixClient::on_connection_established(struct netio_buffered_send_socket* socket) {
    clog_info("Buffered send connection established");
    static_cast<FelixClient*>(socket->usr)->on_connection_established((void*)socket);
}

void FelixClient::on_connection_established(struct netio_subscribe_socket* socket) {
    clog_info("Buffered connection established to %s", get_address(socket).c_str());
    static_cast<FelixClient*>(socket->usr)->on_connection_established((void*)socket);
}

void FelixClient::on_connection_established(struct netio_unbuffered_subscribe_socket* socket) {
    clog_info("Unbuffered connection established to %s", get_address(socket).c_str());
    static_cast<FelixClient*>(socket->usr)->on_connection_established((void*)socket);
}

void FelixClient::on_connection_closed(void* socket) {
    clog_debug("FelixClient::on_connection_closed");
    contextHandler->setSocketState(socket, DISCONNECTED);
    auto fids = contextHandler->getFidsBySocket(socket);
    if(contextHandler->getConnectionType(socket) == SENDRECV){
        decrement_num_send_sockets();
    }
    if(!terminating_felix_client){
        contextHandler->removeSocket(socket);
    }

    for(auto fid : fids ){
        if (cb_on_disconnect) {
            (cb_on_disconnect)(fid);
        }
    }

}

void FelixClient::on_connection_closed(struct netio_send_socket* socket) {
    clog_info("Unbuffered send connection closed");
    static_cast<FelixClient*>(socket->usr)->on_connection_closed((void*)socket);
    //static_cast<FelixClient*>(socket->usr)->decrement_num_send_sockets();
}

void FelixClient::on_connection_closed(struct netio_buffered_send_socket* socket) {
    clog_info("Buffered send connection closed");
    static_cast<FelixClient*>(socket->usr)->on_connection_closed((void*)socket);
    //static_cast<FelixClient*>(socket->usr)->decrement_num_send_sockets();
}

void FelixClient::on_connection_closed(struct netio_subscribe_socket* socket) {
    clog_info("Buffered connection closed to %s", get_address(socket).c_str());
    netio_close_socket(&static_cast<FelixClient*>(socket->usr)->ctx.evloop, socket, socket_type::BSUB);
    static_cast<FelixClient*>(socket->usr)->on_connection_closed((void*)socket);
}

void FelixClient::on_connection_closed(struct netio_unbuffered_subscribe_socket* socket) {
    clog_info("Unbuffered connection closed to %s", get_address(socket).c_str());
    netio_close_socket(&static_cast<FelixClient*>(socket->usr)->ctx.evloop, socket, socket_type::USUB);
    static_cast<FelixClient*>(socket->usr)->on_connection_closed((void*)socket);
}

void FelixClient::on_error_connection_refused(void* socket) {
    clog_debug("on_error_connection_refused");
    contextHandler->setSocketState(socket, DISCONNECTED);
    exec([this, socket]{this->contextHandler->removeSocket(socket, true);});
    clog_debug("Socket deleted from socket map.");
}

void FelixClient::on_error_connection_refused(struct netio_send_socket* socket) {
    clog_info("Unbuffered send connection refused");
    static_cast<FelixClient*>(socket->usr)->on_error_connection_refused((void*)socket);
}

void FelixClient::on_error_connection_refused(struct netio_buffered_send_socket* socket) {
    clog_info("Buffered send connection refused");
    static_cast<FelixClient*>(socket->usr)->on_error_connection_refused((void*)socket);
}

void FelixClient::on_error_connection_refused(struct netio_subscribe_socket* socket) {
    clog_info("Buffered connection refused to %s", get_address(socket).c_str());
    static_cast<FelixClient*>(socket->usr)->on_error_connection_refused((void*)socket);

}

void FelixClient::on_error_connection_refused(struct netio_unbuffered_subscribe_socket* socket) {
    clog_info("Unbuffered connection refused to %s", get_address(socket).c_str());
    static_cast<FelixClient*>(socket->usr)->on_error_connection_refused((void*)socket);

}

void FelixClient::on_send_completed(struct netio_send_socket* socket, uint64_t key) {
    clog_debug("Unbuffered send completed for 0x%x", key);
}

void FelixClient::on_register_msg_received(void* socket, netio_tag_t tag, void* data_ptr, size_t size) {
    if (size > 0) {
        uint8_t* data = (uint8_t*)(data_ptr);
        data++;
        size--;

        // handle subscriptions for felix-register
        clog_info("Answer on register command");

        netio_tag_t ctrl_fid = get_ctrl_fid(tag);
        clog_info(" for ctrl_fid 0x%x", ctrl_fid);

        simdjson::dom::element ops = parser.parse(simdjson::padded_string((const char*)data, size));
        // FIXME check that this works for more than one reply
        for (simdjson::dom::object op : ops) {
            FelixClientThread::Reply reply;
            std::string uuid = std::string(op[FelixClientThread::to_string(FelixClientThread::UUID)]);
            clog_info("   uuid %s", uuid.c_str());

            // only add if we are still looking for the uuid (timeout...)
            if (uuids.find(uuid) != uuids.end()) {
                reply.ctrl_fid = ctrl_fid;
                int64_t s = op[FelixClientThread::to_string(FelixClientThread::STATUS)];
                reply.status = static_cast<FelixClientThread::Status>(s);
                reply.message = std::string(op[FelixClientThread::to_string(FelixClientThread::MESSAGE)]);
                reply.value = uint64_t(op[FelixClientThread::to_string(FelixClientThread::VALUE)]);

                reply_by_uuid[uuid] = reply;
            } else {
                clog_info("   uuid not found, answer ignored", uuid.c_str());
            }
        }
    }
}

void FelixClient::on_register_msg_received(struct netio_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size) {
    static_cast<FelixClient*>(socket->usr)->on_register_msg_received((void*)socket, tag, data, size);
}

void FelixClient::on_register_msg_received(struct netio_unbuffered_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size) {
    static_cast<FelixClient*>(socket->usr)->on_register_msg_received((void*)socket, tag, data, size);
}

void FelixClient::on_timer(void* ptr) {

    clog_debug("On timer called. Attempting to resubscribe");
    uint count = 0;
    FelixClient* client = static_cast<FelixClient*>(ptr);
    if(client->terminating_felix_client.load()){
        clog_debug("Already terminating no reason to resubscribe");
        return;
    }
    std::vector<uint64_t> fids_to_resubscribe = client->contextHandler->getFidsToResubscribe();
    clog_debug("Resubscribing for %d", fids_to_resubscribe.size());
    for(const auto& fid : fids_to_resubscribe) {
        if(client->contextHandler->getSocketState(fid) == DISCONNECTED){
            if(fid & 0x8000){
                std::promise<uint8_t> prom_sub;
                std::future<uint8_t> future_sub_done = prom_sub.get_future();
                client->establish_send_connection(fid, std::move(prom_sub));
                if(!future_sub_done.get()) {
                    count++;
                }
            } else {
                if (!client->contextHandler->canSubscribe(fid)) {
                    clog_debug("Already attempting to subscribe to 0x%x", fid);
                    continue;
                }
                if (client->subscribe_evloop(fid)) {
                    count++;
                }
            }
        }
        clog_trace("Socket has state: %d", client->contextHandler->getSocketState(fid));
        clog_trace("FID 0x%x is in state: %d", fid, client->contextHandler->getSubscriptionState(fid));
    }

    if (count > 0) {
        clog_debug("Subscription timer: resubscribing to %d tags", count);
    }
}


void FelixClient::on_signal(void* ptr) {
    clog_debug("On signal");
    struct SignalData* data = (struct SignalData*)ptr;
    UserFunction user_function = data->user_function;
    (user_function)();

    netio_signal_close(data->evloop, data->signal);
    free(data->signal);
    delete data;
}

std::string FelixClient::get_address(netio_subscribe_socket *socket) {
    return std::string(socket->remote_hostname) + std::string(":") + std::to_string(socket->remote_port);
}

std::string FelixClient::get_address(netio_unbuffered_subscribe_socket *socket) {
    return std::string(socket->remote_hostname) + std::string(":") + std::to_string(socket->remote_port);
}


struct netio_subscribe_socket* FelixClient::get_buffered_subscribe_socket(uint64_t fid) {
    bool needInititialization = contextHandler->addOrCreateSocket(fid);
    struct netio_subscribe_socket* socket = contextHandler->getSocket<netio_subscribe_socket*>(fid);
    clog_debug("get_buffered_subscribe_socket for fid 0x%x and socket 0x%p", fid, socket);
    if(needInititialization){
        initialize_buffered_subscribe_socket(socket, fid);
    }
    return socket;
}

void FelixClient::initialize_buffered_subscribe_socket(struct netio_subscribe_socket* socket, uint64_t fid){
    struct netio_buffered_socket_attr attr;
    attr.num_pages = 2 * contextHandler->getPages(fid); //Hard-coded double the number of pages for FLX-2041, todo: make a parameter later
    attr.pagesize = contextHandler->getPagesize(fid);
    attr.watermark = contextHandler->getWatermark(fid);
    if (contextHandler->isTcp(fid)) {
        clog_debug("Setup tcp socket using address %s:%d", contextHandler->getIp(fid).c_str(), contextHandler->getPort(fid));
        netio_subscribe_tcp_socket_init(socket, &ctx, &attr,
            netio_hostname(local_ip_address.c_str()), contextHandler->getIp(fid).c_str(), contextHandler->getPort(fid));
    } else {
        clog_debug("Setup libfabric socket using address %s:%d", contextHandler->getIp(fid).c_str(), contextHandler->getPort(fid));
        netio_subscribe_socket_init(socket, &ctx, &attr,
            netio_hostname(local_ip_address.c_str()), contextHandler->getIp(fid).c_str(), contextHandler->getPort(fid));
    }

    socket->cb_connection_closed = on_connection_closed;
    socket->cb_connection_established = on_connection_established;
    socket->cb_error_connection_refused = on_error_connection_refused;
    if (contextHandler->isForRegister(fid)) {
        socket->cb_msg_received = on_register_msg_received;
    } else {
        socket->cb_msg_received = on_msg_received;
    }

    if(buffer_callback){
        socket->cb_buf_received = on_buf_received;
    }

    clog_debug("Setup socket for %s", get_address(socket).c_str());
    // store object pointer (this) in socket, for use in c-style callbacks
    socket->usr = this;
}

struct netio_unbuffered_subscribe_socket* FelixClient::get_unbuffered_subscribe_socket(uint64_t fid) {
    // Create socket if it does not exist, otherwise lookup
    bool needInititialization = contextHandler->addOrCreateSocket(fid);

    struct netio_unbuffered_subscribe_socket* socket = contextHandler->getSocket<netio_unbuffered_subscribe_socket*>(fid);
    clog_debug("get_unbuffered_subscribe_socket");
    if(needInititialization){
        initialize_unbuffered_subscribe_socket(socket, fid);
    }


    return socket;
}
void FelixClient::initialize_unbuffered_subscribe_socket(netio_unbuffered_subscribe_socket* socket, uint64_t fid){
    clog_debug("Setup socket using address %s:%d", contextHandler->getIp(fid).c_str(), contextHandler->getPort(fid));

    // FLX-1222 use params
    // FLX-1222 allocate somewhere globally, per socket
    //buffer size not specified, taken by the bus
    if (unbuffered_netio_pages  == 0){
        clog_trace("Creating unbuffered subscribe socket. Buffer not specified, using bus parameters. Pages: %d pagesize: %d", contextHandler->getPages(fid), contextHandler->getPagesize(fid));
        netio_unbuffered_subscribe_socket_init(socket, &ctx,
        local_ip_address.c_str(), contextHandler->getIp(fid).c_str(), contextHandler->getPort(fid),
        contextHandler->getPagesize(fid), contextHandler->getPages(fid));
    } else {
        //buffer size passed as parameter
        clog_trace("Creating unbuffered subscribe socket. Buffer parameter specified. Pages: %d pagesize: %d", unbuffered_netio_pages, unbuffered_netio_pagesize);
        netio_unbuffered_subscribe_socket_init(socket, &ctx,
        local_ip_address.c_str(), contextHandler->getIp(fid).c_str(), contextHandler->getPort(fid),
        unbuffered_netio_pagesize, unbuffered_netio_pages); //correct even if unbuffered_netio_pagesize/-pages are needed?
    }

    socket->cb_connection_closed = on_connection_closed;
    socket->cb_connection_established = on_connection_established;
    socket->cb_error_connection_refused = on_error_connection_refused;
    if (contextHandler->isForRegister(fid)) {
        socket->cb_msg_received = on_register_msg_received;
    } else {
        socket->cb_msg_received = on_msg_received;
    }

    clog_debug("Setup socket for %s", get_address(socket).c_str());

    socket->usr = this;
}


struct netio_buffered_send_socket* FelixClient::get_buffered_send_socket(uint64_t fid) {
    // Create socket if it does not exist, otherwise lookup
    bool needInititialization = false;
    if (contextHandler->getSocket<struct netio_buffered_send_socket*>(fid) == NULL){
        needInititialization = contextHandler->addOrCreateSocket(fid);
    }

    struct netio_buffered_send_socket* socket = contextHandler->getSocket<struct netio_buffered_send_socket*>(fid);

    if (needInititialization)
    {
        struct netio_buffered_socket_attr attr;
        attr.num_pages = contextHandler->getPages(fid);
        attr.pagesize = contextHandler->getPagesize(fid);
        attr.watermark = contextHandler->getWatermark(fid);
        attr.timeout_ms = 5;
        if (contextHandler->isTcp(fid)) {
            netio_buffered_send_tcp_socket_init(socket, &ctx, &attr);
        } else {
            netio_buffered_send_socket_init(socket, &ctx, &attr);
        }
        socket->cb_connection_closed = on_connection_closed;
        socket->cb_connection_established = on_connection_established;
        socket->cb_error_connection_refused = on_error_connection_refused;

        // store object pointer (this) in socket, for use in c-style callbacks
        socket->usr = this;
        netio_buffered_connect(socket, contextHandler->getIp(fid).c_str(), contextHandler->getPort(fid));
    }
    return socket;
}


struct netio_send_socket* FelixClient::get_unbuffered_send_socket(uint64_t fid) {
    // Create socket if it does not exist, otherwise lookup
    bool needInititialization = false;
    if (contextHandler->getSocket<struct netio_send_socket*>(fid) == NULL){
        needInititialization = contextHandler->addOrCreateSocket(fid);
    }

    struct netio_send_socket* socket = contextHandler->getSocket<struct netio_send_socket*>(fid);

    if (needInititialization)
    {
        if (contextHandler->isTcp(fid)) {
            // FIXME is this equivalent to the non-tcp below ?
            netio_init_send_tcp_socket(socket, &ctx);
        } else {
            netio_unbuffered_send_socket_init(socket, &ctx);
        }

        struct netio_buffer buf;
        buf.size = UNBUFFERED_MR_SIZE;
        buf.data = malloc(UNBUFFERED_MR_SIZE);
        memset(buf.data, 0, UNBUFFERED_MR_SIZE);
        buf.mr = nullptr;
        unbuf_mem_regions[contextHandler->getAddress(fid)] = buf;

        socket->cb_connection_closed = on_connection_closed;
        socket->cb_connection_established = on_connection_established;
        socket->cb_error_connection_refused = on_error_connection_refused;
        socket->cb_send_completed = on_send_completed;
        // store object pointer (this) in socket, for use in c-style callbacks
        socket->usr = this;
        netio_connect(socket, contextHandler->getIp(fid).c_str(), contextHandler->getPort(fid));
        clog_debug("Connected. Now registering memory region: 0x%p", &unbuf_mem_regions[contextHandler->getAddress(fid)]);
        netio_register_send_buffer(socket, &unbuf_mem_regions[contextHandler->getAddress(fid)], 0);
    }
    return socket;
}



void FelixClient::user_timer_init(){
    netio_timer_init(&ctx.evloop, &user_timer);
    user_timer.cb = on_user_timer;
    user_timer.data = this;
}

void FelixClient::user_timer_start(unsigned long interval){
    netio_timer_start_ms(&user_timer, interval);
}

void FelixClient::user_timer_stop(){
    netio_timer_stop(&user_timer);
}

void FelixClient::on_user_timer(void* ptr) {
    FelixClient* client = static_cast<FelixClient*>(ptr);
    client->cb_on_user_timer();
}


void FelixClient::decrement_num_send_sockets(){
    if(total_number_send_sockets.load() > 0){
        total_number_send_sockets--;
    }
}
