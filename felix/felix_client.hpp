#pragma once

#ifndef NDEBUG
#define NDEBUG //Remove for enabling assert
#endif

#include <functional>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <future>

#include "simdjson.h"

#include "felixbus/felixbus.hpp"

#include "netio/netio.h"
// #include "netio/netio_tcp.h"

#include "felix/felix_client_thread_interface.hpp"
#include "felix/felix_client_thread_extension.hpp"

#include "felix/felix_client_context_handler.hpp"
#include "felix/felix_client_util.hpp"

#define FELIX_CLIENT_STATUS_OK              (0)
#define FELIX_CLIENT_STATUS_ALREADY_DONE    (1)
#define FELIX_CLIENT_STATUS_TIMEOUT         (2)
#define FELIX_CLIENT_STATUS_CONNECTION_BUSY (3)
#define FELIX_CLIENT_STATUS_ERROR           (4)

#define UNBUFFERED_MR_SIZE (128 * 1024) //128kB

class FelixClient {

public:
    enum { LOG_FATAL, LOG_ERROR, LOG_WARN, LOG_INFO, LOG_DEBUG, LOG_TRACE };

    explicit FelixClient(const std::string& local_ip_address_or_interface,
                std::string bus_dir = "./bus",
                std::string bus_group_name = "FELIX",
                unsigned log_level = LOG_ERROR,
                bool verbose_bus = false,
                unsigned netio_pages = 0,
                unsigned netio_pagesize = 0);
    ~FelixClient();

    // delete copy & assignment
    FelixClient(const FelixClient&) = delete;
    FelixClient& operator=(const FelixClient&) = delete;

    // but moving is ok
    FelixClient(const FelixClient&&) { /* ... */ }
    const FelixClient& operator=(const FelixClient&& client) { return client; }

    // returns true just before on_init is called (class is ready to be used)
    bool is_ready() {
        return ready;
    }

    // event loop control
    void run();
    void stop();

    // send messages to FELIX
    void init_send_data(netio_tag_t fid);
    void send_data(netio_tag_t fid, const uint8_t* data, size_t size, bool flush=false);
    void send_data(netio_tag_t fid, struct iovec* iov, unsigned n, bool flush=false);
    void send_data(netio_tag_t fid, const std::vector<const uint8_t*>& msgs, const std::vector<size_t>& sizes);

    void send_data_nb(netio_tag_t fid, const uint8_t* data, size_t size, bool flush=false);
    void send_data_nb(netio_tag_t fid, const std::vector<const uint8_t*>& msgs, const std::vector<size_t>& sizes);

    // subscription control
    void init_subscribe(netio_tag_t fid);
    int subscribe(netio_tag_t fid, uint timeoutms = 0, bool for_register = false);
    int subscribe(const std::vector<netio_tag_t>& fids, uint timeoutms = 0, bool for_register = false);
    int unsubscribe(netio_tag_t fid);

    //create and manage a timer
    void user_timer_init();
    void user_timer_start(unsigned long interval);
    void user_timer_stop();

    // execute on eventloop
    typedef std::function<void ()> UserFunction;
    void exec(const UserFunction &user_function);

    // handle commands
    FelixClientThreadExtension::Status send_cmd(const std::vector<uint64_t>& fids, FelixClientThreadExtension::Cmd cmd, const std::vector<std::string>& cmd_args, std::vector<FelixClientThreadExtension::Reply>& replies);

    //debug
    void enable_buffer_callback(){
        buffer_callback = true;
    }

    // callback registration
    typedef std::function<void ()> OnInitCallback;
    typedef std::function<void(netio_tag_t fid, const uint8_t* data, size_t size, uint8_t status)> OnDataCallback;
    typedef std::function<void(struct netio_buffer* buf, size_t len)> OnBufferCallback;
    typedef std::function<void(netio_tag_t fid)> OnConnectCallback;
    typedef std::function<void(netio_tag_t fid)> OnDisconnectCallback;
    typedef std::function<void ()> OnUserTimerCallback;
    void callback_on_init( OnInitCallback on_init );
    void callback_on_data( OnDataCallback on_data );
    void callback_on_buffer( OnBufferCallback on_buffer );
    void callback_on_connect( OnConnectCallback on_connect );
    void callback_on_disconnect( OnDisconnectCallback on_disconnect );
    void callback_on_user_timer( OnUserTimerCallback on_user_timer_cb );

    void set_thread_affinity(const std::string& affinity);
    void close_sockets();

    // for felix-register
    netio_tag_t get_ctrl_fid(netio_tag_t fid);
    netio_tag_t get_subscribe_fid(netio_tag_t ctrl_fid);

    void decrement_num_send_sockets();

private:
    std::string affinity;
    std::atomic<bool> terminating_felix_client;
    bool ready;
    bool buffer_callback = false;
    std::mutex send_mtx;
    std::mutex sub_mtx;
    int maxSendAttempts = 5;
    uint64_t timer_delay_millis;
    std::vector<netio_tag_t> send_connections;
    std::thread::id evloop_thread_id;
    std::unordered_map<std::string, netio_buffer> unbuf_mem_regions;

    std::unique_ptr<FelixClientContextHandler> contextHandler;

    struct netio_signal send_signal;
    struct netio_signal sub_signal;


    std::vector<int> parseCpuRange(const std::string &range);

    // generic callbacks
    void on_connection_established(void* socket);
    void on_connection_closed(void* socket);
    void on_error_connection_refused(void* socket);
    // inlined for performance
    void on_msg_received(void* socket, netio_tag_t tag, void* data, size_t size) {
        assert(size);
        (cb_on_data)(tag, (uint8_t*)data + 1, size - 1, *(uint8_t*)data);
    }

    void on_buf_received(void* socket, struct netio_buffer* buf, size_t len){
        (cb_on_buffer)(buf, len);
    }
    void on_register_msg_received(void* socket, netio_tag_t tag, void* data_ptr, size_t size);

    // internal c-callbacks
    static void on_init_eventloop(void* ptr);

    static void on_connection_established(struct netio_send_socket* socket);
    static void on_connection_established(struct netio_buffered_send_socket* socket);
    static void on_connection_established(struct netio_subscribe_socket* socket);
    static void on_connection_established(struct netio_unbuffered_subscribe_socket* socket);

    static void on_connection_closed(struct netio_send_socket* socket);
    static void on_connection_closed(struct netio_buffered_send_socket* socket);
    static void on_connection_closed(struct netio_subscribe_socket* socket);
    static void on_connection_closed(struct netio_unbuffered_subscribe_socket* socket);

    static void on_error_connection_refused(struct netio_send_socket* socket);
    static void on_error_connection_refused(struct netio_buffered_send_socket* socket);
    static void on_error_connection_refused(struct netio_subscribe_socket* socket);
    static void on_error_connection_refused(struct netio_unbuffered_subscribe_socket* socket);

    static void on_send_completed(struct netio_send_socket* socket, uint64_t key);

    // inlined for performance
    static void on_msg_received(struct netio_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size) {
        static_cast<FelixClient*>(socket->usr)->on_msg_received((void*)socket, tag, data, size);
    }

    // inlined for performance
    static void on_msg_received(struct netio_unbuffered_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size) {
        static_cast<FelixClient*>(socket->usr)->on_msg_received((void*)socket, tag, data, size);
    }

    static void on_buf_received(struct netio_subscribe_socket* socket, struct netio_buffer* buf, size_t len) {
        static_cast<FelixClient*>(socket->usr)->on_buf_received((void*)socket, buf, len);
    }

    static void on_register_msg_received(struct netio_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size);
    static void on_register_msg_received(struct netio_unbuffered_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size);

    static void on_timer(void* ptr);
    static void on_send_connection_timer(void* ptr);
    static void on_signal(void* ptr);
    static void on_user_timer(void* ptr);
    static void on_send_signal(void* ptr);
    static void on_sub_signal(void* ptr);

    struct SignalData {
        struct netio_signal* signal;
        UserFunction user_function;
        struct netio_eventloop* evloop;
    };

    static std::string get_address(struct netio_subscribe_socket *socket);
    static std::string get_address(struct netio_unbuffered_subscribe_socket *socket);

    bool subscribe_evloop(netio_tag_t fid);
    void send_unsubscribe_msg(uint64_t fid, std::promise<uint8_t> prom_unsub_done = std::promise<uint8_t>());

    struct netio_buffered_send_socket* get_buffered_send_socket(uint64_t fid);
    struct netio_send_socket* get_unbuffered_send_socket(uint64_t fid);

    struct netio_subscribe_socket* get_buffered_subscribe_socket(uint64_t fid);
    void initialize_buffered_subscribe_socket(struct netio_subscribe_socket* socket, uint64_t fid);

    struct netio_unbuffered_subscribe_socket* get_unbuffered_subscribe_socket(uint64_t fid);
    void initialize_unbuffered_subscribe_socket(struct netio_unbuffered_subscribe_socket* socket, uint64_t fid);

    int send_evloop(netio_tag_t fid, ToFlxHeader header, const uint8_t* data, size_t size, bool flush, std::promise<uint8_t> prom_sub_done);
    int send_evloop(netio_tag_t fid, const std::vector<const uint8_t*>& msgs, const std::vector<size_t>& sizes, std::promise<uint8_t> prom_sub_done);
    void establish_send_connection(uint64_t fid, std::promise<uint8_t> prom_sub_done);
    void send_data_intern(netio_tag_t fid, const uint8_t* data, size_t size, bool flush=false);
    void send_data_intern(netio_tag_t fid, const std::vector<const uint8_t*>& msgs, const std::vector<size_t>& sizes);
    void exec_send();
    void exec_sub();

    struct netio_context ctx;
    struct netio_timer subscribe_timer;
    struct netio_timer send_connection_timer;
    struct netio_timer user_timer;

    // unbuffered buffer params
    unsigned unbuffered_netio_pages;
    unsigned unbuffered_netio_pagesize;

    std::string local_ip_address;

    // felixbus
    felixbus::FelixBus bus;

    OnInitCallback cb_on_init;
    // empty data callback in case none is supplied
    OnDataCallback cb_on_data = [](auto&&...) {};
    OnBufferCallback cb_on_buffer  = [](auto&&...) {};
    OnConnectCallback cb_on_connect;
    OnDisconnectCallback cb_on_disconnect;
    OnUserTimerCallback cb_on_user_timer;

    // felix-register
    simdjson::dom::parser parser;
    std::unordered_set<std::string> uuids;
    std::unordered_map<std::string, FelixClientThreadExtension::Reply> reply_by_uuid;

    //counter of send sockets
    std::atomic<int> total_number_send_sockets{0};

};
