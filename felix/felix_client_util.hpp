#pragma once

#include <string>

// utils
std::string get_value_from_getifaddrs(std::string key, bool return_ip);
std::string get_ip_from_interface(const std::string& interface);
std::string get_interface_from_ip(const std::string& ip);

enum netio_socket_connection { PUBSUB, SENDRECV };
// const char *netio_socket_connection_strings[] = { "PUBSUB", "SENDRECV" };

enum netio_socket_state { DISCONNECTED, CONNECTING, CONNECTED };
std::string netio_socket_state_name(enum netio_socket_state e);

enum netio_socket_type { BUFFERED, UNBUFFERED };
// const char *netio_socket_type_strings[] = { "BUFFERED", "UNBUFFERED" };

enum subscribe_state {SUB, UNSUB, SUBING, UNSUBING, SEND, TIMEOUT};
// const char *subscribe_state_strings[] = { "SUB", "UNSUB", "SUBING", "UNSUBING", "SEND", "TIMEOUT" };

unsigned get_log_level(const std::string& log_level);
