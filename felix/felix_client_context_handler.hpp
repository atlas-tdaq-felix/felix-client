#pragma once

#include "felix/felix_client_util.hpp"
#include "felixbus/felixbus.hpp"
#include "netio/netio.h"
#include "felix/felix_client_info.hpp"
#include "felix/felix_toflx.hpp"

#include <iostream>
#include <unordered_map>
#include <memory>
#include <stdexcept>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <variant>
#include <queue>
#include <map>
#include <type_traits>
#include <future>

template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;


struct SendDataEvent {uint64_t fid; ToFlxHeader header; const uint8_t* data; size_t size; bool flush; std::promise<uint8_t> prom_send_done; };
struct SendDataNbEvent {uint64_t fid; ToFlxHeader header; std::vector<uint8_t> data; size_t size; bool flush; std::promise<uint8_t> prom_send_done; };
struct SendDataVectorEvent { uint64_t fid; std::vector<const uint8_t*> msgs; std::vector<size_t> sizes; std::promise<uint8_t> prom_send_done;};
struct SendDataVectorNbEvent { uint64_t fid; std::vector<std::vector<uint8_t>> data; std::vector<const uint8_t*> msgs; std::vector<size_t> sizes; std::promise<uint8_t> prom_send_done;};
struct UnsubEvent {uint64_t fid; std::promise<uint8_t> prom_unsub_done;};
struct SendConnectEvent {uint64_t fid; std::promise<uint8_t> prom_sub_done;};
struct SubscribeEvent {uint64_t fid;};
struct SendDataInternEvent {uint64_t fid; ToFlxHeader header; const uint8_t* data; size_t size; bool flush; std::promise<uint8_t> prom_send_done; uint8_t count;};
struct SendDataInternVectorEvent { uint64_t fid; std::vector<const uint8_t*> msgs; std::vector<size_t> sizes; std::promise<uint8_t> prom_send_done; uint8_t count;};

using send_var_t = std::variant<std::monostate, SendDataEvent, SendDataVectorEvent, SendDataNbEvent, SendDataVectorNbEvent, SendConnectEvent, SendDataInternEvent, SendDataInternVectorEvent>;
using sub_var_t = std::variant<std::monostate, UnsubEvent, SubscribeEvent>;
template<class T>
class TaskQueue {
    std::queue<T> q;
    mutable std::mutex m;

public:
    TaskQueue() {}
    ~TaskQueue() {}

    void pushTask(T event) {
        std::lock_guard<std::mutex> lock(m);
        q.push(std::forward<T>(event));

    }

    void popTask(T& event) {
        std::lock_guard<std::mutex> lock(m);
        if (!q.empty()) {
            event = std::move(q.front());
            q.pop();
        }
    }

    bool checkTask(){
        if(q.size()){
            return true;
        }
        return false;
    }

    uint64_t numTask(){
        return q.size();
    }

};
class FelixClientContextHandler{

    public:
        FelixClientContextHandler():    mSendQueue( std::make_unique<TaskQueue<send_var_t>>()),
                                        mSubQueue( std::make_unique<TaskQueue<sub_var_t>>()){}
        ~FelixClientContextHandler(){}

        std::atomic<bool> terminatingFelixClient;  //necessary?
        struct SocketContext{
            std::atomic<netio_socket_state> state;
            std::vector<uint64_t> subFids;
            std::unique_ptr<SocketWrapper> socket;
            std::string address;
            std::condition_variable mConnectionWait;
            std::mutex mMux;
            bool closingConnection;
        };

        netio_socket_type getType(uint64_t fid);
        netio_socket_type getType(void* socket);
        netio_socket_connection getConnectionType(uint64_t fid);
        netio_socket_connection getConnectionType(void* socket);
        netio_socket_state getSocketState(uint64_t fid);
        subscribe_state getSubscriptionState(uint64_t fid);
        std::unordered_map<std::string, timespec> getSubscriptionTimes(uint64_t fid);
        uint getWatermark(uint64_t fid);
        uint getPagesize(uint64_t fid);
        uint getPages(uint64_t fid);
        uint getPort(uint64_t fid);
        std::string getIp(uint64_t fid);
        std::string getAddress(uint64_t fid);
        bool isTcp(uint64_t fid);
        std::vector<uint64_t> getFidsToResubscribe();
        std::vector<uint64_t> getFidsBySocket(void* socket);
        std::vector<uint64_t> getFidsToUnsubscribe();
        std::vector<netio_send_socket*> getSendSockets();
        std::unordered_map<void*, netio_socket_type> getSubSocketsToDelete();
        template <typename T> T getSocket(uint64_t fid){
            auto* sw = infoByFid[fid]->getSocket();
            if (std::holds_alternative<typename std::remove_pointer<T>::type>(*sw)){
                return static_cast<T>(mGetVoidSocketPointer(*sw));
            }
            return NULL;
        }
        template<typename T> void pushSendTask(T task){
            mSendQueue->pushTask(std::forward<T>(task));
        }
        void pullSendTask(send_var_t& task);
        // bool checkSendTask();
        // uint64_t numSendTask();

        template<typename T> void pushSubTask(T task){
            mSubQueue->pushTask(std::forward<T>(task));
        }
        void pullSubTask(sub_var_t& task);
        // bool checkSubTask();
        // uint64_t numSubTask();

        bool isForRegister(uint64_t fid);
        bool isSubscribed(uint64_t fid);
        bool isSubscribed(std::vector<uint64_t> fids, uint64_t timeout);
        bool areAllFidsUnsubscribed();
        bool areAllFidsUnsubscribed(void* socket);
        bool canSubscribe(uint64_t fid);
        bool exists(uint64_t fid);
        bool exists(void* socket);
        bool waitConnected(uint64_t fid, uint64_t timeoutMs);
        bool addOrCreateSocket(uint64_t fid);

        void setSubscriptionTimes(uint64_t fid, std::string name, timespec ts);
        void setSocketState(uint64_t fid, netio_socket_state s);
        void setSocketState(void* socket, netio_socket_state s);
        void setSubscriptionState(uint64_t fid, subscribe_state s);
        void addToFidsToResubscribe(uint64_t fid);
        void removeFromFidsToResubscribe(uint64_t fid);
        void removeSocket(void* socket, bool connection_refuse = false);
        void updateFidsWhenUnsub(uint64_t fid);
        void createOrUpdateInfo(uint64_t fid, felixbus::FelixBus* bus, bool forRegister = false);
        void setSocketToAllUnsubscribed(void* socket);
        void deleteSubSocket(void* socket);

    private:

        std::unordered_map<uint64_t, std::unique_ptr<FelixClientContext>> infoByFid;
        std::unordered_map<void*, std::unique_ptr<SocketContext>> socketContextMap;
        std::vector<uint64_t> mResubFids;
        std::vector<std::unique_ptr<SocketWrapper>> mSubSocketsToDelete;
        std::mutex send_mtx;
        std::mutex sub_mtx;
        std::mutex info_mtx;
        std::unique_ptr<TaskQueue<send_var_t>> mSendQueue;
        std::unique_ptr<TaskQueue<sub_var_t>> mSubQueue;

        void mCreateInfo(uint64_t fid, felixbus::FelixBus* bus);
        void mUpdateInfo(uint64_t fid, felixbus::FelixBus* bus);
        void mCreateSocket(uint64_t fid);
        bool mAddSocket(uint64_t fid);
        void* mGetVoidSocketPointer(SocketWrapper& wrapper);
};
