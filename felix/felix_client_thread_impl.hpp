#pragma once

#include <functional>
#include <string>

#include "felix/felix_client_thread_interface.hpp"
#include "felix/felix_client_thread_extension.hpp"
#include "felix/felix_client_thread_extension42.hpp"
#include "felix/felix_client_thread_extension421.hpp"
#include "felix/felix_client_thread_extension422.hpp"
#include "felix/felix_client_thread_extension423.hpp"

#include "felix/felix_client.hpp"

class FelixClientThreadImpl : public FelixClientThreadExtension423 {

public:
	explicit FelixClientThreadImpl(Config& config);
        ~FelixClientThreadImpl();

	void send_data(uint64_t fid, const uint8_t* data, size_t size, bool flush) override;
        void send_data(netio_tag_t fid, const std::vector<const uint8_t*>& msgs, const std::vector<size_t>& sizes) override;

        void send_data_nb(uint64_t fid, const uint8_t* data, size_t size, bool flush) override;
        void send_data_nb(netio_tag_t fid, const std::vector<const uint8_t*>& msgs, const std::vector<size_t>& sizes) override;

	void subscribe(uint64_t fid) override;
        void subscribe(const std::vector<uint64_t>& fids) override;
	void unsubscribe(uint64_t fid) override;

	void exec(const UserFunction &user_function) override;

        void init_send_data(uint64_t fid) override;
        void init_subscribe(uint64_t fid) override;

        Status send_cmd(const std::vector<uint64_t>& fids, Cmd cmd, const std::vector<std::string>& cmd_args, std::vector<Reply>& replies) override;

        void user_timer_start(unsigned long interval) override;
        void user_timer_stop() override;
        void callback_on_user_timer(OnUserTimerCallback on_user_timer_cb) override;

private:
    FelixClient* client;
    std::thread thread;

    uint timeoutms;
};
