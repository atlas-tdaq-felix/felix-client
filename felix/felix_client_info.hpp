#pragma once

#include "felix/felix_client_util.hpp"
#include "felixbus/felixbus.hpp"
#include "netio/netio.h"

#include <unordered_map>
#include <memory>
#include <stdexcept>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <variant>
#include <map>
#include <type_traits>



using SocketWrapper = std::variant<std::monostate, netio_unbuffered_subscribe_socket, netio_subscribe_socket, netio_send_socket, netio_buffered_send_socket>;

struct MultiFidsWaitContext{
    std::condition_variable allSubscribed;
    uint64_t expectedSize;
    uint64_t count;
    std::mutex mux;
};

class FelixClientContext {

    public:

        netio_socket_type getType();
        netio_socket_connection getConnectionType();
        subscribe_state getSubscriptionState();
        SocketWrapper* getSocket();
        bool getForRegister();
        uint getWatermark();
        uint getPagesize();
        uint getPages();
        uint getPort();
        std::string getIp();
        bool isTcp();
        std::unordered_map<std::string, timespec> getSubscriptionTimes();

        void setSubscriptionTimes(std::string name, timespec ts);
        void setSocket(SocketWrapper* s);
        void setInfos( std::string &ip, uint port, uint pages, uint pagesize, uint watermark, bool unbuffered, bool pubsub, bool tcp);
        void setSubscriptionState(subscribe_state s);
        void setForRegister(bool r);
        void setWaitContext(MultiFidsWaitContext* ctx);

        void clearWaitContext();
        void removeSocket();

    private:

        uint mPort;
        uint mPages;
        uint mPagesize;
        uint mWatermark;
        bool mForRegister = false;
        netio_socket_type mType;
        netio_socket_connection mConnection;
        bool mTcp;
        std::string mIp;
        std::mutex mSubMutex;

        std::unique_ptr<SocketWrapper> mDummyWrapper =  std::make_unique<SocketWrapper>(SocketWrapper{std::monostate{}});
        SocketWrapper* mSocket = mDummyWrapper.get();

        std::atomic<subscribe_state> mSubscribed = SEND;
        std::condition_variable mSubscribeWait;
        MultiFidsWaitContext* mMultiWaitCtx = NULL;
        std::unordered_map<std::string, timespec> mSubscriptionTimes;

};
