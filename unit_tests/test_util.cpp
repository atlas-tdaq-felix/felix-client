#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstdio>
#include <cstdlib>
#include <chrono>
#include <thread>
#include <string>

#include <catch2/catch_test_macros.hpp>

#include "felix/felix_client_util.hpp"

TEST_CASE( "Test get_interface_from_ip", "[util]" )
{
  REQUIRE( get_interface_from_ip("127.0.0.1") == "libfabric:lo" );
  REQUIRE( get_interface_from_ip("tcp:127.0.0.1") == "tcp:lo" );
  REQUIRE( get_interface_from_ip("libfabric:127.0.0.1") == "libfabric:lo" );
}
