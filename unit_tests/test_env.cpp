#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstdio>
#include <cstdlib>
#include <chrono>
#include <thread>
#include <string>

#include <catch2/catch_test_macros.hpp>

#include "felix/felix_client_properties.h"
#include "felix/felix_client_status.h"
#include "felix/felix_client_thread.hpp"

TEST_CASE( "Test Properties without ENV", "[env]" )
{
  FelixClientThread::Config config;
  config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] = "127.0.0.1";
  config.property[FELIX_CLIENT_BUS_GROUP_NAME] = "TEST_DEFAULT";
  FelixClientThread* client = new FelixClientThread(config);

  REQUIRE( config.property[FELIX_CLIENT_BUS_GROUP_NAME] == "TEST_DEFAULT" );

  delete client;
}

TEST_CASE( "Test Properties with ENV", "[env]" )
{
  setenv("FELIX_CLIENT_bus_group_name", "TEST_ENV", true);

  FelixClientThread::Config config;
  config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] = "127.0.0.1";
  config.property[FELIX_CLIENT_BUS_GROUP_NAME] = "TEST_DEFAULT";

  config.property[FELIX_CLIENT_READ_ENV] = "True";

  FelixClientThread* client = new FelixClientThread(config);

  REQUIRE( config.property[FELIX_CLIENT_READ_ENV] == "True" );
  REQUIRE( config.property[FELIX_CLIENT_BUS_GROUP_NAME] == "TEST_ENV" );

  delete client;
}
