#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstdio>
#include <cstdlib>
#include <chrono>
#include <thread>
#include <string>
#include <unistd.h>

#include <catch2/catch_test_macros.hpp>

#include "felix/felix_client_exception.hpp"
#include "felix/felix_client.hpp"
#include "felix/felix_client_info.hpp"

TEST_CASE( "Test without IP", "[client]" )
{
  REQUIRE_THROWS_AS( new FelixClient(""), FelixClientException);
}

TEST_CASE( "Test Unimplemented Method", "[client]" )
{
  FelixClient client("127.0.0.1");

  REQUIRE_THROWS_AS(client.send_data(0, (struct iovec*)nullptr, 0, true), FelixClientResourceNotAvailableException);
}

TEST_CASE( "Test CTRL and SUBSCRIBE FID", "[client]" )
{
  FelixClient client("127.0.0.1");

  REQUIRE(client.get_ctrl_fid(0x1f086da00037009a) == 0x1f086da812008000);
  REQUIRE(client.get_subscribe_fid(0x1f086da00037009a) == 0x1f086da812000000);
}

TEST_CASE( "Test Double Subscribe FID", "[client]" )
{
  FelixClient client("127.0.0.1");

  REQUIRE(client.subscribe(0x1f086da00037009a) == FELIX_CLIENT_STATUS_OK);
  //REQUIRE(client.subscribe(0x1f086da00037009a) == FELIX_CLIENT_STATUS_ALREADY_DONE); Does not work anymore, since busfile is not detected.
}

FelixClient* global_client;
void on_timer() {
    global_client->user_timer_stop();
    global_client->stop();
    delete(global_client);
    global_client = nullptr;
}

TEST_CASE( "Test User Timer", "[client]" )
{
  global_client = new FelixClient("127.0.0.1");

  global_client->user_timer_init();
  global_client->callback_on_user_timer( on_timer );

  global_client->user_timer_start(1000);

  global_client->run();
}
