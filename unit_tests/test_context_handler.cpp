#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstdio>
#include <cstdlib>
#include <chrono>
#include <thread>
#include <string>
#include <unistd.h>
#include <typeinfo>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <filesystem>

#include <catch2/catch_test_macros.hpp>

#include "felix/felix_client_exception.hpp"
#include "felixbus/felixbus.hpp"
#include "felix/felix_client_context_handler.hpp"

// check how to get id and hostname of current pc -> do we have environmental variables?

void setBusFile(){
    system("rm -r /tmp/bus_context_handler/FELIX_CLIENT/f0/86da/");
    mkdir("/tmp/bus_context_handler/",0777);
    mkdir("/tmp/bus_context_handler/FELIX_CLIENT/",0777);
    mkdir("/tmp/bus_context_handler/FELIX_CLIENT/f0/",0777);
    mkdir("/tmp/bus_context_handler/FELIX_CLIENT/f0/86da/",0777);
    int ret = std::filesystem::exists("/tmp/bus_context_handler/FELIX_CLIENT/f0/86da/");
    if ( ret != 0 ){
        std::cerr << " Error while creating bus directory: " << strerror(errno) << std::endl;
    } else {
        std::cerr << " Bus directory created." << std::endl;
    }
    std::ofstream busfile("/tmp/bus_context_handler/FELIX_CLIENT/f0/86da/dma-0.ndjson");
    busfile << "{\"hfid\":\"0x1f086da00037009a\",\"fid\":2236157748955250842,\"ip\":\"0.0.0.0\",\"port\":53100,\"unbuffered\":false,\"pubsub\":true,\"netio_pages\":256,\"netio_pagesize\":65536,\"host\":\"\",\"pid\":29555,\"user\":\"zp\"}"<<std::endl;
    busfile << "{\"hfid\":\"0x1f086da00037009b\",\"fid\":2236157748955250843,\"ip\":\"0.0.0.0\",\"port\":53100,\"unbuffered\":false,\"pubsub\":false,\"netio_pages\":256,\"netio_pagesize\":65536,\"host\":\"\",\"pid\":29555,\"user\":\"zp\"}"<<std::endl;
    busfile.close();
}

TEST_CASE( "setBusFile", "[context_handler]" )
{
    setBusFile();
}

 TEST_CASE( "Test getType", "[context_handler]" ) //check if getType returns type as specified in busfile (buffered)
{
    felixbus::FelixBus bus;
    bus.set_path("/tmp/bus_context_handler/");
    bus.set_groupname("FELIX_CLIENT");
    bus.set_verbose( "True");

    std::unique_ptr<FelixClientContextHandler> contextHandler = std::make_unique<FelixClientContextHandler>();
    contextHandler->createOrUpdateInfo(0x1f086da00037009a, &bus);
    contextHandler->addOrCreateSocket(0x1f086da00037009a);
    REQUIRE(contextHandler->getType(static_cast<void*>(contextHandler->getSocket<netio_subscribe_socket*>(0x1f086da00037009a))) == BUFFERED);
}

TEST_CASE( "Test getConnectionType", "[context_handler]" ) //check if getConnectionType returns type as specified in busfile (pubsub)
{
    felixbus::FelixBus bus;
    bus.set_path("/tmp/bus_context_handler/");
    bus.set_groupname("FELIX_CLIENT");
    bus.set_verbose( "True");

    std::unique_ptr<FelixClientContextHandler> contextHandler = std::make_unique<FelixClientContextHandler>();
    contextHandler->createOrUpdateInfo(0x1f086da00037009a, &bus);
    contextHandler->addOrCreateSocket(0x1f086da00037009a);
    REQUIRE(contextHandler->getConnectionType(static_cast<void*>(contextHandler->getSocket<netio_subscribe_socket*>(0x1f086da00037009a))) == PUBSUB);
}

TEST_CASE( "Test getSendSocket", "[context_handler]" ) //check if getSendSocket returns entry when socket with fid of pubsub=false has been created as specified in busfile
{
    felixbus::FelixBus bus;
    bus.set_path("/tmp/bus_context_handler/");
    bus.set_groupname("FELIX_CLIENT");
    bus.set_verbose( "True");

    std::unique_ptr<FelixClientContextHandler> contextHandler = std::make_unique<FelixClientContextHandler>();
    contextHandler->createOrUpdateInfo(0x1f086da00037009b, &bus);
    contextHandler->addOrCreateSocket(0x1f086da00037009b);
    REQUIRE(contextHandler->getSendSockets().size() == 1);
}

TEST_CASE( "Test getSocket", "[context_handler]" ) //check if getSocket returns socket of type netio_subscribe_socket, as specified in busfile (buffered, pubsub)
{
    felixbus::FelixBus bus;
    bus.set_path("/tmp/bus_context_handler/");
    bus.set_groupname("FELIX_CLIENT");
    bus.set_verbose( "True");

    std::unique_ptr<FelixClientContextHandler> contextHandler = std::make_unique<FelixClientContextHandler>();
    contextHandler->createOrUpdateInfo(0x1f086da00037009a, &bus);
    contextHandler->addOrCreateSocket(0x1f086da00037009a);
    REQUIRE(strcmp(typeid(*contextHandler->getSocket<netio_subscribe_socket*>(0x1f086da00037009a)).name(), "22netio_subscribe_socket") == 0); //somehow 22 before datatype necessary
}

TEST_CASE( "Test getSocket which does not exist", "[context_handler]" )
{
    felixbus::FelixBus bus;
    bus.set_path("/tmp/bus_context_handler/");
    bus.set_groupname("FELIX_CLIENT");
    bus.set_verbose( "True");

    std::unique_ptr<FelixClientContextHandler> contextHandler = std::make_unique<FelixClientContextHandler>();
    contextHandler->createOrUpdateInfo(0x1f086da00037009a, &bus);

    REQUIRE(contextHandler->getSocket<netio_subscribe_socket*>(0x1f086da00037009a) == NULL);
}

TEST_CASE( "Test ForRegister", "[context_handler]" )
{
    felixbus::FelixBus bus;
    bus.set_path("/tmp/bus_context_handler/");
    bus.set_groupname("FELIX_CLIENT");
    bus.set_verbose( "True");

    std::unique_ptr<FelixClientContextHandler> contextHandler = std::make_unique<FelixClientContextHandler>();
    contextHandler->createOrUpdateInfo(0x1f086da00037009a, &bus, true);
    REQUIRE(contextHandler->isForRegister(0x1f086da00037009a) == true);
}

TEST_CASE( "Test Timeout isSubscribed", "[context_handler]" )
{
    felixbus::FelixBus bus;
    bus.set_path("/tmp/bus_context_handler/");
    bus.set_groupname("FELIX_CLIENT");
    bus.set_verbose( "True");

    std::unique_ptr<FelixClientContextHandler> contextHandler = std::make_unique<FelixClientContextHandler>();
    contextHandler->createOrUpdateInfo(0x1f086da00037009a, &bus);
    std::vector<uint64_t> fids;
    fids.push_back(0x1f086da00037009a);
    REQUIRE(contextHandler->isSubscribed(fids, 100) == false);
}

TEST_CASE( "Test addOrCreateSocket", "[context_handler]" )
{
    felixbus::FelixBus bus;
    bus.set_path("/tmp/bus_context_handler/");
    bus.set_groupname("FELIX_CLIENT");
    bus.set_verbose( "True");

    std::unique_ptr<FelixClientContextHandler> contextHandler = std::make_unique<FelixClientContextHandler>();
    contextHandler->createOrUpdateInfo(0x1f086da00037009a, &bus);
    REQUIRE(contextHandler->addOrCreateSocket(0x1f086da00037009a) == true);
}

TEST_CASE( "Test areAllFidsUnsubscribed", "[context_handler]" )
{
    std::unique_ptr<FelixClientContextHandler> contextHandler = std::make_unique<FelixClientContextHandler>();
    REQUIRE(contextHandler->areAllFidsUnsubscribed() == true);
}

TEST_CASE( "Test areAllFidsUnsubscribed is not true", "[context_handler]" )
{
    felixbus::FelixBus bus;
    bus.set_path("/tmp/bus_context_handler/");
    bus.set_groupname("FELIX_CLIENT");
    bus.set_verbose( "True");

    std::unique_ptr<FelixClientContextHandler> contextHandler = std::make_unique<FelixClientContextHandler>();
    contextHandler->createOrUpdateInfo(0x1f086da00037009a, &bus);
    contextHandler->addOrCreateSocket(0x1f086da00037009a);
    REQUIRE(contextHandler->areAllFidsUnsubscribed() == false);
}

TEST_CASE( "Test canSubscribe", "[context_handler]" )
{
    std::unique_ptr<FelixClientContextHandler> contextHandler = std::make_unique<FelixClientContextHandler>();
    REQUIRE(contextHandler->canSubscribe(0x1f086da00037009a) == true);
}

TEST_CASE( "Test exists not", "[context_handler]" )
{
    std::unique_ptr<FelixClientContextHandler> contextHandler = std::make_unique<FelixClientContextHandler>();
    REQUIRE(contextHandler->exists(0x1f086da00037009a) == false);
}

TEST_CASE( "Test exists", "[context_handler]" )
{
    felixbus::FelixBus bus;
    bus.set_path("/tmp/bus_context_handler/");
    bus.set_groupname("FELIX_CLIENT");
    bus.set_verbose( "True");

    std::unique_ptr<FelixClientContextHandler> contextHandler = std::make_unique<FelixClientContextHandler>();
    contextHandler->createOrUpdateInfo(0x1f086da00037009a, &bus);
    REQUIRE(contextHandler->exists(0x1f086da00037009a) ==  true);
}

TEST_CASE( "Test Timeout waitConnected", "[context_handler]" )
{
    felixbus::FelixBus bus;
    bus.set_path("/tmp/bus_context_handler/");
    bus.set_groupname("FELIX_CLIENT");
    bus.set_verbose( "True");

    std::unique_ptr<FelixClientContextHandler> contextHandler = std::make_unique<FelixClientContextHandler>();
    contextHandler->createOrUpdateInfo(0x1f086da00037009a, &bus);
    REQUIRE(contextHandler->waitConnected(0x1f086da00037009a,100) == false);
}

TEST_CASE( "Test SocketState", "[context_handler]" )
{
    felixbus::FelixBus bus;
    bus.set_path("/tmp/bus_context_handler/");
    bus.set_groupname("FELIX_CLIENT");
    bus.set_verbose( "True");

    std::unique_ptr<FelixClientContextHandler> contextHandler = std::make_unique<FelixClientContextHandler>();
    contextHandler->createOrUpdateInfo(0x1f086da00037009a, &bus);
    contextHandler->addOrCreateSocket(0x1f086da00037009a);
    contextHandler->setSocketState(0x1f086da00037009a, CONNECTING);
    REQUIRE(contextHandler->getSocketState(0x1f086da00037009a) == CONNECTING);
}

TEST_CASE( "Test SubscriptionState", "[context_handler]" )
{
    felixbus::FelixBus bus;
    bus.set_path("/tmp/bus_context_handler/");
    bus.set_groupname("FELIX_CLIENT");
    bus.set_verbose( "True");

    std::unique_ptr<FelixClientContextHandler> contextHandler = std::make_unique<FelixClientContextHandler>();
    contextHandler->createOrUpdateInfo(0x1f086da00037009a, &bus);
    contextHandler->addOrCreateSocket(0x1f086da00037009a);
    contextHandler->setSubscriptionState(0x1f086da00037009a, SUB);
    REQUIRE(contextHandler->isSubscribed(0x1f086da00037009a) == true);
    REQUIRE(contextHandler->canSubscribe(0x1f086da00037009a) == false);
}

TEST_CASE( "Test FidsToResubscribe", "[context_handler]" )
{

    std::unique_ptr<FelixClientContextHandler> contextHandler = std::make_unique<FelixClientContextHandler>();
    contextHandler->addToFidsToResubscribe(0x1f086da00037009a);
    REQUIRE(contextHandler->getFidsToResubscribe().size() == 1);
    contextHandler->removeFromFidsToResubscribe(0x1f086da00037009a);
    REQUIRE(contextHandler->getFidsToResubscribe().size() == 0);
}

TEST_CASE( "Test removeSocket", "[context_handler]" )
{
    felixbus::FelixBus bus;
    bus.set_path("/tmp/bus_context_handler/");
    bus.set_groupname("FELIX_CLIENT");
    bus.set_verbose( "True");

    std::unique_ptr<FelixClientContextHandler> contextHandler = std::make_unique<FelixClientContextHandler>();
    contextHandler->createOrUpdateInfo(0x1f086da00037009a, &bus);
    contextHandler->addOrCreateSocket(0x1f086da00037009a);
    contextHandler->terminatingFelixClient = false;
    contextHandler->removeSocket(static_cast<void*>(contextHandler->getSocket<netio_subscribe_socket*>(0x1f086da00037009a)),false);

    REQUIRE(contextHandler->getFidsToResubscribe().size() == 1);
    // not best way to check but problem socketContextMap and infoByFid are private -> return size of socketContextMap in removeSocket or implement seperate getSize function
}
TEST_CASE( "Test getSubSocketsToDelete", "[context_handler]" )
{
    felixbus::FelixBus bus;
    bus.set_path("/tmp/bus_context_handler/");
    bus.set_groupname("FELIX_CLIENT");
    bus.set_verbose( "True");

    std::unique_ptr<FelixClientContextHandler> contextHandler = std::make_unique<FelixClientContextHandler>();
    contextHandler->createOrUpdateInfo(0x1f086da00037009a, &bus);
    contextHandler->addOrCreateSocket(0x1f086da00037009a);
    contextHandler->terminatingFelixClient = false;
    contextHandler->removeSocket(static_cast<void*>(contextHandler->getSocket<netio_subscribe_socket*>(0x1f086da00037009a)),false);

    REQUIRE(contextHandler->getSubSocketsToDelete().size() == 1);
  }

TEST_CASE( "Test updateFidsWhenUnsub", "[context_handler]" )
{
    felixbus::FelixBus bus;
    bus.set_path("/tmp/bus_context_handler/");
    bus.set_groupname("FELIX_CLIENT");
    bus.set_verbose( "True");

    std::unique_ptr<FelixClientContextHandler> contextHandler = std::make_unique<FelixClientContextHandler>();
    contextHandler->createOrUpdateInfo(0x1f086da00037009a, &bus);
    contextHandler->addOrCreateSocket(0x1f086da00037009a);
    contextHandler->updateFidsWhenUnsub(0x1f086da00037009a);
    REQUIRE(contextHandler->getFidsToUnsubscribe().size() == 0);
}
