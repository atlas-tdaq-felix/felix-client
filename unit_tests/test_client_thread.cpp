#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstdio>
#include <cstdlib>
#include <chrono>
#include <thread>
#include <string>

#include <catch2/catch_test_macros.hpp>

#include "felix/felix_client_properties.h"
#include "felix/felix_client_status.h"
#include "felix/felix_client_thread.hpp"

TEST_CASE( "Test Affinity 1", "[client_thread]" )
{
  FelixClientThread::Config config;
  config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] = "127.0.0.1";
  config.property[FELIX_CLIENT_THREAD_AFFINITY] = "1";
  FelixClientThread* client = new FelixClientThread(config);

  delete client;
}

TEST_CASE( "Test Affinity 1-1", "[client_thread]" )
{
  FelixClientThread::Config config;
  config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] = "127.0.0.1";
  config.property[FELIX_CLIENT_THREAD_AFFINITY] = "1-1";
  FelixClientThread* client = new FelixClientThread(config);

  delete client;
}

TEST_CASE( "Test Wrong Timeout", "[client_thread]" )
{
  FelixClientThread::Config config;
  config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] = "127.0.0.1";
  config.property[FELIX_CLIENT_THREAD_AFFINITY] = "1-1";
  config.property[FELIX_CLIENT_TIMEOUT] = "10hours";
  FelixClientThread* client = new FelixClientThread(config);

  delete client;
}
