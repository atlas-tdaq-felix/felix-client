#include <cstdio>
#include <cmath>
#include <iostream>
#include <string>
#include <unistd.h>
#include <map>
#include <mutex>

#include "docopt/docopt.h"

#include "felix/felix_client.hpp"
#include "felix/felix_client_properties.h"
#include "felix/felix_client_status.h"
#include "felix/felix_client_util.hpp"
#include "felix/felix_client_thread.hpp"
#include "felix/felix_client_exception.hpp"

#include "hdlc_coder/hdlc.hpp"

#include "felixtag.h"
#include "felix/felix_fid.h"

/* The following constant relates to Henk B. method of achieving (relatively small - up to say 50 us) delays on the HDLC-encoded elink.
 * A good reading on Henk's method is in: https://its.cern.ch/jira/browse/FLX-581
 * The constant below defines how many zeros to put per microsecond of delay.
 * HDLC Elink throughput is 80Mbps, so 80 bits goes through each 1us.  80bits is 10 bytes that's why we insert 10 bytes to delay by 1 us.
 *
 * see also: https://gitlab.cern.ch/atlas-dcs-common-software/ScaSoftware/-/blob/master/NetioNextBackend/HdlcBackend.cpp#L109
 */
const unsigned int FLX_ZEROS_PER_MICROSECOND = 10;

//Constants from ScaSoftware HldcBackendCommon.h
const uint8_t NETIO_FRAME_HDLC_START_OFFSET = 8;  // HDLC frame shall be placed starting from this index
const uint8_t HDLC_FRAME_PAYLOAD_OFFSET = 2;      // HDLC payload offset in HDLC frame
const uint8_t HDLC_FRAME_TRAILER_SIZE = 2;        // HDLC trailer size in HDLC frame

const uint8_t NULL_REQ[] = {
    0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
    0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
    0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
    0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
    0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
    0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
    }; // Padding
const uint8_t NULL_REQ_SIZE = 6*8;  // 48 us delay

const uint8_t CHIP_ID_REQ[] = {0x01, 0x14, 0x04, 0xD1, 0x0, 0x0, 0x1, 0x0}; //Message to request the chip Id
const uint8_t CHIP_ID_REQ_SIZE = 8;

static const char USAGE[] =
R"(felix-client-thread-sca-commands - Exchanges messages with a GBT-SCA for several commands

    Usage:
      felix-client-thread-sca-commands [options] <local_ip_or_interface> <cid> <did> <elinks>...

    Options:
      -h --help                         Show this screen.
      --version                         Show version.
      --log-level=<loglevel>            Specify level of logging (trace, debug, info, warning, error, fatal) [default: info]
      --verbose-bus                     Show bus information
      --bus-dir=<directory>             Set directory for bus to use [default: ./bus]
      --bus-group-name=<group-name>     Set group-name for bus to use [default: FELIX]
      --timeout=<timeoutms>             Set timeout to subscribe (0 is no timeout) [default: 0]
      --netio-pages=<pages>             Number of pages for buffer [default: 256]
      --netio-pagesize=<pagesize>       Pagesize for buffer in bytes [default: 65536]
      --scas=<number>                   Send commands to number of scas starting from first elink [default: 1]
      --non-blocking                    Use non-blocking calls for send_data
)";

class Commands {

public:
    explicit Commands(std::map<std::string, docopt::value> &args) {
        try {
            FelixClientThread::Config config;
            unsigned log_level = get_log_level(args["--log-level"].asString());
            config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] = args["<local_ip_or_interface>"].asString();
            config.property[FELIX_CLIENT_LOG_LEVEL] = args["--log-level"].asString();
            config.property[FELIX_CLIENT_BUS_DIR] = args["--bus-dir"].asString();
            config.property[FELIX_CLIENT_BUS_GROUP_NAME] = args["--bus-group-name"].asString();
            config.property[FELIX_CLIENT_VERBOSE_BUS] = args["--verbose-bus"].asBool() ? "True" : "False";
            config.property[FELIX_CLIENT_TIMEOUT] = args["--timeout"].asString();
            config.property[FELIX_CLIENT_NETIO_PAGES] = args["--netio-pages"].asString();
            config.property[FELIX_CLIENT_NETIO_PAGESIZE] = args["--netio-pagesize"].asString();

            cid = args["<cid>"].asLong();
            did = args["<did>"].asLong();

            verbosity = log_level;

            non_blocking = args["--non-blocking"].asBool();

            // Fill elink list
            std::vector<std::string> elink_list = args["<elinks>"].asStringList();
            std::vector<std::uint32_t> elinks;
            uint32_t scas = args["--scas"].asLong();
            if (scas == 1) {
                std::transform(std::begin(elink_list),
                std::end(elink_list),
                std::back_inserter(elinks),
                [](std::string s) { return static_cast<uint32_t>(std::stoul(s, nullptr, 0)); }
                );
            } else {
                uint32_t elink_offset = std::stoul(elink_list[0], nullptr, 0);
                for (size_t i = 0; i<scas; i++) {
                    elinks.push_back(elink_offset + i);
                }
            }
            num_elinks = elinks.size();
            printf("Number of SCAs: %d\n", num_elinks);

            config.on_init_callback = std::bind(&Commands::on_init, this);
            config.on_data_callback = std::bind(&Commands::on_data, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
            config.on_connect_callback = std::bind(&Commands::on_connect, this, std::placeholders::_1);
            config.on_disconnect_callback = std::bind(&Commands::on_disconnect, this, std::placeholders::_1);

            client = std::make_unique<FelixClientThread>(config);
            std::vector<uint64_t> send_tags;

            reset_msg = (uint8_t*)calloc( reset_msg_size, sizeof(uint8_t));
            null_msg = (uint8_t*)calloc( null_msg_size, sizeof(uint8_t));
            scaid_msg = (uint8_t*)calloc( scaid_msg_size, sizeof(uint8_t));

            for (auto &elink : elinks){
                uint64_t subscribe_tag;
                uint64_t send_tag;
                uint8_t stream_id = 0;
                uint8_t vid = 1;

                printf("Creating fid with did 0x%x, cid 0x%x, elink 0x%x, streamId 0x%x, vid 0x%x \n", did, cid, elink, stream_id, vid);
                subscribe_tag = get_fid_from_ids(did, cid, elink, stream_id, vid, 0, 0);
                send_tag = get_fid_from_ids(did, cid, elink, stream_id, vid, 1, 0);
                send_tags.push_back(send_tag);

                printf("Subscribe tag 0x%lx\n", subscribe_tag);
                printf("send tag 0x%lx\n", send_tag);

                counters perf;
                map_mutex.lock();
                subscribe_vec.push_back(subscribe_tag);
                subscription_map[subscribe_tag] = send_tag;
                performance_map[send_tag] = perf;
                connected_map[send_tag] = false;
                connected_map[subscribe_tag] = false;
                reply_map[send_tag] = std::vector<std::vector<uint8_t>>(0);
                map_mutex.unlock();


                client->subscribe(subscribe_tag);
            }

            // check if all subscriptions are connected
            printf("Waiting for subscriptions...\n");
            bool all_connected = false;
            while (!all_connected) {
                all_connected = true;
                map_mutex.lock();
                for(auto& subscription : subscription_map) {
                    all_connected &= connected_map[subscription.first];
                }
                map_mutex.unlock();
                sleep(1);
            }

            if (non_blocking) {
                printf("Initialize Send Connections...\n");
                for(auto& subscription : subscription_map) {
                    client->init_send_data(subscription.second);
                }

                printf("Waiting for send connections...\n");
                bool all_send_connected = false;
                while (!all_send_connected) {
                    all_send_connected = true;
                    map_mutex.lock();
                    for(auto& subscription : subscription_map) {
                        all_send_connected &= connected_map[subscription.second];
                    }
                    map_mutex.unlock();
                    sleep(1);
                }
            }

            for(auto& subscription : subscription_map) {
                printf("Sending commands...\n");
                try {
                    send_cmd(subscription.second);
                    printf("Sending done to %lx\n", subscription.second);
                } catch(std::exception& e) {
                    std::cout << "\x1b[31m Unable to send, because: " << e.what() << " \x1b[0m" << std::endl;
                }
            }

            printf("Waiting for the following replies...\n");
            for(auto& subscription : subscription_map) {
                auto send_tag = subscription.second;
                printf(" for tag 0x%lx:\n", send_tag);
                for (auto& reply : reply_map[send_tag]) {
                    printf("    Expected Reply: ");
                    for (auto& byte : reply) {
                        printf("\x1b[32m 0x%02X ", byte);
                    }
                    printf("\x1b[0m \n");
                }
            }

            printf("Checking for replies...\n");
            bool all_replied = false;
            while (!all_replied) {
                printf("in all replied loop\n");
                size_t outstanding = 0;
                all_replied = true;
                map_mutex.lock();
                for(auto& subscription : subscription_map) {
                    outstanding += reply_map[subscription.second].size();
                    all_replied &= (reply_map[subscription.second].size() == 0);
                }
                map_mutex.unlock();
                printf("outstanding %zu\n", outstanding);
                sleep(1);
            }

            map_mutex.lock();
            for(auto& subscription : subscription_map){
                results(&performance_map[subscription.second], subscription.first);
            }
            subscription_map.clear();
            map_mutex.unlock();

            std::cout << "Exiting now " << std::endl;
            //exit(0);

        } catch (std::invalid_argument const& error) {
            std::cerr << "Argument or option of wrong type" << std::endl;
            std::cout << std::endl;
            std::cout << USAGE << std::endl;
            return_code  = -1;
        } catch (FelixClientResourceNotAvailableException& error) {
            printf("Failed to send\n");
           return_code  = 1;
        }
    }

    ~Commands() {
    }
    int return_code = 0;

private:
    unsigned verbosity;
    bool non_blocking;
    std::unique_ptr<FelixClientThread> client;

    //messages
    uint8_t* reset_msg;
    size_t reset_msg_size = (HDLC_FRAME_PAYLOAD_OFFSET + HDLC_FRAME_TRAILER_SIZE);

    uint8_t* null_msg;
    size_t null_msg_size = NULL_REQ_SIZE;

    uint8_t* scaid_msg;
    size_t scaid_msg_size = (HDLC_FRAME_PAYLOAD_OFFSET + CHIP_ID_REQ_SIZE + HDLC_FRAME_TRAILER_SIZE);

    //counters
    unsigned long timeout_ms = 1000;

    struct counters {
        long send_counter = 0;
        long recv_counter = 0;
        struct timespec t0 = { 0, 0 };
        //struct timespec tt = { 0, 0 };
        std::vector<double> times;
    };

    //links
    uint8_t seqnr = 1;
    uint32_t cid;
    uint32_t did;

    std::mutex map_mutex;
    std::map<uint64_t, uint64_t> subscription_map;                      // fid -> send_tag
    std::map<uint64_t, struct counters> performance_map;
    std::map<uint64_t, bool> connected_map;                             // fid/send_tag -> connected
    std::map<uint64_t, std::vector<std::vector<uint8_t>>> reply_map;    // send_tag -> list of uint8_t sequences
    std::vector<uint64_t> subscribe_vec;
    uint8_t num_elinks = 0;

    //methods
    void on_init() {
        printf("Initialising connections...\n");
    }

    void on_connect(uint64_t fid) {
        printf("\x1b[32m Connection up for fid 0x%lx \x1b[0m\n", fid);
        map_mutex.lock();
        connected_map[fid] = true;
        map_mutex.unlock();
    }

    void on_disconnect(uint64_t fid) {
        printf("\x1b[31m Disconnection for fid 0x%lx \x1b[0m \n", fid);
        map_mutex.lock();
        // crashes as its called from the exit handler, no more this pointer
        // connected_map[fid] = false;
        map_mutex.unlock();
    }

    void send_cmd(uint64_t send_tag) {
        std::vector<const uint8_t*> msgs;
        std::vector<size_t> sizes;

        // fill
        felix::hdlc::encode_hdlc_ctrl_header(reset_msg, 0, 143); // 0x8f
        felix::hdlc::encode_hdlc_trailer(
            reset_msg + HDLC_FRAME_PAYLOAD_OFFSET, // where to put the trailer
            reset_msg, // pointer to the beginning of HDLC frame
            HDLC_FRAME_PAYLOAD_OFFSET); // len for crc

        // reset
        msgs.emplace_back((const uint8_t*)reset_msg);
        sizes.emplace_back(reset_msg_size);
        std::vector<uint8_t> reset_reply{ 0xFF, 0x63 };
        reply_map[send_tag].push_back(reset_reply);

        // chip ID
        felix::hdlc::encode_hdlc_msg_header(scaid_msg, 0, seqnr); //HDLC address, always 0 for SCA
        memcpy((void*)(scaid_msg + HDLC_FRAME_PAYLOAD_OFFSET), CHIP_ID_REQ, CHIP_ID_REQ_SIZE);
        felix::hdlc::encode_hdlc_trailer(
            scaid_msg + HDLC_FRAME_PAYLOAD_OFFSET + CHIP_ID_REQ_SIZE, //where to put the trailer
            scaid_msg, // pointer to the beginning of HDLC frame
            HDLC_FRAME_PAYLOAD_OFFSET + CHIP_ID_REQ_SIZE); // len for crc
        msgs.emplace_back((const uint8_t*)scaid_msg);
        sizes.emplace_back(scaid_msg_size);
        std::vector<uint8_t> scaid_reply{ 0x00, (uint8_t)(0x22 * (seqnr &= 0x7)), 0x01, 0x14, 0x00, 0x04, 0x0B, 0x00, 0x00, 0x07 };
        reply_map[send_tag].push_back(scaid_reply);
        ++seqnr;

        printf("\x1b[33m Sending reset command for 0x%lx: ", send_tag);
        for(unsigned int b=0; b<reset_msg_size; ++b){printf("%02X ", reset_msg[b]);}
        printf("\x1b[0m \n");

        printf("\x1b[33m Sending id command for 0x%lx: ", send_tag);
        for(unsigned int b=0; b<scaid_msg_size; ++b){printf("%02X ", scaid_msg[b]);}
        printf("\x1b[0m \n");

        clock_gettime(CLOCK_MONOTONIC_RAW, &(performance_map[send_tag].t0));
        try{
            if (non_blocking) {
                client->send_data_nb(send_tag, msgs, sizes);
            } else {
                client->send_data(send_tag, msgs, sizes);
            }
            std::cout << "Sending done " << (non_blocking ? "(non-blocking)" : "(blocking)") << std::endl;
        } catch(std::exception& e) {
            std::cout << "\x1b[31m Unable to send, because: " << e.what() << " \x1b[0m" << std::endl;
        }

        performance_map[send_tag].send_counter++;
    }

    void on_data(uint64_t fid, const uint8_t* data, size_t size, uint8_t status) {
        printf("Message from 0x%lx\n", fid);
        map_mutex.lock();
        uint64_t send_tag = subscription_map[fid];

        auto perf = &performance_map[send_tag];
        struct timespec t1;
        clock_gettime(CLOCK_MONOTONIC_RAW, &t1);
        ++(perf->recv_counter);
        double ms = 1.0e-6*(t1.tv_nsec - perf->t0.tv_nsec);
        perf->times.push_back(ms);

        if(verbosity<6){
            printf("Message from 0x%lx:", fid);
            uint8_t *msg = (uint8_t*) data;
            for(unsigned int b=0; b<size; ++b){
                printf("\x1b[32m %02X ", msg[b]);
            }
            printf("status %u received after %f ms\x1b[0m\n", status, ms);
        }

        // Check CRC
        if (!felix::hdlc::verify_hdlc_trailer(data, size)) {
            printf("CRC incorrect\n");
            return_code = 1;
        }
        size -= 2;

        // verify answer
        if( reply_map.count(send_tag) > 0){
            for(auto m = reply_map.at(send_tag).begin(); m != reply_map.at(send_tag).end(); m++){
                if(compare_messages(data, size, *m)){
                    reply_map.at(send_tag).erase(m);
                    printf("Reply found!\n");
                    map_mutex.unlock();
                    return;
                }
            }
        }

        printf("Error Unexpected Reply\n");
        return_code = 1;
        map_mutex.unlock();
    }

    bool compare_messages(const uint8_t* msg, size_t size, std::vector<uint8_t> expected)
    {
        if(size != expected.size()){
            return false;
        }
        for(unsigned int b=0; b<size; ++b){
            if (msg[b] != expected.at(b)) {
                return false;
            }
        }
        return true;
    }

    void results(struct counters* perf, uint64_t fid){
        double avg{0.}, rms{0.}, N{0.};
        std::vector<double> times = perf->times;
        for(unsigned int i=1; i<times.size(); ++i){
            if (times.at(i)>0 && times.at(i) < timeout_ms){
                avg+=times.at(i);
                rms+=times.at(i)*times.at(i);
                N+=1.0;
            }
        }
        avg/=N;
        rms = sqrt(rms)/(N-1);
        std::cout << "\n \x1b[32m  FID: " << std::hex << fid << std::dec << " Averaged time on "<< (int)N << " measurements: " << avg << " +/- " << rms << " ms\x1b[0m\n"<< std::endl;
        this->client->unsubscribe(fid);
    }
};


int main(int argc, char** argv) {
    std::map<std::string, docopt::value> args
        = docopt::docopt(USAGE,
                         { argv + 1, argv + argc },
                         true,               // show help if requested
                         (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string

    Commands sca(args);
    return sca.return_code;
}
