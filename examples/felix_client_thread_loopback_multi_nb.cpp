#include <cstdio>
#include <cstring>
#include <string>
#include <thread>
#include <iostream>
#include <unistd.h>
#include <vector>
#include <map>
#include <mutex>

#include "docopt/docopt.h"

#include "felix/felix_client_properties.h"
#include "felix/felix_client_status.h"
#include "felix/felix_client_thread.hpp"
#include "felix/felix_client_exception.hpp"

#include "felixtag.h"
#include "felix/felix_fid.h"

static const char USAGE[] =
R"(felix-client-thread-loopback-multi - Subscribes to a number of tags and send info to a number of tags, checking if it is returned.

    Usage:
      felix-client-thread-loopback-multi [options] <local_ip_or_interface> <cid> <did> <elink> <no-of-elinks> <message>

    Options:
      -h --help                         Show this screen.
      --version                         Show version.
      --log-level=<loglevel>            Specify level of logging (trace, debug, info, warning, error, fatal) [default: info]
      --verbose-bus                     Show bus information
      --bus-dir=<directory>             Set directory for bus to use [default: ./bus]
      --bus-group-name=<group-name>     Set group-name for bus to use [default: FELIX]
      --timeout=<timeoutms>             Set timeout to subscribe (0 is no timeout) [default: 0]
      --netio-pages=<pages>             Number of pages for buffer [default: 256]
      --netio-pagesize=<pagesize>       Pagesize for buffer in bytes [default: 65536]
)";


class UserClass {

public:
    int return_code = 0;

    explicit UserClass(std::map<std::string, docopt::value> &args) {

        try {
            FelixClientThread::Config config;
            config.on_init_callback = std::bind(&UserClass::on_init, this);
            config.on_data_callback = std::bind(&UserClass::on_data, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
            config.on_connect_callback = std::bind(&UserClass::on_connect, this, std::placeholders::_1);
            config.on_disconnect_callback = std::bind(&UserClass::on_disconnect, this, std::placeholders::_1);

            config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] = args["<local_ip_or_interface>"].asString();
            config.property[FELIX_CLIENT_LOG_LEVEL] = args["--log-level"].asString();
            config.property[FELIX_CLIENT_BUS_DIR] = args["--bus-dir"].asString();
            config.property[FELIX_CLIENT_BUS_GROUP_NAME] = args["--bus-group-name"].asString();
            config.property[FELIX_CLIENT_VERBOSE_BUS] = args["--verbose-bus"].asBool() ? "True" : "False";
            config.property[FELIX_CLIENT_TIMEOUT] = args["--timeout"].asString();
            config.property[FELIX_CLIENT_NETIO_PAGES] = args["--netio-pages"].asString();
            config.property[FELIX_CLIENT_NETIO_PAGESIZE] = args["--netio-pagesize"].asString();

            client = new FelixClientThread(config);

            uint64_t cid = args["<cid>"].asLong();
            uint64_t did = args["<did>"].asLong();
            bool all_subscribed = false;
            bool all_send_connection_up = false;

            uint64_t elink = args["<elink>"].asLong();
            uint64_t no_of_elinks = args["<no-of-elinks>"].asLong();
            message = args["<message>"].asString();

            for (size_t i=0; i<no_of_elinks; i++) {
                std::lock_guard<std::mutex> lock(map_mutex);
                uint8_t stream_id = 0;
                uint8_t vid = 1;

                uint64_t subscribe_tag = get_fid_from_ids(did, cid, elink + i, stream_id, vid, 0, 0);
                uint64_t send_tag = get_fid_from_ids(did, cid, elink + i, stream_id, vid, 1, 0);

                subscribed[subscribe_tag] = false;
                sent[send_tag] = false;
                received[subscribe_tag] = false;
            }

            printf("Subscribing...\n");
            for (auto& subscription : subscribed) {
                client->subscribe(subscription.first);
            }

            for (auto& send_tag : sent) {
                client->init_send_data(send_tag.first);
            }

            std::string data_copy = message;

            printf("Waiting for subscriptions...\n");
            while (!all_subscribed || !all_send_connection_up) {
                sleep(1);
                std::lock_guard<std::mutex> lock(map_mutex);
                if (std::all_of(subscribed.cbegin(), subscribed.cend(), [](auto& sub){ return sub.second; }))
                {
                    all_subscribed = true;
                }
                if (std::all_of(sent.cbegin(), sent.cend(), [](auto& s){ return s.second; }))
                {
                    all_send_connection_up = true;
                }     
            }
            sleep(2);
            printf("Sending messages...\n");
            for(auto& sender : sent) {
                try {
                    std::cout << "Sending to: " << std::hex << sender.first << std::dec << std::endl;
                    client->send_data_nb(sender.first, (const uint8_t*)data_copy.c_str(), data_copy.size() + 1, true);
                } catch(...){
                    printf("failed to send 0x%lx...\n", sender.first);
                }
            }
            data_copy = "";
            // printf("Waiting for send connections...\n");
            // bool all_sent = false;
            // while (!all_sent) {
            //     all_sent = true;
            //     map_mutex.lock();
            //     for(auto& sender : sent) {
            //         all_sent &= sender.second;
            //     }
            //     map_mutex.unlock();
            //     sleep(1);
            // }

            printf("Checking for replies...\n");
            bool all_replied = false;
            while (!all_replied) {
                size_t outstanding = 0;
                all_replied = true;
                sleep(1);
                std::lock_guard<std::mutex> lock(map_mutex);
                for(auto& receive : received) {
                    outstanding += receive.second ? 0 : 1;
                    all_replied &= receive.second;
                }
                printf("outstanding %zu\n", outstanding);

            }

            printf("Unsubscribing...\n");
            for (auto& subscription : subscribed) {
                client->unsubscribe(subscription.first);
            }

            // printf("Waiting for unsubscriptions...\n");
            // bool some_subscribed = true;
            // while (some_subscribed) {
            //     some_subscribed = false;
            //     map_mutex.lock();
            //     for(auto& subscription : subscribed) {
            //         some_subscribed |= subscription.second;
            //     }
            //     map_mutex.unlock();
            //     sleep(1);
            // }

            return_code = 42;
        } catch (std::invalid_argument const& error) {
            std::cerr << "Argument or option of wrong type" << std::endl;
            std::cout << std::endl;
            std::cout << USAGE << std::endl;
            return_code = -1;
        } catch (FelixClientResourceNotAvailableException& error) {
            printf("Failed to send\n");
            return_code = 1;
        }

    }

    ~UserClass() {
        delete client;
    }


private:
    FelixClientThread* client;

    std::string message;

    std::mutex map_mutex;
    std::map<uint64_t, bool> subscribed;
    std::map<uint64_t, bool> sent;
    std::map<uint64_t, bool> received;

    void on_init() {
        printf("on_init called\n");
    }

    void on_connect(uint64_t fid) {
        printf("on_connect called 0x%lx\n", fid);

        std::lock_guard<std::mutex> lock(map_mutex);
        if (subscribed.count(fid)) {
            subscribed[fid] = true;
        } else if (sent.count(fid)) {
            sent[fid] = true;
        } else {
            printf("ERROR: on_connect() unknown fid 0x%lx\n", fid);
        }
    }

    void on_disconnect(uint64_t fid) {
        printf("on_disconnect called 0x%lx\n", fid);

        std::lock_guard<std::mutex> lock(map_mutex);
        if (subscribed.count(fid)) {
            subscribed[fid] = false;
        } else if (sent.count(fid)) {
            sent[fid] = false;
        } else {
            printf("ERROR: on_connect() unknown fid 0x%lx\n", fid);
        }
    }

    void on_data(uint64_t fid, const uint8_t* data, size_t size, uint8_t status) {
        printf("on_data called for 0x%lx with size %zu and status %u\n", fid, size, status);


        if (received.count(fid)) {
            if (size > 0) {
                std::lock_guard<std::mutex> lock(map_mutex);
                std::string str((char*)data, std::min(size, strlen((const char*)data)));
                std::cout << "'" << str << "'" << std::endl;
                std::cout << "'" << message << "'" << std::endl;
                if (!str.compare(message)) {
                    std::cout << "Expected message received." << std::endl;
                    received[fid] = true;
                }
            }
        } else {
            printf("ERROR: on_data() unknown fid 0x%lx\n", fid);
        }
    }
};


int main(int argc, char** argv) {
    std::map<std::string, docopt::value> args
            = docopt::docopt(USAGE,
                            { argv + 1, argv + argc },
                            true,               // show help if requested
                            (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string

    UserClass userClass(args);
    return userClass.return_code;
}
