#include <cstdio>
#include <cmath>
#include <iostream>
#include <string>
#include <unistd.h>

#include "docopt/docopt.h"

#include "felix/felix_client.hpp"
#include "felix/felix_client_status.h"
#include "felix/felix_client_util.hpp"

#include "hdlc_coder/hdlc.hpp"

#include "felixtag.h"


//Constants from ScaSoftware HldcBackendCommon.h
const uint8_t NETIO_FRAME_HDLC_START_OFFSET = 8;  // HDLC frame shall be placed starting from this index
const uint8_t HDLC_FRAME_PAYLOAD_OFFSET = 2;      // HDLC payload offset in HDLC frame
const uint8_t HDLC_FRAME_TRAILER_SIZE = 2;        // HDLC trailer size in HDLC frame

const uint8_t CHIP_ID_REQ[8] = {0x0, 0x14, 0x04, 0xD1, 0x0, 0x0, 0x1, 0x0}; //Message to request the chip Id
const uint8_t SCA_MSG_SIZE = 8;


static const char USAGE[] =
R"(felix-client-sca-loopback - Exchanges messages with a GBT-SCA.

    Usage:
      felix-client-sca-loopback [options] <local_ip_or_interface> <subscribe-tag> <send-tag> <iterations>

    Options:
      -h --help                         Show this screen.
      --version                         Show version.
      --log-level=<loglevel>            Specify level of logging (trace, debug, info, warning, error, fatal) [default: info]
      --verbose-bus                     Show bus information
      --bus-dir=<directory>             Set directory for bus to use [default: ./bus]
      --bus-group-name=<group-name>     Set group-name for bus to use [default: FELIX]
      --netio-pages=<pages>             Number of pages for buffer [default: 256]
      --netio-pagesize=<pagesize>       Pagesize for buffer in bytes [default: 65536]
)";

class Loopback {

public:
    explicit Loopback(std::map<std::string, docopt::value> &args) {
        try {

            unsigned log_level = get_log_level(args["--log-level"].asString());
            client = new FelixClient(args["<local_ip_or_interface>"].asString(),
                                            args["--bus-dir"].asString(),
                                            args["--bus-group-name"].asString(),
                                            log_level,
                                            args["--verbose-bus"].asBool(),
                                            args["--netio-pages"].asLong(),
                                            args["--netio-pagesize"].asLong());
            subscribe_tag = args["<subscribe-tag>"].asLong();
            send_tag = args["<send-tag>"].asLong();
            reps = args["<iterations>"].asLong();
            verbosity = log_level;

        } catch (std::invalid_argument const& error) {
            std::cerr << "Argument or option of wrong type" << std::endl;
            std::cout << std::endl;
            std::cout << USAGE << std::endl;
            return_value = -1;
        }
        sleep(2);
        client->callback_on_init(std::bind(&Loopback::on_init, this));
        client->callback_on_data(std::bind(&Loopback::on_data, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
        client->callback_on_connect(std::bind(&Loopback::on_connect, this, std::placeholders::_1));
        client->callback_on_disconnect(std::bind(&Loopback::on_disconnect, this, std::placeholders::_1));
        client->callback_on_user_timer(std::bind(&Loopback::on_reply_timeout, this));

        reset_msg = (uint8_t*)calloc( reset_msg_size, sizeof(uint8_t));
        scaid_msg = (uint8_t*)calloc( scaid_msg_size, sizeof(uint8_t));

        felix::hdlc::encode_hdlc_ctrl_header(reset_msg, 0, 143); //0x8f
	    felix::hdlc::encode_hdlc_trailer(
	        reset_msg + HDLC_FRAME_PAYLOAD_OFFSET, // where to put the trailer
	        reset_msg,
	        HDLC_FRAME_PAYLOAD_OFFSET);
        /*
        try {
            //client->subscribe(subscribe_tag);
            //send the first

        } catch (const std::exception& e) {
            printf("Failed to send\n");
            delete client;
            exit(1);
        }
        */

        client->run();
        while(true){
            sleep(30);
        }
    }

    ~Loopback() {
        delete client;
    }
    int return_value = 55;

private:
    unsigned verbosity;
    FelixClient* client;

    //links
    uint64_t subscribe_tag;
    uint64_t send_tag;
    uint8_t seqnr = 1;

    //messages
    uint8_t* reset_msg;
    size_t reset_msg_size = (HDLC_FRAME_PAYLOAD_OFFSET + HDLC_FRAME_TRAILER_SIZE);

    uint8_t* scaid_msg;
    size_t scaid_msg_size = (HDLC_FRAME_PAYLOAD_OFFSET + SCA_MSG_SIZE + HDLC_FRAME_TRAILER_SIZE);

    //counters
    long reps = 0;
    long send_counter = 0;
    long recv_counter = 0;
    unsigned long timeout_ms = 1000;

    bool connected_tohost = false;
    bool connected_toflx = false;

    struct timespec t0;
    struct timespec t1;
    std::vector<double> times;

    //methods
    void on_init() {
        printf("Inititalising connections...\n");
        client->init_send_data(send_tag);
        client->user_timer_init();
        client->subscribe(subscribe_tag);
    }

    void on_connect(netio_tag_t fid) {
        printf("Connection up for fid 0x%lx\n", fid);
        if(fid == subscribe_tag){connected_tohost = true;}
        if(fid == send_tag){connected_toflx = true;}
        if(connected_tohost && connected_toflx){this->send_reset(send_tag);}
    }

    void on_disconnect(netio_tag_t fid) {
        printf("Dicsconnection for fid 0x%lx\n", fid);
    }

    void send_reset(uint64_t send_tag) {
        clock_gettime(CLOCK_MONOTONIC_RAW, &t0);
        client->send_data(send_tag, (const uint8_t*)reset_msg, reset_msg_size, true);
        client->user_timer_start(timeout_ms);
        ++send_counter;
        if(verbosity<6){
            printf("Sent reset command: ");
            for(unsigned int b=0; b<reset_msg_size; ++b){printf("%02X ", reset_msg[b]);}
            printf("\n");
        }
    }

    void send_scaid_req(uint64_t fid) {
        this->encode_request(scaid_msg, CHIP_ID_REQ, seqnr);
        clock_gettime(CLOCK_MONOTONIC_RAW, &t0);
        client->send_data(send_tag, (const uint8_t*)scaid_msg, scaid_msg_size, true);
        client->user_timer_start(timeout_ms);
        ++send_counter;
        ++seqnr;
        if(verbosity<6){
            printf("Sent sca ID request: ");
            for(unsigned int b=0; b<reset_msg_size; ++b){printf("%02X ", reset_msg[b]);}
            printf("\n");
        }
    }

    void on_data(uint64_t fid, const uint8_t* data, size_t size, uint8_t status) {
        struct timespec t2;
        clock_gettime(CLOCK_MONOTONIC_RAW, &t2);
        client->user_timer_stop();
        ++recv_counter;
        double ms = 1.0e-6*(t2.tv_nsec - t0.tv_nsec);
        times.push_back(ms);
        if(verbosity<6){
            printf("Message from 0x%lx: ", fid);
            uint8_t *msg = (uint8_t*) data;
            for(unsigned int b=0; b<size; ++b){
                printf("%02X ", msg[b]);
            }
            printf("status %u received after %f ms\n", status, ms);
        }
        if(recv_counter==reps){results();}
        //usleep(100000);
        this->send_reset(send_tag);
    }

    void on_reply_timeout(){
        printf("Reply timeout. Sending a new request."); fflush(stdout);
        times.push_back(timeout_ms);

        this->send_reset(send_tag);
    }

    void results(){
        double avg{0.}, rms{0.}, N{0.};
        for(unsigned int i=1; i<times.size(); ++i){
            if (times.at(i)>0 && times.at(i) < timeout_ms){
                avg+=times.at(i);
                rms+=times.at(i)*times.at(i);
                N+=1.0;
            }
        }
        avg/=N;
        rms = sqrt(rms)/(N-1);
        std::cout << "\nAveraged time on "<< (int)N << " measurements: " << avg << " +/- " << rms << " ms\n"<< std::endl;
        client->stop();
        return_value = 0;
    }

    void encode_request(uint8_t* hdlc_encoded_msg, const uint8_t* msg, uint8_t seqnr){
        felix::hdlc::encode_hdlc_msg_header(hdlc_encoded_msg, 0, seqnr); //HDLC address, always 0 for SCA
        memcpy((void*)(hdlc_encoded_msg+HDLC_FRAME_PAYLOAD_OFFSET), msg, SCA_MSG_SIZE);
        felix::hdlc::encode_hdlc_trailer(
                hdlc_encoded_msg + HDLC_FRAME_PAYLOAD_OFFSET + SCA_MSG_SIZE, //where to put the trailer
                hdlc_encoded_msg, // pointer to the beginning of HDLC frame
                SCA_MSG_SIZE + HDLC_FRAME_PAYLOAD_OFFSET); //total size
    }


};


int main(int argc, char** argv) {
    std::map<std::string, docopt::value> args
        = docopt::docopt(USAGE,
                         { argv + 1, argv + argc },
                         true,               // show help if requested
                         (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string

    Loopback sca(args);
    return sca.return_value;
}
