#include <cstdio>
#include <string>
#include <cmath>
#include <thread>
#include <iostream>
#include <unistd.h>
#include <vector>
#include <map>
#include <atomic>
#include <sys/types.h>
#include <unistd.h>

#include "docopt/docopt.h"

#include "felix/felix_client_properties.h"
#include "felix/felix_client_status.h"
#include "felix/felix_client_exception.hpp"
#include "felix/felix_client_thread.hpp"

#include "felixtag.h"

static const char USAGE[] =
R"(felix-client-thread-subscribe-loop - Subscribes to a felix-id using felix-bus to find the publisher.

    Usage:
      felix-client-thread-subscribe-loop [options] <iterations> <local_ip_or_interface> <fids>...

    Options:
      -h --help                         Show this screen.
      --version                         Show version.
      --log-level=<loglevel>            Specify level of logging (trace, debug, info, warning, error, fatal) [default: info]
      --verbose-bus                     Show bus information
      --bus-dir=<directory>             Set directory for bus to use [default: ./bus]
      --bus-group-name=<group-name>     Set group-name for bus to use [default: FELIX]
      --timeout=<timeoutms>             Set timeout to subscribe (0 is no timeout) [default: 0]
      --messages=<no_messages>          Exit after no_messages are received [default: 0]
      --netio-pages=<pages>             Number of pages for buffer [default: 256]
      --netio-pagesize=<pagesize>       Pagesize for buffer in bytes [default: 65536]
)";


struct {
    struct timespec t0;
    uint64_t expected_messages;
    uint64_t total_messages_received;
    uint64_t messages_received;
    uint64_t bytes_received;

    std::vector<uint64_t> elinks;
    std::map<uint64_t, uint64_t> counters;
    std::map<uint64_t, uint64_t> truncated;
    std::map<uint64_t, uint64_t> crc;
    std::map<uint64_t, uint64_t> error;
} statistics;



class UserClass {

public:
    explicit UserClass(std::map<std::string, docopt::value> &args) {

        try {
            statistics.expected_messages = args["--messages"].asLong();
            uint64_t iterations  = args["<iterations>"].asLong();

            std::cout << "Runnning for " << iterations << " iterations." << std::endl;

            pid_t pid = getpid(); // long unsigned

            FelixClientThread::Config config;
            config.on_init_callback = std::bind(&UserClass::on_init, this);
            config.on_data_callback = std::bind(&UserClass::on_data, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
            config.on_connect_callback = std::bind(&UserClass::on_connect, this, std::placeholders::_1);
            config.on_disconnect_callback = std::bind(&UserClass::on_disconnect, this, std::placeholders::_1);

            config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] = args["<local_ip_or_interface>"].asString();
            config.property[FELIX_CLIENT_LOG_LEVEL] = args["--log-level"].asString();
            config.property[FELIX_CLIENT_BUS_DIR] = args["--bus-dir"].asString();
            config.property[FELIX_CLIENT_BUS_GROUP_NAME] = args["--bus-group-name"].asString();
            config.property[FELIX_CLIENT_VERBOSE_BUS] = args["--verbose-bus"].asBool() ? "True" : "False";
            config.property[FELIX_CLIENT_TIMEOUT] = args["--timeout"].asString();
            config.property[FELIX_CLIENT_NETIO_PAGES] = args["--netio-pages"].asString();
            config.property[FELIX_CLIENT_NETIO_PAGESIZE] = args["--netio-pagesize"].asString();

            std::string last_fds = "";
            std::string fds;

            for (uint64_t i = 0; i < iterations; i++){
                client = new FelixClientThread(config);
                FelixClientThread::UserFunction user_function = std::bind(&UserClass::on_exec, this);
                client->exec(user_function);

                clock_gettime(CLOCK_MONOTONIC_RAW, &statistics.t0);
                std::vector<std::string> fid_list = args["<fids>"].asStringList();
                total_fids = fid_list.size();
                for(std::vector<std::string>::iterator it = fid_list.begin(); it != fid_list.end(); ++it) {
                    uint64_t fid = std::stoul(*it, 0, 0);
                    statistics.elinks.push_back(fid);
                    statistics.counters[fid] = 0;
                    statistics.truncated[fid] = 0;
                    statistics.crc[fid] = 0;
                    statistics.error[fid] = 0;
                    client->subscribe(fid);
                }
                ++fids_done;
                printf("All FIDs are subscribed. Waiting for incoming messages. Subscribed FIDs: %lu\n",  fids_connect.load());

                sleep(3);
                for(std::vector<std::string>::iterator it = fid_list.begin(); it != fid_list.end(); ++it) {
                    uint64_t fid = std::stoul(*it, 0, 0);
                    client->unsubscribe(fid);
                }
                // delete client;
                //std::string fds = execCommand("lsof -a -p " + std::to_string(pid));// + " | wc -l");
                //std::cout << fds << std::endl;

                fds = execCommand("lsof -a -p " + std::to_string(pid) + " | wc -l");
                std::cout << "Iteration: " << i << ": Number of File Descrpiptors: " <<  fds << std::endl;
                if (last_fds != "" && last_fds != fds){
                    std::cerr << "Number of FDs is increasing over time." << std::endl;
                    return_code = 2;
                }
                last_fds = fds;
            }
            return_code = 0;

        } catch (FelixClientResourceNotAvailableException& exc) {
            std::cerr << "Timed out on subscription" << std::endl;
            return_code = 1;
        } catch (std::invalid_argument const& error) {
            std::cerr << "Argument or option of wrong type" << std::endl;
            std::cout << std::endl;
            std::cout << USAGE << std::endl;
            return_code = -1;
        }
    }

    ~UserClass() {
        delete client;
    }
    int return_code = 0;

private:
    void on_init() {
        printf("on_init called\n");
    }

    void on_connect(uint64_t fid) {
        printf("on_connect called 0x%lx\n", fid);
        ++fids_connect;
    }

    void on_disconnect(uint64_t fid) {
        printf("on_disconnect called 0x%lx\n", fid);
    }

    void on_data(uint64_t fid, const uint8_t* data, size_t size, uint8_t status) {
        //printf("on_data called for %lu with size %lu and status %u\n", fid, size, status);
        if(fids_connect.load() == total_fids || fids_done.load()){
            statistics.total_messages_received++;

            statistics.messages_received++;
            statistics.bytes_received += size;
            statistics.counters[fid]++;

            // if ((statistics.expected_messages > 0) && (statistics.total_messages_received >= statistics.expected_messages)) {
            //     printf("Expected messages received\n");
            //     exit(42);
            // }

            if (status == FELIX_STATUS_FW_TRUNC || status == FELIX_STATUS_SW_TRUNC) {
                statistics.truncated[fid]++;
            }
            if (status == FELIX_STATUS_FW_CRC) {
                statistics.crc[fid]++;
            }
            if (status == FELIX_STATUS_FW_MALF || status == FELIX_STATUS_SW_MALF) {
                statistics.error[fid]++;
            }

            // statistics
            struct timespec t1;
            clock_gettime(CLOCK_MONOTONIC_RAW, &t1);
            double seconds = t1.tv_sec - statistics.t0.tv_sec
                            + 1e-9*(t1.tv_nsec - statistics.t0.tv_nsec);
            if (seconds < 1.0) {
                return;
            }

            printf("data rate: %2f Gb/s   message rate: %2f kHz     ",
                    statistics.bytes_received*8/1000./1000./1000./seconds,
                    statistics.messages_received/1000./seconds);
            statistics.bytes_received = 0;
            statistics.messages_received = 0;
            statistics.t0 = t1;

            printf("FID, chunks, trunc, err, crc: { ");
            for(unsigned i=0; i<statistics.elinks.size(); i++) {
                uint64_t elink = statistics.elinks[i];
                printf("0x%lx:%lu:%lu:%lu:%lu ", elink, statistics.counters[elink], statistics.truncated[elink], statistics.error[elink], statistics.crc[elink]);
                if (i>=8) {
                    printf("... ");
                    break;
                }
            }
            printf("}\n");
        }
    }

    void on_exec() {
        printf("on_exec called\n");
    }

    std::string execCommand(const std::string& cmd)
    {
        auto pPipe = ::popen(cmd.c_str(), "r");
        if(pPipe == nullptr)
        {
            throw std::runtime_error("Cannot open pipe");
        }

        std::array<char, 256> buffer;
        std::string result;

        while(not std::feof(pPipe))
        {
            auto bytes = std::fread(buffer.data(), 1, buffer.size(), pPipe);
            result.append(buffer.data(), bytes);
        }

        ::pclose(pPipe);
        return result;
    }

    std::atomic<uint64_t> fids_connect = 0;
    std::atomic<uint64_t> fids_done = 0;
    uint64_t total_fids;
    FelixClientThread* client;
};


int main(int argc, char** argv) {
    std::map<std::string, docopt::value> args
            = docopt::docopt(USAGE,
                            { argv + 1, argv + argc },
                            true,               // show help if requested
                            (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string

    UserClass userClass(args);
    return userClass.return_code;
}
