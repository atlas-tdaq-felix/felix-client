#include <cstdio>
#include <cstring>

#include <string>
#include <thread>
#include <iostream>
#include <unistd.h>
#include <vector>
#include <map>

#include "docopt/docopt.h"

#include "felixtag.h"

#include "clog.h"

static const char USAGE[] =
R"(felix-client-thread-log - Test log function.

    Usage:
      felix-client-thread-log [options]

    Options:
      -h --help                         Show this screen.
      --version                         Show version.
)";

class UserClass {

public:
    explicit UserClass(std::map<std::string, docopt::value> &args) {

        clog_set_level(CLOG_TRACE);

        clog_trace("T Interface");
        clog_debug("D Interface");
        clog_info("I Interface");
        clog_warn("W Interface");
        clog_error("E Interface");
        clog_fatal("F Interface");
    }

    int return_code = 0;

    ~UserClass() {
    }
};

int main(int argc, char** argv) {
    std::map<std::string, docopt::value> args
            = docopt::docopt(USAGE,
                            { argv + 1, argv + argc },
                            true,               // show help if requested
                            (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string

    UserClass userClass(args);
    return userClass.return_code;
}
