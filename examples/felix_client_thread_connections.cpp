#include <cstdio>
#include <cstdlib>
#include <string>
#include <cmath>
#include <iostream>
#include <unistd.h>
#include <vector>
#include <map>
#include <atomic>
#include <thread>
#include <mutex>
#include <condition_variable>

#include "docopt/docopt.h"

#include "felix/felix_client_properties.h"
#include "felix/felix_client_status.h"
#include "felix/felix_client_exception.hpp"
#include "felix/felix_client_thread.hpp"

#include "felixtag.h"

static const char USAGE[] =
R"(felix-client-thread-subscribe - Subscribes and unsubscribes to elinks. One felix-client-thread per elink.

    Usage:
      felix-client-thread-nsw [options] <local_ip_or_interface> <fids>...

    Options:
      -h --help                         Show this screen.
      --version                         Show version.
      --log-level=<loglevel>            Specify level of logging (trace, debug, info, warning, error, fatal) [default: info]
      --verbose-bus                     Show bus information
      --bus-dir=<directory>             Set directory for bus to use [default: ./bus]
      --bus-group-name=<group-name>     Set group-name for bus to use [default: FELIX]
      --timeout=<T>                     Set timeout to subscribe (0 is no timeout) [default: 0]
      --messages=<N>                    Disconnect after N messages [default: 0]
      --lifetime=<L>                    Disconnect after L seconds. [default: 0]
      --random                          Disconnect after a random time between 1 and T seconds.
      --iterations=<N>                  Repeat the program N times [default: 1]
      --evloop                          Unsubscribe using evlopp (via felix-client-thread exec)
)";

std::map<std::string, docopt::value> args;
std::atomic<uint64_t> connected = 0;
std::atomic<uint64_t> disconnected = 0;


class ElinkClient {

public:
    explicit ElinkClient(uint64_t fid,  uint64_t delay_ms) {

        try {
            printf("[fid 0x%lx] Starting client", fid);
            FelixClientThread::Config config;
            config.on_data_callback = std::bind(&ElinkClient::on_data, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
            config.on_connect_callback = std::bind(&ElinkClient::on_connect, this, std::placeholders::_1);
            config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] = args["<local_ip_or_interface>"].asString();
            config.property[FELIX_CLIENT_LOG_LEVEL] = args["--log-level"].asString();
            config.property[FELIX_CLIENT_BUS_DIR] = args["--bus-dir"].asString();
            config.property[FELIX_CLIENT_BUS_GROUP_NAME] = args["--bus-group-name"].asString();
            config.property[FELIX_CLIENT_VERBOSE_BUS] = args["--verbose-bus"].asBool() ? "True" : "False";
            config.property[FELIX_CLIENT_TIMEOUT] = args["--timeout"].asString();

            m_elink = fid;
            m_subscribed = false;
            m_lifetime = args["--lifetime"].asLong();
            m_expected_messages = args["--messages"].asLong();
            m_evloop = args["--evloop"].asBool() ? true : false;

            bool random = args["--random"].asBool() ? true : false;
            if(m_lifetime && random){
                m_lifetime = rand() % m_lifetime + 1;
            }

            usleep(1e3*delay_ms);
            client = std::make_unique<FelixClientThread>(config);

        } catch (std::invalid_argument const& error) {
            std::cerr << "Argument or option of wrong type" << std::endl;
            std::cout << std::endl;
            std::cout << USAGE << std::endl;
        }
        printf("[fid 0x%lx] Starting run.\n", m_elink);
    }



    void subscribe(){
        std::unique_lock lk(mtx);
        try {
            std::cout << "Subscribung to link: " << std::hex << m_elink <<std::dec << std::endl;
            client->subscribe(m_elink);
        } catch (FelixClientResourceNotAvailableException& exc) {
            printf("[fid 0x%lx] Subscription timeout, disconnecting.\n", m_elink);
            return;
        }

        if(m_lifetime){
            sleep(m_lifetime);
            if(!m_subscribed){return;}
            m_subscribed = false;
            if(m_evloop){
                FelixClientThread::UserFunction unsubscribe_client = std::bind(&ElinkClient::disconnect, this);
                client->exec(unsubscribe_client);
                ++disconnected;
                printf("[fid 0x%lx] Client expiration (%lu s) via evloop. Unsubscribed fids %ld \n", m_elink, m_lifetime, disconnected.load());
            }
            else{
                client->unsubscribe(m_elink);
                ++disconnected;
                printf("[fid 0x%lx] Client expiration (%lu s). Unsubscribed fids %ld \n", m_elink, m_lifetime, disconnected.load());
            }
            printf("[fid 0x%lx] end of run\n", m_elink);
            sleep(2);
            return;
        }
        else{
            printf("[fid 0x%lx] Client returning from run.\n", m_elink);
            m_running.wait(lk);
            return;
        }
    }

private:

    void on_connect(uint64_t fid) {
        ++connected;
        m_subscribed = true;
        printf("[fid 0x%lx] on_connect called. Connections up %ld\n", fid, connected.load());
    }

    void on_data(uint64_t fid, const uint8_t* data, size_t size, uint8_t status) {
        if(!m_subscribed) {return;}
        m_messages_received++;
        if ((m_expected_messages > 0) && (m_messages_received >= m_expected_messages)) {
            m_subscribed = false;
            FelixClientThread::UserFunction unsubscribe_client = std::bind(&ElinkClient::disconnect, this);
            if(m_evloop){
                client->exec(unsubscribe_client);
                ++disconnected;
                printf("[fid 0x%lx] Expected messages received. Unsubscribed fids %ld \n", fid, disconnected.load());
                m_running.notify_one();
            }
            else{
                client->unsubscribe(m_elink);
                ++disconnected;
                printf("[fid 0x%lx] Expected messages received. Unsubscribed fids %ld \n", fid, disconnected.load());
                m_running.notify_one();
            }
        }
    }

    void disconnect() {
        client->unsubscribe(m_elink);
    }

    std::unique_ptr<FelixClientThread> client;
    std::mutex mtx;
    std::condition_variable m_running;

    uint64_t m_elink;
    uint64_t m_lifetime;
    bool m_evloop;
    bool m_subscribed;

    uint64_t m_expected_messages;
    uint64_t m_messages_received;
};

int main(int argc, char** argv) {
    args = docopt::docopt(USAGE,
                    { argv + 1, argv + argc },
                    true,               // show help if requested
                    (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string

    std::vector<std::string> fid_list = args["<fids>"].asStringList();
    printf("Number of elinks %lu\n", (unsigned long)fid_list.size());

    unsigned long iterations = args["--iterations"].asLong();
    std::vector<std::unique_ptr<ElinkClient>> clients;
    std::vector<std::thread> threads;
    threads.reserve(fid_list.size());
    clients.reserve(fid_list.size());

    for(unsigned long i=0; i<iterations; ++i){
        printf("============================ Iteration %lu ============================\n", i);
        uint64_t delay = 0;
        for(auto& s : fid_list) {
            uint64_t fid = std::stoul(s, 0, 0);
            clients.emplace_back(std::make_unique<ElinkClient>(fid, delay));
            threads.emplace_back(&ElinkClient::subscribe, clients.back().get());
            delay+=1;
        }

        for(auto& th : threads){
            th.join();
        }
        connected = 0;
        disconnected = 0;
        clients.clear();
        threads.clear();
        printf("All threads finished. End of iteration %lu/%lu\n", i+1, iterations);
    }
    printf("End of all iterations\n");
    return 0;
}

