#include <cstdio>
#include <iostream>
#include <string>

#include "docopt/docopt.h"

#include "felix/felix_client.hpp"
#include "felix/felix_client_status.h"

#include "felix/felix_client_util.hpp"

#include "felixtag.h"

static const char USAGE[] =
R"(felix-client-subscribe - Subscribes to a felix-id using felix-bus to find the publisher.

    Usage:
      felix-client-subscribe [options] <local_ip_or_interface> <fids>...

    Options:
      -h --help                         Show this screen.
      --version                         Show version.
      --log-level=<loglevel>            Specify level of logging (trace, debug, info, warning, error, fatal) [default: info]
      --verbose-bus                     Show bus information
      --bus-dir=<directory>             Set directory for bus to use [default: ./bus]
      --bus-group-name=<group-name>     Set group-name for bus to use [default: FELIX]
      --timeout=<timeoutms>             Set timeout to subscribe (0 is no timeout) [default: 0]
      --messages=<no_messages>          Exit after no_messages are received [default: 0]
      --netio-pages=<pages>             Number of pages for buffer [default: 256]
      --netio-pagesize=<pagesize>       Pagesize for buffer in bytes [default: 65536]
)";

struct {
    struct timespec t0;
    uint64_t expected_messages;
    uint64_t total_messages_received;
    uint64_t messages_received;
    uint64_t bytes_received;

    std::vector<uint64_t> elinks;
    std::map<uint64_t, uint64_t> counters;
    std::map<uint64_t, uint64_t> truncated;
    std::map<uint64_t, uint64_t> crc;
    std::map<uint64_t, uint64_t> error;

    FelixClient* client;
} statistics;
int return_code = 0;

void on_init() {
    printf("on_init called\n");
}

void on_connect(netio_tag_t fid) {
    printf("on_connect called 0x%lx\n", fid);
}

void on_disconnect(netio_tag_t fid) {
    printf("on_disconnect called 0x%lx\n", fid);
}

void on_data(netio_tag_t fid, const uint8_t* data, size_t size, uint8_t status) {
    // printf("on_data called for %lu with size %lu and status %u\n", fid, size, status);
    statistics.total_messages_received++;

    if ((statistics.expected_messages > 0) && (statistics.total_messages_received >= statistics.expected_messages)) {
        printf("Expected messages received\n");

        statistics.client->stop();
        return_code = 42;
    }

    statistics.messages_received++;
    statistics.bytes_received += size;
    statistics.counters[fid]++;

    if (status == FELIX_STATUS_FW_TRUNC || status == FELIX_STATUS_SW_TRUNC) {
        statistics.truncated[fid]++;
    }
    if (status == FELIX_STATUS_FW_CRC) {
        statistics.crc[fid]++;
    }
    if (status == FELIX_STATUS_FW_MALF || status == FELIX_STATUS_SW_MALF) {
        statistics.error[fid]++;
    }

    // statistics
    struct timespec t1;
    clock_gettime(CLOCK_MONOTONIC_RAW, &t1);
    double seconds = t1.tv_sec - statistics.t0.tv_sec
                    + 1e-9*(t1.tv_nsec - statistics.t0.tv_nsec);
    if (seconds < 1.0) {
        return;
    }

    printf("data rate: %2f Gb/s   message rate: %2f kHz     ",
            statistics.bytes_received*8/1000./1000./1000./seconds,
            statistics.messages_received/1000./seconds);
    statistics.bytes_received = 0;
    statistics.messages_received = 0;
    statistics.t0 = t1;

    printf("FID, chunks, trunc, err, crc: { ");
    for(unsigned i=0; i<statistics.elinks.size(); i++) {
        uint64_t elink = statistics.elinks[i];
        printf("0x%lx:%lu:%lu:%lu:%lu ", elink, statistics.counters[elink], statistics.truncated[elink], statistics.error[elink], statistics.crc[elink]);
        if (i>=8) {
            printf("... ");
            break;
        }
    }
    printf("}\n");
}

void on_exec() {
    printf("on_exec called\n");
}

int main(int argc, char** argv) {
    std::map<std::string, docopt::value> args
        = docopt::docopt(USAGE,
                         { argv + 1, argv + argc },
                         true,               // show help if requested
                         (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string

    try {
        statistics.expected_messages = args["--messages"].asLong();

        unsigned log_level = get_log_level(args["--log-level"].asString());

        statistics.client = new FelixClient(args["<local_ip_or_interface>"].asString(),
                                            args["--bus-dir"].asString(),
                                            args["--bus-group-name"].asString(),
                                            log_level,
                                            args["--verbose-bus"].asBool(),
                                            args["--netio-pages"].asLong(),
                                            args["--netio-pagesize"].asLong());

        statistics.client->callback_on_init(on_init);
        statistics.client->callback_on_data(on_data);
        statistics.client->callback_on_connect(on_connect);
        statistics.client->callback_on_disconnect(on_disconnect);

        FelixClient::UserFunction user_function = on_exec;
        statistics.client->exec(user_function);

        clock_gettime(CLOCK_MONOTONIC_RAW, &statistics.t0);

        std::vector<std::string> fid_list = args["<fids>"].asStringList();
        for(std::vector<std::string>::iterator it = fid_list.begin(); it != fid_list.end(); ++it) {
            netio_tag_t fid = std::stoul(*it, 0, 0);
            statistics.elinks.push_back(fid);
            statistics.counters[fid] = 0;
            statistics.truncated[fid] = 0;
            statistics.crc[fid] = 0;
            statistics.error[fid] = 0;
            // FIXME check for status
            statistics.client->subscribe(fid, args["--timeout"].asLong());
        }
        statistics.client->run();

        return return_code;
    } catch (std::invalid_argument const& error) {
        std::cerr << "Argument or option of wrong type" << std::endl;
        std::cout << std::endl;
        std::cout << USAGE << std::endl;
        return -1;
    }

}
