#include <cstdio>
#include <ctime>
#include <string>
#include <ostream>
#include <iostream>

#include "docopt/docopt.h"

#include "felixbus/felixbus.h"

#include "felix/felix_fid.h"
#include "felix/felix_toflx.h"

#include "netio/netio.h"
#include "netio/netio_tcp.h"

#include "felixtag.h"

#define NUM_PAGES (256)
#define WATERMARK (56*1024)
#define PAGESIZE (64*1024)

static const char USAGE[] =
R"(netio-bus-buffered-recv - Receive some (buffered) data from host/port under certain felix-id, and inform felix-bus.

    Usage:
      netio-bus-buffred-recv [options] <hostname> <port> <tags>...

    Options:
      -h --help                         Show this screen.
      --version                         Show version.
      --verbose-bus                     Show bus information
      --bus-dir=<bus-directory>         Set bus directory [default: ./bus]
      --bus-group-name=<group-name>     Set group-name for bus to use [default: FELIX]
)";

struct {
    std::string hostname;
    unsigned port;
    std::vector<netio_tag_t> tags;
    std::string bus_directory;
    std::string bus_group_name;
    bool verbose_bus;

    struct netio_context ctx;
    struct netio_buffered_listen_socket socket;
    struct netio_buffered_socket_attr attr;
} config;

struct {
    struct netio_timer timer;
    struct timespec t0;
    uint64_t messages_received;
    uint64_t bytes_received;
} statistics;

// Forward declarations
void on_connection_established(struct netio_buffered_recv_socket*);
void on_connection_closed(struct netio_buffered_recv_socket*);
void on_msg_received(struct netio_buffered_recv_socket*, void* data, size_t len);
void on_stats(void* ptr);

// Callbacks
void on_init(void *ptr)
{
    printf("on_init\n");
    config.attr.num_pages = NUM_PAGES;
    config.attr.pagesize = PAGESIZE;
    config.attr.watermark = WATERMARK;

    bool tcp = netio_tcp_mode(config.hostname.c_str());
    const char* host = netio_hostname(config.hostname.c_str());
    if (tcp) {
        netio_buffered_listen_tcp_socket_init(&config.socket, &config.ctx, &config.attr);
        netio_buffered_listen_tcp(&config.socket, host, config.port);
    } else {
        netio_buffered_listen_socket_init(&config.socket, &config.ctx, &config.attr);
        netio_buffered_listen(&config.socket, host, config.port);
    }

    config.socket.cb_connection_established = on_connection_established;
    config.socket.cb_connection_closed = on_connection_closed;
    config.socket.cb_msg_received = on_msg_received;
    clock_gettime(CLOCK_MONOTONIC_RAW, &statistics.t0);
    statistics.timer.cb = on_stats;
    // netio_timer_start_ms(&statistics.timer, 1000);

    // FelixBus
    struct felix_bus_info info;
    info.ip = host;
    info.raw_tcp = tcp;
    info.port = config.port;
    info.unbuffered = false;
    info.pubsub = false;
    info.netio_pages = config.attr.num_pages;
    info.netio_pagesize = config.attr.pagesize;
    info.stream = false;

    printf("Using bus to publish %s:%u\n", info.ip, info.port);
    felix_bus bus = nullptr;

    int rc;
    for(std::vector<netio_tag_t>::iterator it = config.tags.begin(); it != config.tags.end(); ++it) {
        netio_tag_t tag = *it;

        if (!bus) {
            uint8_t vid = get_vid(tag);
            uint8_t did = get_did(tag);
            uint32_t cid = get_cid(tag);
            printf("vid %x, did %x, cid %x\n", vid, did, cid);

            char* bus_path = felix_bus_path(config.bus_directory.c_str(), config.bus_group_name.c_str(), vid, did, cid, "dma-0");
            if (bus_path == NULL) {
                printf("felix_bus_path: Cannot create bus_path\n");
                break;
            }
            printf("%s\n", bus_path);

            bus = felix_bus_open(bus_path);
            if (!bus) {
                printf("felix_bus_open: errno=%d str=%s\n", errno, strerror(errno));
                break;
            }
        }

        printf("Publish 0x%lx\n", tag);
        rc = felix_bus_write(bus, tag, &info);
        if (rc < 0) {
            printf("felix_bus_write: errno=%d str=%s\n", errno, strerror(errno));
            break;
        }
    }

    rc = felix_bus_close(bus);
    if (rc < 0) {
        printf("felix_bus_close: errno=%d str=%s\n", errno, strerror(errno));
    }

}

void on_connection_established(struct netio_buffered_recv_socket* socket)
{
    printf("on_connection_established\n");
}

void on_connection_closed(struct netio_buffered_recv_socket* socket) {
    printf("on_connection_closed\n");
}

void on_msg_received(struct netio_buffered_recv_socket* socket, void* data, size_t len)
{
    printf("on_msg_received %zu\n", len);
    struct felix_toflx_header* hdr = NULL;

    size_t pos = 0;
    hdr = (struct felix_toflx_header*)((uint8_t*)data);
    pos += sizeof(struct felix_toflx_header);

    printf("Header length: %u, tag: 0x%lx\n", hdr->length, hdr->elink);

    std::string s((char*)data + pos, hdr->length);
    printf("data: %s\n", s.c_str());

    statistics.messages_received++;
    statistics.bytes_received += len;
}

void on_stats(void* ptr)
{
    struct timespec t1;
    clock_gettime(CLOCK_MONOTONIC_RAW, &t1);
    double seconds = t1.tv_sec - statistics.t0.tv_sec
                    + 1e-9*(t1.tv_nsec - statistics.t0.tv_nsec);
    printf("data rate: %2f Gb/s   message rate: %2f kHz\n",
            statistics.bytes_received*8/1024./1024./1024./seconds,
            statistics.messages_received/1000./seconds);
    statistics.bytes_received = 0;
    statistics.messages_received = 0;
    statistics.t0 = t1;
}

int main(int argc, char** argv)
{
    std::map<std::string, docopt::value> args
        = docopt::docopt(USAGE,
                         { argv + 1, argv + argc },
                         true,               // show help if requested
                         (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string

    try {
        config.hostname = args["<hostname>"].asString();
        config.port = args["<port>"].asLong();
        std::vector<std::string> tag_list = args["<tags>"].asStringList();
        for(std::vector<std::string>::iterator it = tag_list.begin(); it != tag_list.end(); ++it) {
            netio_tag_t tag = std::stoul(*it, 0, 0);
            config.tags.push_back(tag);
        }
        config.bus_directory = args["--bus-dir"].asString();
        config.bus_group_name = args["--bus-group-name"].asString();
        config.verbose_bus = args["--verbose-bus"].asBool();
    } catch (std::invalid_argument const& error) {
        std::cerr << "Argument or option of wrong type" << std::endl;
        std::cout << std::endl;
        std::cout << USAGE << std::endl;
        return -1;
    }

    printf("netio-bus-buffered-recv\n");

    netio_init(&config.ctx);
    config.ctx.evloop.cb_init = on_init;
    netio_timer_init(&config.ctx.evloop, &statistics.timer);
    netio_run(&config.ctx.evloop);

    return 0;
}
