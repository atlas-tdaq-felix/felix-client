#include <cstdio>
#include <cmath>
#include <iostream>
#include <string>
#include <unistd.h>
#include <map>

#include "docopt/docopt.h"

#include "felix/felix_client.hpp"
#include "felix/felix_client_properties.h"
#include "felix/felix_client_status.h"
#include "felix/felix_client_util.hpp"
#include "felix/felix_client_thread.hpp"
#include "felix/felix_client_exception.hpp"

#include "hdlc_coder/hdlc.hpp"

#include "felixtag.h"
#include "felix/felix_fid.h"


//Constants from ScaSoftware HldcBackendCommon.h
const uint8_t NETIO_FRAME_HDLC_START_OFFSET = 8;  // HDLC frame shall be placed starting from this index
const uint8_t HDLC_FRAME_PAYLOAD_OFFSET = 2;      // HDLC payload offset in HDLC frame
const uint8_t HDLC_FRAME_TRAILER_SIZE = 2;        // HDLC trailer size in HDLC frame

const uint8_t CHIP_ID_REQ[8] = {0x0, 0x14, 0x04, 0xD1, 0x0, 0x0, 0x1, 0x0}; //Message to request the chip Id
const uint8_t SCA_MSG_SIZE = 8;


static const char USAGE[] =
R"(felix-client-thread-sca-send - Exchanges messages with a GBT-SCA.

    Usage:
      felix-client-thread-sca-send [options] <local_ip_or_interface> <cid> <did> <iterations> <elinks>...

    Options:
      -h --help                         Show this screen.
      --version                         Show version.
      --log-level=<loglevel>            Specify level of logging (trace, debug, info, warning, error, fatal) [default: info]
      --verbose-bus                     Show bus information
      --bus-dir=<directory>             Set directory for bus to use [default: ./bus]
      --bus-group-name=<group-name>     Set group-name for bus to use [default: FELIX]
      --timeout=<timeoutms>             Set timeout to subscribe (0 is no timeout) [default: 0]
      --netio-pages=<pages>             Number of pages for buffer [default: 256]
      --netio-pagesize=<pagesize>       Pagesize for buffer in bytes [default: 65536]
)";

class Loopback {

public:
    explicit Loopback(std::map<std::string, docopt::value> &args) {
        try {
            FelixClientThread::Config config;
            unsigned log_level = get_log_level(args["--log-level"].asString());
            config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] = args["<local_ip_or_interface>"].asString();
            config.property[FELIX_CLIENT_LOG_LEVEL] = args["--log-level"].asString();
            config.property[FELIX_CLIENT_BUS_DIR] = args["--bus-dir"].asString();
            config.property[FELIX_CLIENT_BUS_GROUP_NAME] = args["--bus-group-name"].asString();
            config.property[FELIX_CLIENT_VERBOSE_BUS] = args["--verbose-bus"].asBool() ? "True" : "False";
            config.property[FELIX_CLIENT_TIMEOUT] = args["--timeout"].asString();
            config.property[FELIX_CLIENT_NETIO_PAGES] = args["--netio-pages"].asString();
            config.property[FELIX_CLIENT_NETIO_PAGESIZE] = args["--netio-pagesize"].asString();

            cid = args["<cid>"].asLong();
            did = args["<did>"].asLong();
            reps = args["<iterations>"].asLong();
            std::cout<<"reps:"<<reps<<std::endl;
            std::vector<std::string> elink_list = args["<elinks>"].asStringList();
            verbosity = log_level;
            std::vector<std::uint32_t> elinks;
            // for (std::string str : elink_list){
            //     elinks.push_back(static_cast<uint32_t>(std::stoul(str)));
            // }
            std::transform(std::begin(elink_list),
               std::end(elink_list),
               std::back_inserter(elinks),
               [](std::string s) { return static_cast<uint32_t>(std::stoul(s, nullptr, 0)); }
            );
            num_elinks = elinks.size();

            config.on_init_callback = std::bind(&Loopback::on_init, this);
            config.on_data_callback = std::bind(&Loopback::on_data, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
            config.on_connect_callback = std::bind(&Loopback::on_connect, this, std::placeholders::_1);
            config.on_disconnect_callback = std::bind(&Loopback::on_disconnect, this, std::placeholders::_1);

            client = std::make_unique<FelixClientThread>(config);
            std::vector<uint64_t> send_tags;

            reset_msg = (uint8_t*)calloc( reset_msg_size, sizeof(uint8_t));
            scaid_msg = (uint8_t*)calloc( scaid_msg_size, sizeof(uint8_t));

            felix::hdlc::encode_hdlc_ctrl_header(reset_msg, 0, 143); //0x8f
            felix::hdlc::encode_hdlc_trailer(
                reset_msg + HDLC_FRAME_PAYLOAD_OFFSET, // where to put the trailer
                reset_msg,
                HDLC_FRAME_PAYLOAD_OFFSET);

            for (auto &elink : elinks){
                uint64_t subscribe_tag;
                uint64_t send_tag;
                uint8_t stream_id = 0;
                uint8_t vid = 1;

                printf("Creating fid with did 0x%x, cid 0x%x, elink 0x%x, streamId 0x%x, vid 0x%x \n", did, cid, elink, stream_id, vid);
                subscribe_tag = get_fid_from_ids(did, cid, elink, stream_id, vid, 0, 0);
                send_tag = get_fid_from_ids(did, cid, elink, stream_id, vid, 1, 0);
                send_tags.push_back(send_tag);

                printf("Subscribe tag 0x%lx\n", subscribe_tag);
                printf("send tag 0x%lx\n", send_tag);



                counters perf;
                subscribe_vec.push_back(subscribe_tag);
                subscription_map[subscribe_tag] = send_tag;
                performance_map[send_tag] = perf;
                connection_map[send_tag] = std::make_pair(false, false);

                client->subscribe(subscribe_tag);
            }

            int i = 1;
            while(not_finished){
                for(auto& it : subscription_map) {
                    try {
                        if(i%500 == 0) {
                            resubscribe(it.first);
                        } else {
                            send_reset(it.second);
                        }
                    } catch(std::exception& e) {
                        std::cout << "\x1b[31m Unable to send, because: " << e.what() << " \x1b[0m" << std::endl;
                    }
                }
                usleep(1);
                i++;
            }
            for(auto& it : subscription_map){
                results(&performance_map[it.second],it.first);
            }
            subscription_map.clear();
            std::cout << "Exiting now " << std::endl;
            //exit(0);

        } catch (std::invalid_argument const& error) {
            std::cerr << "Argument or option of wrong type" << std::endl;
            std::cout << std::endl;
            std::cout << USAGE << std::endl;
            return_code = -1;
        } catch (FelixClientResourceNotAvailableException& error) {
            printf("Failed to send\n");
            return_code = 1;
        }
    }

    ~Loopback() {
    }
    int return_code = 0;

private:
    unsigned verbosity;
    std::unique_ptr<FelixClientThread> client;
    bool not_finished = true;
    uint8_t fids_done = 0;

    //messages
    uint8_t* reset_msg;
    size_t reset_msg_size = (HDLC_FRAME_PAYLOAD_OFFSET + HDLC_FRAME_TRAILER_SIZE);

    uint8_t* scaid_msg;
    size_t scaid_msg_size = (HDLC_FRAME_PAYLOAD_OFFSET + SCA_MSG_SIZE + HDLC_FRAME_TRAILER_SIZE);

    //counters
    long reps = 0;
    unsigned long timeout_ms = 1000;

    struct counters {
        long send_counter = 0;
        long recv_counter = 0;
        struct timespec t0 = { 0, 0 };
        //struct timespec tt = { 0, 0 };
        std::vector<double> times;
    };


    //links
    uint8_t seqnr = 1;
    uint32_t cid;
    uint32_t did;

    std::map<uint64_t, uint64_t> subscription_map;
    std::map<uint64_t, struct counters> performance_map;
    std::map<uint64_t, std::pair <bool, bool>> connection_map;
    std::vector<uint64_t> subscribe_vec;
    uint8_t num_elinks = 0;

    //methods
    void on_init() {
        printf("Initialising connections...\n");
    }

    void on_connect(uint64_t fid) {
        printf("\x1b[32m Connection up for fid 0x%lx \x1b[0m\n", fid);
        uint64_t send_tag;
        auto it = subscription_map.find(fid);
        if(it != subscription_map.end()){
            send_tag = it->second;
            connection_map[send_tag].first = true;
        }
        else{
            for (auto& it2 : subscription_map) {
                if (it2.second == fid){
                    send_tag = fid;
                    connection_map[send_tag].second = true;
                }
            }
        }
    }

    void on_disconnect(uint64_t fid) {
        printf("\x1b[31m Disconnection for fid 0x%lx \x1b[0m \n", fid);
    }

    void send_reset(uint64_t send_tag) {
        auto perf = &performance_map[send_tag];
        // printf("\x1b[33m Sent reset command for 0x%lx: ", send_tag);
        // for(unsigned int b=0; b<reset_msg_size; ++b){printf("%02X ", reset_msg[b]);}
        // printf("\x1b[0m \n");
        clock_gettime(CLOCK_MONOTONIC_RAW, &(perf->t0));
        try{
            client->send_data(send_tag, (const uint8_t*)reset_msg, reset_msg_size, true);
        } catch(std::exception& e){
            std::cout << "\x1b[31m Unable to send, because: " << e.what() << " \x1b[0m" << std::endl;
        }

        ++(perf->send_counter);
    }

    void on_data(uint64_t fid, const uint8_t* data, size_t size, uint8_t status) {
        if(not_finished){
            uint64_t send_tag = subscription_map[fid];
            auto perf = &performance_map[send_tag];
            struct timespec t1;
            clock_gettime(CLOCK_MONOTONIC_RAW, &t1);
            ++(perf->recv_counter);
            double ms = 1.0e-6*(t1.tv_nsec - perf->t0.tv_nsec);
            perf->times.push_back(ms);
            if(verbosity<6 && perf->recv_counter%1000 == 0){
                printf("Message from 0x%lx:", fid);
                uint8_t *msg = (uint8_t*) data;
                for(unsigned int b=0; b<size; ++b){
                    printf("\x1b[32m %02X ", msg[b]);
                }
                printf("status %u received after %f ms\x1b[0m\n", status, ms);
            }
            if(perf->recv_counter==reps){
                fids_done++;
                printf("\x1b[32m FID 0x%lx finished all iterations\x1b[0m\n", fid);
                if(fids_done == num_elinks){
                    not_finished = false;
                }
            }
        }
    }

    void resubscribe(uint64_t fid){
        std::cout << "FID " << std::hex << fid << std::dec << " resubscribes." <<  std::endl;
        client->unsubscribe(fid);
        client->subscribe(fid);
    }

    void results(struct counters* perf, uint64_t fid){
        double avg{0.}, rms{0.}, N{0.};
        std::vector<double> times = perf->times;
        for(unsigned int i=1; i<times.size(); ++i){
            if (times.at(i)>0 && times.at(i) < timeout_ms){
                avg+=times.at(i);
                rms+=times.at(i)*times.at(i);
                N+=1.0;
            }
        }
        avg/=N;
        rms = sqrt(rms)/(N-1);
        std::cout << "\n \x1b[32m  FID: " << std::hex << fid << std::dec << " Averaged time on "<< (int)N << " measurements: " << avg << " +/- " << rms << " ms\x1b[0m\n"<< std::endl;
        this->client->unsubscribe(fid);
        //subscription_map.erase(fid);
    }

};


int main(int argc, char** argv) {
    std::map<std::string, docopt::value> args
        = docopt::docopt(USAGE,
                         { argv + 1, argv + argc },
                         true,               // show help if requested
                         (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string

    for (uint i = 0; i < 100; i++){
        printf("-------------------------------------> starting iteration %d <---------------------------------------------", i);
        Loopback sca(args);
        if(sca.return_code){
            return sca.return_code;
        }
    }

    return 0;
}
