#include <cstdio>
#include <string>
#include <thread>
#include <chrono>
#include <iostream>
#include <unistd.h>
#include <vector>
#include <map>
#include <functional>

#include "docopt/docopt.h"
#include "external/tsl/robin_map.h"

#include "felix/felix_client_properties.h"
#include "felix/felix_client_status.h"
#include "felix/felix_client_exception.hpp"
#include "felix/felix_client_thread.hpp"

#include "felixtag.h"

static const char USAGE[] =
R"(felix-client-thread-mt-subscribe - Subscribes to felix-ids using felix-bus. The fid list is split evenly between two threads.

    Usage:
      felix-client-thread-mt-subscribe [options] <local_ip_or_interface> <first_fid> <last_fid>

    Options:
      -h --help                         Show this screen.
      --threads=<nthreads>              Number of threads [default: 2].
      --version                         Show version.
      --log-level=<loglevel>            Specify level of logging (trace, debug, info, warning, error, fatal) [default: info]
      --verbose-bus                     Show bus information
      --bus-dir=<directory>             Set directory for bus to use [default: ./bus]
      --bus-group-name=<group-name>     Set group-name for bus to use [default: FELIX]
      --timeout=<timeoutms>             Set timeout to subscribe (0 is no timeout) [default: 0]
      --messages=<no_messages>          Exit after no_messages are received [default: 0]
      --netio-pages=<pages>             Number of pages for buffer [default: 256]
      --netio-pagesize=<pagesize>       Pagesize for buffer in bytes [default: 65536]
)";


struct statistics {
    uint64_t total_messages_received;
    uint64_t messages_received;
    uint64_t bytes_received;

    std::vector<uint64_t> elinks;
    std::map<uint64_t, uint64_t> counters;
    std::map<uint64_t, uint64_t> truncated;
    std::map<uint64_t, uint64_t> crc;
    std::map<uint64_t, uint64_t> error;
};

typedef std::function<void(uint64_t fid, const uint8_t * data, uint32_t size, uint8_t status)> DataCallback;

class SharedClient {

public:
    explicit SharedClient(std::map<std::string, docopt::value> &args) {

        try {
            //client configuration
            FelixClientThread::Config config;

            config.on_init_callback = std::bind(&SharedClient::on_init, this);
            config.on_data_callback = std::bind(&SharedClient::dataReceived, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
            config.on_connect_callback = std::bind(&SharedClient::on_connect, this, std::placeholders::_1);
            config.on_disconnect_callback = std::bind(&SharedClient::on_disconnect, this, std::placeholders::_1);

            config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] = args["<local_ip_or_interface>"].asString();
            config.property[FELIX_CLIENT_LOG_LEVEL] = args["--log-level"].asString();
            config.property[FELIX_CLIENT_BUS_DIR] = args["--bus-dir"].asString();
            config.property[FELIX_CLIENT_BUS_GROUP_NAME] = args["--bus-group-name"].asString();
            config.property[FELIX_CLIENT_VERBOSE_BUS] = args["--verbose-bus"].asBool() ? "True" : "False";
            config.property[FELIX_CLIENT_TIMEOUT] = args["--timeout"].asString();
            config.property[FELIX_CLIENT_NETIO_PAGES] = args["--netio-pages"].asString();
            config.property[FELIX_CLIENT_NETIO_PAGESIZE] = args["--netio-pagesize"].asString();

            //client creation, start if the eventloop
            client = new FelixClientThread(config);

        } catch (std::invalid_argument const& error) {
            std::cerr << "Argument or option of wrong type" << std::endl;
            std::cout << std::endl;
            std::cout << USAGE << std::endl;
            return_code = -1;
        }
    }

    ~SharedClient() {
        delete client;
    }

    void subscribe(uint64_t fid, const DataCallback& callback) {
        m_callbacks.insert(std::make_pair(fid, callback));
        client->subscribe(fid);
    }
    int return_code = 0;
private:
    FelixClientThread* client;

    struct Hash {
        size_t operator()(uint64_t x) const noexcept {
            x = ((x >> 17) ^ x) * 0x45d9f3b;
            return (x >> 17) ^ x;
        }
    };

    typedef tsl::robin_map<uint64_t, DataCallback, Hash> Callbacks;

    Callbacks m_callbacks;
    uint64_t m_last_id = -1;
    Callbacks::iterator m_last_it;

    void dataReceived(uint64_t fid, const uint8_t* data, size_t size, uint8_t status) {
        #define UNLIKELY(x) __builtin_expect(x,0)
        if (m_last_id != fid) {
            m_last_it = m_callbacks.find(fid);
            if (UNLIKELY(m_last_it == m_callbacks.end())) {
                std::cout << "No callback was found for 0x" << std::hex << fid << " input link";
                m_last_id = -1;
                return;
            }
            m_last_id = fid;
        }
        m_last_it->second(fid, data, size, status); //!status ? 0 : translateStatus(status)
    }

    void on_init() {
        printf("on_init called\n");
    }

    void on_connect(uint64_t fid) {
        printf("on_connect called 0x%lx\n", fid);
    }

    void on_disconnect(uint64_t fid) {
        printf("on_disconnect called 0x%lx\n", fid);
    }

};


class UserApplication {

public:

    UserApplication(int t, SharedClient* client, std::vector<uint64_t>* fids){
        thread_no = t;
        fclient = client;
        fid_list = fids;
        self = this;
        stats.total_messages_received=0;
        stats.messages_received=0;
        stats.bytes_received=0;
    }

    void subscribe(){
        //for(std::vector<uint64_t>::iterator it = fid_list->begin(); it != fid_list->end(); ++it) {
        for(unsigned int i=0; i<fid_list->size(); ++i) {
            uint64_t fid = fid_list->at(i);
            stats.elinks.push_back(fid);
            stats.counters[fid] = 0;
            stats.truncated[fid] = 0;
            stats.crc[fid] = 0;
            stats.error[fid] = 0;
            std::cout << "Thread " << thread_no << " subscribing to " << fid <<  std::endl;

            // How to set the callbacks:
            // 1. Using std::bind passing a copy of this object
            // fclient->subscribe(fid, std::bind(&UserApplication::on_data_callback,
            //        *this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
            //
            // 2. Using std::bind passing a reference to this object
            fclient->subscribe(fid, std::bind(&UserApplication::on_data_callback,
                    self, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
            //
            // 3. Using a lambda, passing a pointer to this object. Is [=] deprecated in C++ 2020?
            //fclient->subscribe(fid, [=](uint64_t fid, const uint8_t* data, size_t size, uint8_t status){self->on_data_callback(fid, data, size, status);});
        }
    }

    void on_data_callback(uint64_t fid, const uint8_t* data, size_t size, uint8_t status) {
        stats.total_messages_received++;
        stats.messages_received++;
        stats.counters[fid]++;
        stats.bytes_received += size;
        if(!(stats.counters[fid]%10000)){
            auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
            struct tm stm = *localtime(&timenow);
            std::cout << "[" << stm.tm_hour << ":" << stm.tm_min << ":"<< stm.tm_sec << " Thread " << thread_no << "] "
                    << stats.counters[fid] << " messages from fid " << fid << std::endl;
        }
    }

private:
    volatile int thread_no;
    SharedClient* fclient;
    UserApplication* self;
    std::vector<uint64_t>* fid_list;
    struct statistics stats;

};


int main(int argc, char** argv) {
    std::map<std::string, docopt::value> args
            = docopt::docopt(USAGE,
                            { argv + 1, argv + argc },
                            true,               // show help if requested
                            (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string

    uint64_t first_fid = std::stoul(args["<first_fid>"].asString());
    uint64_t last_fid = std::stoul(args["<last_fid>"].asString());
    bool use_fid = (first_fid & 0x1000000000000000) > 0L;

    size_t number_of_fids=0;
    for(uint64_t fid=first_fid; fid<=last_fid; fid += use_fid ? 0x10000 : 1) {
        number_of_fids++;
    }

    size_t nthreads = args["--threads"].asLong();
    if(number_of_fids<nthreads || nthreads==0){std::cout << "ERROR: the number of threads is larger than the number of fids" << std::endl; return 1;}

    const size_t fids_per_thread = number_of_fids/nthreads;
    std::vector< std::vector<uint64_t>> fid_lists(nthreads);

    size_t f_counter=0; size_t t_counter=0;
    for(uint64_t fid=first_fid; fid<=last_fid; fid += use_fid ? 0x10000 : 1) {
        if(f_counter<fids_per_thread){
            //std::cout << "A adding " << fid << " to thread " << t_counter+1 << std::endl;
            fid_lists.at(t_counter).push_back(fid);
            f_counter++;
        }
        else{
            f_counter=0;
            t_counter++;
            if(t_counter>=nthreads){t_counter=0;}
            fid_lists.at(t_counter).push_back(fid);
            f_counter++;
            //std::cout << "B adding " << fid << " to thread " << t_counter+1 << std::endl;
        }
    }

    for(unsigned int i=0; i<fid_lists.size(); ++i){
        std::cout << "INFO: thread " << i+1 << " has fid(s) ";
        for(unsigned int j=0; j<fid_lists.at(i).size(); ++j){
            std::cout <<  fid_lists.at(i).at(j) << " ";
        }
        std::cout << std::endl;
    }

    SharedClient fclient =  SharedClient(args);
    if(fclient.return_code){
        return fclient.return_code;
    }

    std::vector<UserApplication*> apps;
    std::vector<std::thread> threads;
    apps.reserve(nthreads);
    threads.reserve(nthreads);

    for(unsigned int t=0; t<nthreads; ++t){
        apps.emplace_back(new UserApplication(t+1, &fclient, &fid_lists.at(t)));
        threads.emplace_back(std::thread(&UserApplication::subscribe, apps.at(t)));
    }

    sleep(60);
    return fclient.return_code;
}
