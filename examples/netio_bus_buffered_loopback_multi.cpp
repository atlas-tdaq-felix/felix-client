#include <cstdio>
#include <ctime>
#include <string>
#include <ostream>
#include <iostream>

#include "docopt/docopt.h"

#include "felixbus/felixbus.h"

#include "felix/felix_fid.h"
#include "felix/felix_toflx.h"

#include "netio/netio.h"
#include "netio/netio_tcp.h"

#include "felixtag.h"

#define NUM_PAGES (256)
#define WATERMARK (56*1024)
#define PAGESIZE (64*1024)
static const char USAGE[] =
R"(netio-bus-buffered-loopback-multi - Receive some (buffered) data and publish answer.

    Usage:
      netio-bus-buffered-loopback-multi [options] <hostname> <recv-port> <pub-port> <cid> <did> <elink> <no-of-elinks>

    Options:
      -h --help                         Show this screen.
      --version                         Show version.
      --log-level=<loglevel>            Specify level of logging (trace, debug, info, warning, error, fatal) [default: info]
      --verbose-bus                     Show bus information
      --bus-dir=<bus-directory>         Set bus directory [default: bus]
      --bus-group-name=<group-name>     Set group-name for bus to use [default: FELIX]
)";

struct {
    std::string log_level;

    std::string hostname;
    unsigned recv_port;
    unsigned pub_port;
    int64_t cid;
    int64_t did;
    netio_tag_t elink;
    size_t no_of_elinks;

    std::string bus_directory;
    std::string bus_group_name;
    bool verbose_bus;

    struct netio_context ctx;

    struct netio_buffered_listen_socket recv_socket;
    struct netio_buffered_socket_attr recv_attr;

    struct netio_publish_socket pub_socket;
    struct netio_buffered_socket_attr pub_attr;

    uint8_t* data;
    size_t datasize;
} config;

struct {
    struct netio_timer timer;
    struct timespec t0;
    uint64_t messages_received;
    uint64_t bytes_received;
} statistics;

// Forward declarations
void recv_on_connection_established(struct netio_buffered_recv_socket*);
void recv_on_connection_closed(struct netio_buffered_recv_socket*);
void on_msg_received(struct netio_buffered_recv_socket*, void*, size_t);
void pub_on_subscribe(struct netio_publish_socket*, netio_tag_t, void*, size_t);
void pub_on_connection_established(struct netio_publish_socket*);
void pub_on_connection_closed(struct netio_publish_socket*);
void on_stats(void* ptr);

// Callbacks
void on_init(void *ptr)
{
    setbuf(stdout, NULL);

    printf("on_init\n");
    config.recv_attr.num_pages = NUM_PAGES;
    config.recv_attr.pagesize = PAGESIZE;
    config.recv_attr.watermark = WATERMARK;

    bool tcp = netio_tcp_mode(config.hostname.c_str());
    const char* host = netio_hostname(config.hostname.c_str());
    if (tcp) {
        netio_buffered_listen_tcp_socket_init(&config.recv_socket, &config.ctx, &config.recv_attr);
        netio_buffered_listen_tcp(&config.recv_socket, host, config.recv_port);
    } else {
        netio_buffered_listen_socket_init(&config.recv_socket, &config.ctx, &config.recv_attr);
        netio_buffered_listen(&config.recv_socket, host, config.recv_port);
    }
    config.recv_socket.cb_connection_established = recv_on_connection_established;
    config.recv_socket.cb_connection_closed = recv_on_connection_closed;
    config.recv_socket.cb_msg_received = on_msg_received;
    clock_gettime(CLOCK_MONOTONIC_RAW, &statistics.t0);
    statistics.timer.cb = on_stats;
    // netio_timer_start_ms(&statistics.timer, 1000);

    // FelixBus
    struct felix_bus_info info;
    info.ip = host;
    info.raw_tcp = tcp;
    info.port = config.recv_port;
    info.unbuffered = false;
    info.pubsub = false;
    info.netio_pages = config.recv_attr.num_pages;
    info.netio_pagesize = config.recv_attr.pagesize;
    info.stream = false;

    printf("Using bus to receive %s:%u\n", info.ip, info.port);

    uint8_t vid = 1;
    char* bus_path = felix_bus_path(config.bus_directory.c_str(), config.bus_group_name.c_str(), vid, config.did, config.cid, "dma-4");
    if (bus_path == NULL) {
        printf("felix_bus_path: Cannot create bus_path\n");
    }
    felix_bus bus = felix_bus_open(bus_path);
    if (!bus) {
        printf("felix_bus_open: errno=%d str=%s\n", errno, strerror(errno));
    }

    int rc;
    for (size_t i = 0; i<config.no_of_elinks; i++) {
        uint8_t stream_id = 0;
        uint64_t recv_tag = get_fid_from_ids(config.did, config.cid, config.elink + i, stream_id, vid, 1, 0);

        printf("Receive 0x%lx\n", recv_tag);
        rc = felix_bus_write(bus, recv_tag, &info);
        if (rc < 0) {
            printf("felix_bus_write: errno=%d str=%s\n", errno, strerror(errno));
        }
    }

    rc = felix_bus_close(bus);
    if (rc < 0) {
        printf("felix_bus_close: errno=%d str=%s\n", errno, strerror(errno));
    }

    config.pub_attr.num_pages = NUM_PAGES;
    config.pub_attr.pagesize = PAGESIZE;
    config.pub_attr.watermark = WATERMARK;

    printf("Opening publish socket on %s:%u\n", config.hostname.c_str(), config.pub_port);
    netio_publish_socket_init(&config.pub_socket, &config.ctx, config.hostname.c_str(), config.pub_port, &config.pub_attr);
    config.pub_socket.cb_subscribe = pub_on_subscribe;
    config.pub_socket.cb_connection_established = pub_on_connection_established;
    config.pub_socket.cb_connection_closed = pub_on_connection_closed;

    // FelixBus
    struct felix_bus_info pub_info;
    pub_info.ip = host;
    pub_info.raw_tcp = tcp;
    pub_info.port = config.pub_port;
    pub_info.unbuffered = false;
    pub_info.pubsub = true;
    pub_info.netio_pages = config.pub_attr.num_pages;
    pub_info.netio_pagesize = config.pub_attr.pagesize;

    printf("Using bus to publish %s:%u\n", pub_info.ip, pub_info.port);

    char* pub_bus_path = felix_bus_path(config.bus_directory.c_str(), config.bus_group_name.c_str(), vid, config.did, config.cid, "0");
    if (pub_bus_path == NULL) {
        printf("felix_bus_path: Cannot create pub_bus_path\n");
    }
    felix_bus pub_bus = felix_bus_open(pub_bus_path);
    if (pub_bus == NULL) {
        printf("felix_bus_open: errno=%d str=%s\n", errno, strerror(errno));
    }

    int pub_rc;
    for (size_t i = 0; i<config.no_of_elinks; i++) {
        uint8_t stream_id = 0;
        uint64_t pub_tag = get_fid_from_ids(config.did, config.cid, config.elink + i, stream_id, vid, 0, 0);

        printf("Publish 0x%lx\n", pub_tag);
        pub_rc = felix_bus_write(pub_bus, pub_tag, &pub_info);
        if (pub_rc < 0) {
            printf("felix_bus_write: errno=%d str=%s\n", errno, strerror(errno));
        }
    }

    pub_rc = felix_bus_close(pub_bus);
    if (pub_rc < 0) {
        printf("felix_bus_close: errno=%d str=%s\n", errno, strerror(errno));
    }
}

void recv_on_connection_established(struct netio_buffered_recv_socket* socket)
{
    printf("recv_on_connection_established\n");
}

void recv_on_connection_closed(struct netio_buffered_recv_socket* socket) {
    printf("recv_on_connection_closed\n");
}

void on_msg_received(struct netio_buffered_recv_socket* socket, void* data, size_t len)
{
    printf("on_msg_received %zu\n", len);
    struct felix_toflx_header* hdr = NULL;

    size_t pos = 0;
    hdr = (struct felix_toflx_header*)((uint8_t*)data);
    pos += sizeof(struct felix_toflx_header);

    printf("Header length: %u, recv-tag: 0x%lx\n", hdr->length, hdr->elink);

    char* payload = (char*)data + pos;
    printf("data: '%s'\n", payload);

    statistics.messages_received++;
    statistics.bytes_received += len;

    // FIXME check hdr.elink against config.outtag

    // prepending status byte to payload
    uint8_t send_datasize = hdr->length + 1;
    char* send_data = (char*)malloc(send_datasize);
    send_data[0] = 0x00; // Fake status byte
    for(unsigned i=1; i<send_datasize; i++) {
        send_data[i] = payload[i-1];
    }

    // create corresponding pub_tag
    uint8_t vid = 1;
    uint8_t stream_id = 0;
    uint64_t pub_tag = get_fid_from_ids(config.did, config.cid, hdr->elink, stream_id, vid, 0, 0);

    // supply an answer
    netio_buffered_publish(&config.pub_socket, pub_tag, send_data, send_datasize, 0, NULL);
    netio_buffered_publish_flush(&config.pub_socket, 0, NULL);
}

// publish
void pub_on_subscribe(struct netio_publish_socket* socket, netio_tag_t pub_tag, void* addr, size_t addrlen) {
    printf("pub remote subscribed to pub-tag 0x%lx\n", pub_tag);
}

void pub_on_connection_established(struct netio_publish_socket* socket) {
    printf("pub connection to subscriber established\n");
}

void pub_on_connection_closed(struct netio_publish_socket* socket) {
    printf("pub connection to subscriber closed\n");
}


void on_stats(void* ptr)
{
    struct timespec t1;
    clock_gettime(CLOCK_MONOTONIC_RAW, &t1);
    double seconds = t1.tv_sec - statistics.t0.tv_sec
                    + 1e-9*(t1.tv_nsec - statistics.t0.tv_nsec);
    printf("data rate: %2f Gb/s   message rate: %2f kHz\n",
            statistics.bytes_received*8/1024./1024./1024./seconds,
            statistics.messages_received/1000./seconds);
    statistics.bytes_received = 0;
    statistics.messages_received = 0;
    statistics.t0 = t1;
}

int main(int argc, char** argv)
{
    std::map<std::string, docopt::value> args
        = docopt::docopt(USAGE,
                         { argv + 1, argv + argc },
                         true,               // show help if requested
                         (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string

    try {
        config.hostname = args["<hostname>"].asString();
        config.recv_port = args["<recv-port>"].asLong();
        config.pub_port = args["<pub-port>"].asLong();
        config.cid = args["<cid>"].asLong();
        config.did = args["<did>"].asLong();
        config.elink = args["<elink>"].asLong();
        config.no_of_elinks = args["<no-of-elinks>"].asLong();
        config.log_level = args["--log-level"].asString();
        config.bus_directory = args["--bus-dir"].asString();
        config.bus_group_name = args["--bus-group-name"].asString();
        config.verbose_bus = args["--verbose-bus"].asBool();
    } catch (std::invalid_argument const& error) {
        std::cerr << "Argument or option of wrong type" << std::endl;
        std::cout << std::endl;
        std::cout << USAGE << std::endl;
        return -1;
    }

    printf("netio-bus-buffered-loopback\n");

    netio_init(&config.ctx);
    config.ctx.evloop.cb_init = on_init;
    netio_timer_init(&config.ctx.evloop, &statistics.timer);
    netio_run(&config.ctx.evloop);

    return 0;
}
