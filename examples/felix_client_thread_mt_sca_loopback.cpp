#include <cstdio>
#include <cmath>
#include <string>
#include <unistd.h>
#include <map>
#include <iomanip>
#include <iostream>
#include "docopt/docopt.h"
#include <mutex>
#include <queue>
#include <variant>

//#include "external/tsl/robin_map.h"

#include "felix/felix_client.hpp"
#include "felix/felix_client_properties.h"
#include "felix/felix_client_status.h"
#include "felix/felix_client_util.hpp"
#include "felix/felix_client_thread.hpp"
#include "felix/felix_client_exception.hpp"

#include "hdlc_coder/hdlc.hpp"

#include "felixtag.h"
#include "felix/felix_fid.h"


//Constants from ScaSoftware HldcBackendCommon.h
const uint8_t NETIO_FRAME_HDLC_START_OFFSET = 8;  // HDLC frame shall be placed starting from this index
const uint8_t HDLC_FRAME_PAYLOAD_OFFSET = 2;      // HDLC payload offset in HDLC frame
const uint8_t HDLC_FRAME_TRAILER_SIZE = 2;        // HDLC trailer size in HDLC frame

const uint8_t CHIP_ID_REQ[8] = {0x0, 0x14, 0x04, 0xD1, 0x0, 0x0, 0x1, 0x0}; //Message to request the chip Id
const uint8_t SCA_MSG_SIZE = 8;
//std::mutex mtx;
bool DONE = false;
std::atomic<uint64_t> fid_done = 0;

struct statistics {
    uint64_t timeout_ms = 1000;
    uint64_t send_counter = 0;
    uint64_t recv_counter = 0;
    struct timespec t0 = { 0, 0 };
    std::vector<double> times;
    std::pair <bool, bool> connection_status = std::make_pair(false, false);
};


static const char USAGE[] =
R"(felix-client-thread-mt-sca-loopback - Exchanges messages with a GBT-SCA.

    Usage:
      felix-client-thread-mt-sca-loopback [options] <local_ip_or_interface> <cid> <did> <iterations> <elinks>...

    Options:
      -h --help                         Show this screen.
      --threads=<nthreads>              Number of threads [default: 1].
      --version                         Show version.
      --log-level=<loglevel>            Specify level of logging (trace, debug, info, warning, error, fatal) [default: info]
      --verbose-bus                     Show bus information
      --bus-dir=<directory>             Set directory for bus to use [default: ./bus]
      --bus-group-name=<group-name>     Set group-name for bus to use [default: FELIX]
      --timeout=<timeoutms>             Set timeout to subscribe (0 is no timeout) [default: 0]
      --netio-pages=<pages>             Number of pages for buffer [default: 0]
      --netio-pagesize=<pagesize>       Pagesize for buffer in bytes [default: 0]
)";

typedef std::function<void(uint64_t fid, const uint8_t * data, uint32_t size, uint8_t status)> DataCallback;
typedef std::function<void(uint64_t fid)> ConnectionCallback;

class SharedClient {
public:
    explicit SharedClient(std::map<std::string, docopt::value> &args){
        try{
            FelixClientThread::Config config;
            config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] = args["<local_ip_or_interface>"].asString();
            config.property[FELIX_CLIENT_BUS_DIR] = args["--bus-dir"].asString();
            config.property[FELIX_CLIENT_LOG_LEVEL] = args["--log-level"].asString();
            config.property[FELIX_CLIENT_BUS_GROUP_NAME] = args["--bus-group-name"].asString();
            config.property[FELIX_CLIENT_VERBOSE_BUS] = args["--verbose-bus"].asBool() ? "True" : "False";
            config.property[FELIX_CLIENT_TIMEOUT] = args["--timeout"].asString();
            config.property[FELIX_CLIENT_NETIO_PAGES] = args["--netio-pages"].asString();
            config.property[FELIX_CLIENT_NETIO_PAGESIZE] = args["--netio-pagesize"].asString();

            config.on_init_callback = std::bind(&SharedClient::on_init, this);
            config.on_data_callback = std::bind(&SharedClient::on_data_received, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
            config.on_connect_callback = std::bind(&SharedClient::on_connect, this, std::placeholders::_1);
            config.on_disconnect_callback = std::bind(&SharedClient::on_disconnect, this, std::placeholders::_1);

            client = std::make_unique<FelixClientThread>(config);
        } catch (std::invalid_argument const& error) {
            std::cerr << "Argument or option of wrong type" << std::endl;
            std::cout << std::endl;
            std::cout << USAGE << std::endl;
            return_code = -1;
        } catch (FelixClientResourceNotAvailableException& error) {
            std::cerr << "Failed to send." << std::endl;
            std::cout << std::endl;
            return_code = 1;
        }
    }

    ~SharedClient() {
    }

    void subscribe(uint64_t fid, const ConnectionCallback& con_callback, const DataCallback& data_callback) {
        {
            std::lock_guard<std::mutex> lock(mtx);
            m_conenction_callbacks.insert(std::make_pair(fid, con_callback));
            m_data_callbacks.insert(std::make_pair(fid, data_callback));
        }
        client->subscribe(fid);
    }

    void resubscribe(uint64_t fid) {
        //std::thread::id threadID = std::this_thread::get_id();
        //std::cout << "Thread " <<std::hex<<threadID << " called resubscribe in fclient." <<  std::endl;
        client->subscribe(fid);
    }


    void unsubscribe(uint64_t fid){
        client->unsubscribe(fid);
    }

    void init_send_data(uint64_t fid, const ConnectionCallback& con_callback) {
        {
            std::lock_guard<std::mutex> lock(mtx);
            m_conenction_callbacks.insert(std::make_pair(fid, con_callback));
        }
        client->init_send_data(fid);
    }

    FelixClientThread* getClient(){
        return client.get();
    }
    int return_code = 0;

private:
    std::unique_ptr<FelixClientThread> client;
    std::mutex mtx;

    //typedef tsl::robin_map<uint64_t, DataCallback, Hash> DataCallbacks;
    //typedef tsl::robin_map<uint64_t, ConnectionCallback, Hash> ConnectionCallbacks;

    typedef std::map<uint64_t, DataCallback> DataCallbacks;
    typedef std::map<uint64_t, ConnectionCallback> ConnectionCallbacks;

    DataCallbacks m_data_callbacks;
    uint64_t m_data_last_id = -1;
    DataCallbacks::iterator m_data_last_it;

    ConnectionCallbacks m_conenction_callbacks;
    uint64_t m_conenction_last_id = -1;
    ConnectionCallbacks::iterator m_connection_callbacks_it;


    void on_init() {
        printf("Inititalising connections...\n");
    }

    void on_connect(uint64_t fid) {
        //printf("Connection callback for fid 0x%lx\n", fid);
        #define UNLIKELY(x) __builtin_expect(x,0)
        if (m_conenction_last_id != fid) {
            m_connection_callbacks_it = m_conenction_callbacks.find(fid);
            if (UNLIKELY(m_connection_callbacks_it == m_conenction_callbacks.end())) {
                std::cout << "No connection callback was found for 0x" << std::hex << fid << " input link";
                m_conenction_last_id = -1;
                return;
            }
            m_conenction_last_id = fid;
        }
        m_connection_callbacks_it->second(fid); //!status ? 0 : translateStatus(status)

    }



    void on_disconnect(uint64_t fid) {
        printf("Dicsconnection for fid 0x%lx\n", fid);
    }

    void on_data_received(uint64_t fid, const uint8_t* data, size_t size, uint8_t status) {
        //printf("On data received callback. Looking for matching callback for fid 0x%lx\n", fid);
        #define UNLIKELY(x) __builtin_expect(x,0)
        if (m_data_last_id != fid) {
            m_data_last_it = m_data_callbacks.find(fid);
            if (UNLIKELY(m_data_last_it == m_data_callbacks.end())) {
                std::cout << "No data callback was found for 0x" << std::hex << fid << " input link";
                m_data_last_id = -1;
                return;
            }
            m_data_last_id = fid;
        }
        m_data_last_it->second(fid, data, size, status); //!status ? 0 : translateStatus(status)
    }

};


struct OnConnectEvent { std::int64_t fid; };
struct OnDataEvent { uint64_t fid; const uint8_t* data; size_t size; uint8_t status; };

// template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
// template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

template<class T>
class ThreadQueue {
    std::queue<T> q;
    mutable std::mutex m;

public:
    ThreadQueue() {}

    void pushTask(T event) {
        std::lock_guard<std::mutex> lock(m);
        q.push(event);
    }

    void pullTask(T& event) {
        std::lock_guard<std::mutex> lock(m);
        if (!q.empty()) {
            event = q.front();
            q.pop();
        }
    }

};



class Loopback {
public:
    Loopback(uint8_t t, std::shared_ptr<SharedClient> client, std::map<uint64_t, uint64_t>* fids, uint64_t r, std::int8_t v, uint8_t tt){
        reset_msg = (uint8_t*)calloc( reset_msg_size, sizeof(uint8_t));
        scaid_msg = (uint8_t*)calloc( scaid_msg_size, sizeof(uint8_t));
        verbosity = v;
        num_thread = t;
        fclient = client;
        fid_map = fids;
        reps = r;
        total_threads = tt;
        num_elinks = fid_map->size();
        queue = std::make_unique<ThreadQueue<std::variant<std::monostate, OnConnectEvent, OnDataEvent>>>();

        felix::hdlc::encode_hdlc_ctrl_header(reset_msg, 0, 143); //0x8f
        felix::hdlc::encode_hdlc_trailer(
            reset_msg + HDLC_FRAME_PAYLOAD_OFFSET, // where to put the trailer
            reset_msg,
            HDLC_FRAME_PAYLOAD_OFFSET);
    }
    ~Loopback() {

    }

    void loop() {
        using var_t = std::variant<std::monostate, OnConnectEvent, OnDataEvent>;
        var_t event;
        queue->pullTask(event);
        if (std::holds_alternative<std::monostate>(event)) {
            return;
        }

        std::visit(overloaded {
            [](std::monostate const&) {},
            [this](OnConnectEvent const& onConnect) {
                //std::cout << "On connect: " << onConnect.fid << std::endl;
                this->on_connect(onConnect.fid);
            },
            [this](OnDataEvent const& onData) {
                //std::cout << "On data: " << onData.fid << std::endl;
                this->on_data(onData.fid, onData.data, onData.size, onData.status);
            }
        }, event);
    }

    void subscribe(){

        for (auto& fid : *fid_map){
            statistics s;
            stat_map[fid.second] = s;
            std::thread::id threadID = std::this_thread::get_id();
            std::cout << "Thread " <<std::hex<<threadID << " subscribing to " << fid.first <<  std::endl;

            fclient->subscribe(fid.first, [this](std::int64_t fid) {
                this->queue->pushTask(OnConnectEvent{ fid });
            }, [this](uint64_t fid, const uint8_t* data, size_t size, uint8_t status ) {
                this->queue->pushTask(OnDataEvent{fid, data, size, status});
            });
            //fclient->init_send_data(fid.second, std::bind(&Loopback::on_connect,
            //    this, std::placeholders::_1));

        }
        while(fid_done.load() < (total_threads)){
            loop();
            usleep(100);
        }
    }

    //FIX to keep conenction open
    void subscribe_stable(){
        printf("Stable thread subscribing.\n");
        for (auto& fid : *fid_map){
            fclient->subscribe(fid.first, std::bind(&Loopback::on_connect_stable,
                    this, std::placeholders::_1), std::bind(&Loopback::on_data,
                    this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));

        }
        while(!DONE){
            usleep(1000);
        }
        for (auto& fid : *fid_map){
            fclient->unsubscribe(fid.first);
        }
        printf("Leaving...\n");
    }

    void on_connect_stable(uint64_t fid) {
        printf("Connection up for fid 0x%lx, this should stay permanently\n", fid);
    }

    //callbacks

    void on_connect(uint64_t fid) {
        printf("Connection up for fid 0x%lx\n", fid);
        std::pair <bool, bool>* status = nullptr;
        uint64_t send_tag = 0;
        auto it = fid_map->find(fid);
        if(it != fid_map->end()){
            send_tag = it->second;
            status = &stat_map[fid].connection_status;
            status->first = true;
        } else {
             for (auto& it2 : *fid_map) {
                 if (it2.second == fid){
                     send_tag = fid;
                     status = &stat_map[it2.first].connection_status;
                     status->second = true;
                 }
            }
        //     if(status == nullptr){
        //         std::cerr << "Fid is not in subscribtion map" << std::endl;
        //         std::exit(-1);
        //     }
        }
        // if(status->first && status->second){
        //    stat_map[fid].connection_status = std::make_pair(false, false);
        if (send_tag != 0) {
            this->send_reset(send_tag);
        }
        //}
    }

    void on_data(uint64_t fid, const uint8_t* data, size_t size, uint8_t status) {
        struct timespec t1;
        clock_gettime(CLOCK_MONOTONIC_RAW, &t1);
        uint64_t send_tag = (*fid_map)[fid];
        auto stats = &stat_map[send_tag];
        ++stats->recv_counter;
        double ms = 1.0e3*(t1.tv_sec - stats->t0.tv_sec) + 1.0e-6*(t1.tv_nsec - stats->t0.tv_nsec);
        stats->times.push_back(ms);
        if(verbosity<6){
            printf("Message from 0x%lx: ", fid);
            uint8_t *msg = (uint8_t*) data;
            for(unsigned int b=0; b<size; ++b){
                printf("%02X ", msg[b]);
            }
            printf("received msgs %lu received after %f ms\n", stats->recv_counter, ms);
        }
        if(stats->recv_counter==reps){results(stats, fid);}
        //else if(stats->recv_counter%50 == 0){resubscribe(fid);}
        else{this->send_reset(send_tag);}
    }

private:
    unsigned verbosity = 0;
    std::map<uint64_t, uint64_t>* fid_map;
    std::map<uint64_t, struct statistics> stat_map;
    uint64_t num_elinks = 0;
    std::shared_ptr<SharedClient> fclient;
    std::uint8_t num_thread = 0;
    uint64_t reps = 0;
    uint8_t total_threads = 0;
    unsigned long timeout_ms = 1000;
    std::unique_ptr<ThreadQueue<std::variant<std::monostate, OnConnectEvent, OnDataEvent>>> queue;

    //messages
    uint8_t* reset_msg;
    size_t reset_msg_size = (HDLC_FRAME_PAYLOAD_OFFSET + HDLC_FRAME_TRAILER_SIZE);

    uint8_t* scaid_msg;
    size_t scaid_msg_size = (HDLC_FRAME_PAYLOAD_OFFSET + SCA_MSG_SIZE + HDLC_FRAME_TRAILER_SIZE);

    void resubscribe(uint64_t fid){
        std::thread::id threadID = std::this_thread::get_id();
        std::cout << "Thread " << std::hex << threadID << " resubscribes." <<  std::endl;
        fclient->unsubscribe(fid);
        fclient->resubscribe(fid);
        std::cout << "---------->>>>>>>>>>>  resubscribe done. Attempting to send again to FID: " << std::hex << fid << std::dec << " <<<<<<<<--------------" << std::endl;
        usleep(100);
        this->send_reset((*fid_map)[fid]);
    }

    //methods for SCA
    void send_reset(uint64_t fid) {
        auto stats = &stat_map[fid];
        clock_gettime(CLOCK_MONOTONIC_RAW, &(stats->t0));
        try{
            fclient->getClient()->send_data(fid, (const uint8_t*)reset_msg, reset_msg_size, true);
        } catch(std::exception& e){
            std::cout << "Unable to send, because: " << e.what() << std::endl;
        }
        ++stats->send_counter;
        if(verbosity<6){
            printf("FID: 0x%lx: Sent reset command: ", fid);
            for(unsigned int b=0; b<reset_msg_size; ++b){printf("%02X ", reset_msg[b]);}
            printf("\n");
        }
    }



    void results(struct statistics* stats, uint64_t fid){
        double avg{0.}, rms{0.}, N{0.};
        double largest = 0;
        std::vector<double> times = stats->times;
        std::cout<<"size of time vector"<<times.size()<<std::endl;
        for(unsigned int i=1; i<times.size(); ++i){
            if (times.at(i)>0 && times.at(i) < timeout_ms){
                if (times.at(i) > largest){largest = times.at(i);}
                avg+=times.at(i);
                rms+=times.at(i)*times.at(i);
                N+=1.0;
            } else {
                std::cout<<"Time error discovered at"<<times.at(i)<<std::endl;
            }
        }
        avg/=N;
        rms = sqrt(rms)/(N-1);
        ++fid_done;
        std::cout << fid_done.load() << " out of " << std::to_string(total_threads) << " finished.\n" << std::endl;
        while(fid_done.load()<total_threads){
            usleep(1000);
        }
        std::cout << "\nThread: " << std::to_string(num_thread) << ": Averaged time on "<< N << " measurements: " << avg << " +/- " << rms << " ms. With the longest latency of " << largest << " ms.\n"<< std::endl;
        fflush(stdout);

    }
};



int main(int argc, char** argv) {
    std::map<std::string, docopt::value> args
        = docopt::docopt(USAGE,
                         { argv + 1, argv + argc },
                         true,               // show help if requested
                         (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string

    uint64_t cid = args["<cid>"].asLong();
    uint64_t did = args["<did>"].asLong();
    uint64_t reps = args["<iterations>"].asLong();
    unsigned log_level = get_log_level(args["--log-level"].asString());
    std::vector<std::string> elink_list = args["<elinks>"].asStringList();
    uint8_t num_fids = elink_list.size();
    size_t nthreads = args["--threads"].asLong();
    if(num_fids<nthreads || nthreads==0){std::cout << "ERROR: the number of threads is larger than the number of fids" << std::endl; return 1;}
    //THREADS = nthreads;
    uint8_t stream_id = 0;
    uint8_t vid = 1;
    int return_code_main = 0;

    const size_t fids_per_thread = num_fids/nthreads;
    std::vector<std::map<uint64_t, uint64_t>> fid_maps(nthreads+1);

    size_t f_counter=0;
    size_t t_counter=0;
    for (auto &el : elink_list){
        uint32_t elink = static_cast<uint32_t>(std::stoul(el, nullptr, 0));
        uint64_t subscribe_tag = get_fid_from_ids(did, cid, elink, stream_id, vid, 0, 0);
        uint64_t send_tag = get_fid_from_ids(did, cid, elink, stream_id, vid, 1, 0);

        if(f_counter<fids_per_thread){
            fid_maps.at(t_counter).insert(std::pair<uint64_t,uint64_t>(subscribe_tag, send_tag));
            f_counter++;
        }
        else{
            f_counter=0;
            t_counter++;
            if(t_counter>=nthreads){t_counter=0;}
            fid_maps.at(t_counter).insert(std::pair<uint64_t,uint64_t>(subscribe_tag, send_tag));
            f_counter++;
        }
    }
    std::shared_ptr<SharedClient> fclient = std::make_shared<SharedClient>(args);

    std::vector<std::unique_ptr<Loopback>> apps;
    std::vector<std::thread> threads;
    fid_maps.at(nthreads).insert(std::pair<uint64_t,uint64_t>(1157425104259317760, 1157425104259350528));//(1157425104255123456,1157425104255156224));
    //std::thread stable_thread(&Loopback::subscribe_stable, std::make_unique<Loopback>(nthreads, fclient, &fid_maps.at(nthreads), reps,(std::int8_t) log_level, nthreads));
    apps.reserve(nthreads);
    threads.reserve(nthreads);
    for(unsigned int t=0; t<nthreads; ++t){
        apps.emplace_back(std::make_unique<Loopback>(t, fclient, &fid_maps.at(t), reps, log_level, nthreads));
        threads.emplace_back(&Loopback::subscribe, apps.at(t).get());
    }
    return_code_main = fclient->return_code;
    fclient.reset();
    for(auto& th : threads){ th.join();}
    DONE = true;
    //stable_thread.join();
    return return_code_main;
}
