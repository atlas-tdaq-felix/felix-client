#include <cstdio>
#include <cstring>
#include <string>
#include <thread>
#include <iostream>
#include <unistd.h>
#include <vector>
#include <map>

#include "docopt/docopt.h"

#include "felix/felix_client_properties.h"
#include "felix/felix_client_status.h"
#include "felix/felix_client_thread.hpp"
#include "felix/felix_client_exception.hpp"

#include "felixtag.h"

static const char USAGE[] =
R"(felix-client-thread-loopback - Subscribes to a tag and send info to a tag, checking if it is returned.

    Usage:
      felix-client-thread-loopback [options] <local_ip_or_interface> <subscribe-tag> <send-tag> <message>

    Options:
      -h --help                         Show this screen.
      --version                         Show version.
      --log-level=<loglevel>            Specify level of logging (trace, debug, info, warning, error, fatal) [default: info]
      --verbose-bus                     Show bus information
      --bus-dir=<directory>             Set directory for bus to use [default: ./bus]
      --bus-group-name=<group-name>     Set group-name for bus to use [default: FELIX]
      --timeout=<timeoutms>             Set timeout to subscribe (0 is no timeout) [default: 0]
      --netio-pages=<pages>             Number of pages for buffer [default: 256]
      --netio-pagesize=<pagesize>       Pagesize for buffer in bytes [default: 65536]
)";


struct {
    struct timespec t0;
    uint64_t messages_received;
    uint64_t bytes_received;

    uint64_t subscribe_tag;
    uint64_t send_tag;

    uint64_t counters;
    uint64_t truncated;
    uint64_t crc;
    uint64_t error;

    std::string message;

    unsigned show_statistics;
    bool subscribed;
} statistics;


class UserClass {

public:
    explicit UserClass(std::map<std::string, docopt::value> &args) {

        try {
            FelixClientThread::Config config;
            config.on_init_callback = std::bind(&UserClass::on_init, this);
            config.on_data_callback = std::bind(&UserClass::on_data, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
            config.on_connect_callback = std::bind(&UserClass::on_connect, this, std::placeholders::_1);
            config.on_disconnect_callback = std::bind(&UserClass::on_disconnect, this, std::placeholders::_1);

            config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] = args["<local_ip_or_interface>"].asString();
            config.property[FELIX_CLIENT_LOG_LEVEL] = args["--log-level"].asString();
            config.property[FELIX_CLIENT_BUS_DIR] = args["--bus-dir"].asString();
            config.property[FELIX_CLIENT_BUS_GROUP_NAME] = args["--bus-group-name"].asString();
            config.property[FELIX_CLIENT_VERBOSE_BUS] = args["--verbose-bus"].asBool() ? "True" : "False";
            config.property[FELIX_CLIENT_TIMEOUT] = args["--timeout"].asString();
            config.property[FELIX_CLIENT_NETIO_PAGES] = args["--netio-pages"].asString();
            config.property[FELIX_CLIENT_NETIO_PAGESIZE] = args["--netio-pagesize"].asString();

            client = new FelixClientThread(config);

            clock_gettime(CLOCK_MONOTONIC_RAW, &statistics.t0);

            statistics.subscribe_tag = args["<subscribe-tag>"].asLong();
            statistics.send_tag = args["<send-tag>"].asLong();
            statistics.message = args["<message>"].asString();

            statistics.show_statistics = false;
            statistics.counters = 0;
            statistics.truncated = 0;
            statistics.crc = 0;
            statistics.error = 0;
            statistics.subscribed = false;
            client->subscribe(statistics.subscribe_tag);

            while(!statistics.subscribed) {
                usleep(100000);
            }

            // sleep(2);

            try{
                client->send_data(statistics.send_tag, (const uint8_t*)statistics.message.c_str(), statistics.message.size() + 1, true);
            } catch(...){
                printf("failed to send...\n");
            }


            while(return_code == 0) {
                sleep(1);
            }
        } catch (std::invalid_argument const& error) {
            std::cerr << "Argument or option of wrong type" << std::endl;
            std::cout << std::endl;
            std::cout << USAGE << std::endl;
            return_code = -1;
        } catch (FelixClientResourceNotAvailableException& error) {
            printf("Failed to send\n");
            return_code = 1;
        }
    }

    ~UserClass() {
        delete client;
    }
    int return_code = 0;

private:
    void on_init() {
        printf("on_init called\n");
    }

    void on_connect(uint64_t fid) {
        printf("on_connect called 0x%lx %lx\n", fid, statistics.subscribe_tag);

        // FIXME seems to deadlock trying to setup send connection
        if (fid == statistics.subscribe_tag) {
            statistics.subscribed = true;
        }
    }

    void on_disconnect(uint64_t fid) {
        printf("on_disconnect called 0x%lx\n", fid);
    }

    void on_data(uint64_t fid, const uint8_t* data, size_t size, uint8_t status) {
        printf("on_data called for 0x%lx with size %zu and status %u\n", fid, size, status);
        statistics.messages_received++;
        statistics.bytes_received += size;
        statistics.counters++;

        if (status == FELIX_STATUS_FW_TRUNC || status == FELIX_STATUS_SW_TRUNC) {
            statistics.truncated++;
        }
        if (status == FELIX_STATUS_FW_CRC) {
            statistics.crc++;
        }
        if (status == FELIX_STATUS_FW_MALF || status == FELIX_STATUS_SW_MALF) {
            statistics.error++;
        }

        if (statistics.show_statistics) {
            // statistics
            struct timespec t1;
            clock_gettime(CLOCK_MONOTONIC_RAW, &t1);
            double seconds = t1.tv_sec - statistics.t0.tv_sec
                            + 1e-9*(t1.tv_nsec - statistics.t0.tv_nsec);
            if (seconds < 1.0) {
                return;
            }

            printf("data rate: %2f Gb/s   message rate: %2f kHz     ",
                    statistics.bytes_received*8/1000./1000./1000./seconds,
                    statistics.messages_received/1000./seconds);
            statistics.bytes_received = 0;
            statistics.messages_received = 0;
            statistics.t0 = t1;

            printf("FID, chunks, trunc, err, crc: { ");
            printf("0x%lx:%lu:%lu:%lu:%lu ", fid, statistics.counters, statistics.truncated, statistics.error, statistics.crc);
            printf("}\n");
        }

        if (size > 0) {
            std::string str((char*)data, std::min(size, strlen((const char*)data)));
            std::cout << "'" << str << "'" << std::endl;
            // std::cout << "'" << statistics.message << "'" << std::endl;
            if (str == statistics.message) {
                //delete client;
                return_code = 42;
                return;
            }
        }
        //delete client;
        return_code = 1;
    }

    FelixClientThread* client;
};


int main(int argc, char** argv) {
    std::map<std::string, docopt::value> args
            = docopt::docopt(USAGE,
                            { argv + 1, argv + argc },
                            true,               // show help if requested
                            (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string

    UserClass userClass(args);
    return userClass.return_code;
}
