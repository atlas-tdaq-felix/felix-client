#include <cstdio>
#include <string>
#include <thread>
#include <functional>
#include <iostream>
#include <unistd.h>

#include "docopt/docopt.h"

#include "felix/felix_client_properties.h"
#include "felix/felix_client_thread.hpp"
#include "felix/felix_client_exception.hpp"

#include "felixtag.h"

static const char USAGE[] =
R"(felix-client-thread-send - Sends msg to a felix-id using felix-bus to find the publisher.

    Usage:
      felix-client-thread-send [options] <local_ip_or_interface> <fid> <msg>

    Options:
      -h --help                         Show this screen.
      --version                         Show version.
      --log-level=<loglevel>            Specify level of logging (trace, debug, info, warning, error, fatal) [default: info]
      --verbose-bus                     Show bus information
      --bus-dir=<directory>             Set directory for bus to use [default: ./bus]
      --bus-group-name=<group-name>     Set group-name for bus to use [default: FELIX]
      --sleep=<seconds>                 Sleep after sending [default: 0]
)";

void on_init() {
    printf("on_init called\n");
}

void on_connect(uint64_t fid) {
    printf("on_connect called 0x%lx\n", fid);
}

void on_disconnect(uint64_t fid) {
    printf("on_disconnect called 0x%lx\n", fid);
}

int main(int argc, char** argv) {
    std::map<std::string, docopt::value> args
        = docopt::docopt(USAGE,
                         { argv + 1, argv + argc },
                         true,               // show help if requested
                         (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string

    FelixClientThread::Config config;
    config.on_init_callback = on_init;
    config.on_connect_callback = on_connect;
    config.on_disconnect_callback = on_disconnect;

    uint64_t fid;
    std::string msg;

    try {
        config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] = args["<local_ip_or_interface>"].asString();
        config.property[FELIX_CLIENT_LOG_LEVEL] = args["--log-level"].asString();
        config.property[FELIX_CLIENT_BUS_DIR] = args["--bus-dir"].asString();
        config.property[FELIX_CLIENT_BUS_GROUP_NAME] = args["--bus-group-name"].asString();
        config.property[FELIX_CLIENT_VERBOSE_BUS] = args["--verbose-bus"].asBool() ? "True" : "False";

        fid = args["<fid>"].asLong();
        msg = args["<msg>"].asString();

    } catch (std::invalid_argument const& error) {
        std::cerr << "Argument or option of wrong type" << std::endl;
        std::cout << std::endl;
        std::cout << USAGE << std::endl;
        return -1;
    }

    FelixClientThread* client = new FelixClientThread(config);

    try {
        //client->init_send_data(fid);
        client->send_data(fid, (const uint8_t*)msg.c_str(), msg.size() + 1, true);
    } catch (FelixClientResourceNotAvailableException& error) {
        printf("Failed to send\n");
        delete client;
        return 1;
    }

    int sleep_time = args["--sleep"].asLong();
    if (sleep_time > 0) {
        printf("Sleeping %d s\n", sleep_time);
        sleep(sleep_time);
    }

    printf("Normal exit\n");
    delete client;
    return 0;

}
