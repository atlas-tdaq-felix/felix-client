#include <cstdio>
#include <string>
#include <map>
#include <ostream>
#include <iostream>

#include "docopt/docopt.h"

#include "felixbus/felixbus.h"

#include "felix/felix_fid.h"

#include "netio/netio.h"

#include "felixtag.h"

static const char USAGE[] =
R"(netio-bus-unbuffered-publish - Publish some data from host/port under certain felix-id, and inform felix-bus.

    Usage:
      netio-bus-unbuffered-publish [options] <hostname> <port> <tags>...

    Options:
      -h --help                         Show this screen.
      --version                         Show version.
      --delay=<time_us>                 Delay in sending data [default: 100000].
      --verbose-bus                     Show bus information
      --bus-dir=<bus-directory>         Set bus directory [default: bus]
      --bus-group-name=<group-name>     Set group-name for bus to use [default: FELIX]
)";

struct {
    std::string hostname;
    uint port;
    std::vector<netio_tag_t> tags;
    uint delay;

    std::string bus_directory;
    std::string bus_group_name;
    bool verbose_bus;
    bool no_bus;

    struct netio_context ctx;
    struct netio_unbuffered_publish_socket socket;
    struct netio_buffer buf;

    struct netio_timer timer;

    uint8_t* data;
    size_t datasize;
} config;


void on_subscribe(netio_unbuffered_publish_socket* socket, netio_tag_t tag, void* addr, size_t addrlen) {
    printf("remote subscribed to tag 0x%lx\n", tag);
}

void on_connection_established(struct netio_unbuffered_publish_socket* socket) {
    printf("connection to subscriber established\n");
}

void on_connection_closed(struct netio_unbuffered_publish_socket* socket) {
    printf("connection to subscriber closed\n");
}

void on_msg_published(struct netio_unbuffered_publish_socket* socket, uint64_t key) {
    // printf("msg published, completion key=%lu\n", key);
}

std::map<uint64_t, uint64_t> keys;

void on_timer(void* ptr) {
    for(std::vector<netio_tag_t>::iterator it = config.tags.begin(); it != config.tags.end(); ++it) {
        netio_tag_t tag = *it;

        struct iovec iov;
        iov.iov_base = config.data;
        iov.iov_len = config.datasize;

        // printf("publish %lu\n", config.tag);
        keys[tag] = 0xFF;

        netio_unbuffered_publishv(&config.socket,
                                tag,
                                &iov,
                                1,    // iov count
                                &keys[tag], // key
                                0,    // flags
                                NULL  // subscription cache
                                );
    }
}


void on_init(void* data) {
    config.buf.size = 1024;
    config.buf.data = malloc(config.buf.size);

    printf("Opening publish socket on %s:%d\n", config.hostname.c_str(), config.port);
    netio_unbuffered_publish_socket_init(&config.socket, &config.ctx, config.hostname.c_str(), config.port, &config.buf);
    config.socket.cb_subscribe = on_subscribe;
    config.socket.cb_connection_established = on_connection_established;
    config.socket.cb_connection_closed = on_connection_closed;
    config.socket.cb_msg_published = on_msg_published;

    printf("Preparing data\n");
    config.datasize = 780;
    config.data = (uint8_t*)config.buf.data;
    config.data[0] = 0x00; // Fake status byte
    for(unsigned i=1; i<config.datasize; i++) {
        config.data[i] = 'x';
    }

    // FelixBus
    struct felix_bus_info info;
    info.ip = netio_hostname(config.hostname.c_str());
    info.raw_tcp = netio_tcp_mode(config.hostname.c_str());
    info.port = config.port;
    info.unbuffered = true;
    info.pubsub = true;
    info.netio_pages = 256;
    info.netio_pagesize = 65536;
    info.stream = false;

    printf("Using bus to publish %s:%u\n", info.ip, info.port);

    felix_bus bus = nullptr;

    int rc;
    for(std::vector<netio_tag_t>::iterator it = config.tags.begin(); it != config.tags.end(); ++it) {
        netio_tag_t tag = *it;

        if (!bus) {
            uint8_t vid = get_vid(tag);
            uint8_t did = get_did(tag);
            uint32_t cid = get_cid(tag);
            printf("vid %x, did %x, cid %x\n", vid, did, cid);

            char* bus_path = felix_bus_path(config.bus_directory.c_str(), config.bus_group_name.c_str(), vid, did, cid, "dma-0");
            if (bus_path == NULL) {
                printf("felix_bus_path: Cannot create bus_path\n");
                break;
            }

            bus = felix_bus_open(bus_path);
            if (!bus) {
                printf("felix_bus_open: errno=%d str=%s\n", errno, strerror(errno));
                break;
            }
        }

        printf("Publish 0x%lx\n", tag);
        rc = felix_bus_write(bus, tag, &info);
        if (rc < 0) {
            printf("felix_bus_write: errno=%d str=%s\n", errno, strerror(errno));
            break;
        }
    }

    rc = felix_bus_close(bus);
    if (rc < 0) {
        printf("felix_bus_close: errno=%d str=%s\n", errno, strerror(errno));
    }

    printf("Starting to publish every %d microseconds\n", config.delay);
    netio_timer_start_us(&config.timer, config.delay);
}

int main(int argc, char** argv) {

    std::map<std::string, docopt::value> args
        = docopt::docopt(USAGE,
                         { argv + 1, argv + argc },
                         true,               // show help if requested
                         (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string

    try {
        config.hostname = args["<hostname>"].asString();
        config.port = args["<port>"].asLong();
        std::vector<std::string> tag_list = args["<tags>"].asStringList();
        for(std::vector<std::string>::iterator it = tag_list.begin(); it != tag_list.end(); ++it) {
            netio_tag_t tag = std::stoul(*it, 0, 0);
            config.tags.push_back(tag);
        }
        config.delay = args["--delay"].asLong();
        config.bus_directory = args["--bus-dir"].asString();
        config.bus_group_name = args["--bus-group-name"].asString();
        config.verbose_bus = args["--verbose-bus"].asBool();
    } catch (std::invalid_argument const& error) {
        std::cerr << "Argument or option of wrong type" << std::endl;
        std::cout << std::endl;
        std::cout << USAGE << std::endl;
        return -1;
    }

    printf("netio-bus-publish\n");

    netio_init(&config.ctx);
    config.ctx.evloop.cb_init = on_init;

    netio_timer_init(&config.ctx.evloop, &config.timer);
    config.timer.cb = on_timer;

    netio_run(&config.ctx.evloop);

    return 0;
}
