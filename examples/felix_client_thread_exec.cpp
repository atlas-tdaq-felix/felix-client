#include <cstdio>
#include <string>
#include <thread>
#include <iostream>
#include <unistd.h>
#include <vector>
#include <map>

#include "docopt/docopt.h"

#include "felix/felix_client_properties.h"
#include "felix/felix_client_status.h"
#include "felix/felix_client_thread.hpp"

#include "felixtag.h"

static const char USAGE[] =
R"(felix-client-thread-exec - Test exec function.

    Usage:
      felix-client-thread-exec [options] <local_ip_or_interface>

    Options:
      -h --help                         Show this screen.
      --version                         Show version.
      --log-level=<loglevel>            Specify level of logging (trace, debug, info, warning, error, fatal) [default: info]
      --verbose-bus                     Show bus information
      --bus-group-name=<group-name>     Set group-name for bus to use [default: FELIX]
)";


class UserClass {

public:
    explicit UserClass(std::map<std::string, docopt::value> &args) {

        try {
            FelixClientThread::Config config;
            config.on_init_callback = std::bind(&UserClass::on_init, this);

            config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] = args["<local_ip_or_interface>"].asString();
            config.property[FELIX_CLIENT_LOG_LEVEL] = args["--log-level"].asString();
            config.property[FELIX_CLIENT_BUS_GROUP_NAME] = args["--bus-group-name"].asString();
            config.property[FELIX_CLIENT_VERBOSE_BUS] = args["--verbose-bus"].asBool() ? "True" : "False";

            client = new FelixClientThread(config);

            FelixClientThread::UserFunction user_function = std::bind(&UserClass::on_exec, this);
            client->exec(user_function);

            while(!exec_called) {
                sleep(1);
            }

            // delete client;

            return_code = exec_called ? 42 : 1;
        } catch (std::invalid_argument const& error) {
            std::cerr << "Argument or option of wrong type" << std::endl;
            std::cout << std::endl;
            std::cout << USAGE << std::endl;
            return_code = -1;
        }
    }

    ~UserClass() {
        delete client;
    }
    int return_code = 0;

private:
    bool exec_called;

    void on_init() {
        printf("on_init called\n");
        exec_called = false;
    }

    void on_exec() {
        printf("on_exec called\n");
        exec_called = true;
    }

    FelixClientThread* client;
};


int main(int argc, char** argv) {
    std::map<std::string, docopt::value> args
            = docopt::docopt(USAGE,
                            { argv + 1, argv + argc },
                            true,               // show help if requested
                            (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string

    UserClass userClass(args);
    return userClass.return_code;
}
